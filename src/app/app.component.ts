import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';

import { IconSetService } from '@coreui/icons-angular';
import { freeSet } from '@coreui/icons';
import { LoaderService } from './services/commonServices/loader.service';

@Component({
  // tslint:disable-next-line
  selector: 'NA-root',
  templateUrl: './app.component.html',
  providers: [IconSetService],
})
export class AppComponent implements OnInit {
  constructor(
    private router: Router,
    public iconSet: IconSetService,
    private loaderService: LoaderService
  ) {
    // iconSet singleton
    iconSet.icons = { ...freeSet };
  }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (evt instanceof NavigationStart) {
            this.loaderService.show();
      }
      if (evt instanceof NavigationStart) {
        this.loaderService.hide();
      }
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });

    // this.method_for_digit ();
  }



  public a = [];
  async method_for_digit () {
    let c = await this.method_for_adding_digit (this.a.length);
    if (this.a.length != 100) {
      this.a.push(c);
      this.method_for_digit();
    } else {
      console.log(this.a);
    }
  }

  async method_for_adding_digit(value) {
    value = value + 1;
    return value;
  }



}
