import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmActionComponent } from './confirm-action.component';
import { MaterialModule } from '../../material.module';



@NgModule({
  declarations: [
    ConfirmActionComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [ConfirmActionComponent],
  entryComponents: [ConfirmActionComponent]
})
export class ConfirmActionModule { }
