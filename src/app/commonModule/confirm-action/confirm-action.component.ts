import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
  message: string;
  title: string;
}

@Component({
  selector: 'NA-confirm-action',
  templateUrl: './confirm-action.component.html',
  styleUrls: ['./confirm-action.component.scss']
})
export class ConfirmActionComponent implements OnInit {
  message = "Would you like to delete this item?";
  title = "Delete Item"
  constructor( public dialogRef: MatDialogRef<ConfirmActionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {

        this.message = data.message || "Would you like to delete this item?";
        this.title = data.title ||  'Delete Item';
    }

  ngOnInit(): void {
  }

  action(type = false){
      this.dialogRef.close(type);
  }

}
