import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BreadcrumbComponent } from './breadcrumb.component';
import { MaterialModule } from '../../material.module';
import { AppBreadcrumbModule } from '@coreui/angular';



@NgModule({
  declarations: [BreadcrumbComponent],
  imports: [
    CommonModule,
    MaterialModule,
    AppBreadcrumbModule
  ],exports: [
    BreadcrumbComponent
  ]
})
export class BreadcrumbModule { }
