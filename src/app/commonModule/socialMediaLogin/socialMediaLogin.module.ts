import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SocialMediaLoginComponent } from './socialMediaLogin.component';

import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        component: SocialMediaLoginComponent
    }
]

@NgModule({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        RouterModule.forChild(routes)
    ], 
    declarations: [
        SocialMediaLoginComponent
    ],
    exports: [
        SocialMediaLoginComponent
    ]
})

export class SocialMediaLoginModule {

}