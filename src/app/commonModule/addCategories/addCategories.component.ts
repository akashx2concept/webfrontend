import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { CategoriesApiService } from "../../services/api/CategoriesApi.service";
import { NotificationService } from "../../services/commonServices/notification.service";
import { NeighborhoodsApiService } from "../../services/neighborhoodsApi.service";
import { ImageCropperComponent } from "../imageCropper/imageCropper.component";




@Component ({
    selector: 'NA-addCategories',
    templateUrl: './addCategories.component.html',
    styleUrls: [ './addCategories.component.scss' ]
})


export class AddCategoriesComponent implements OnInit {

    public parentObj:any = {};
    public text;

    public updateDetailsForm: FormGroup;
    url = "";
    @Output() action = new EventEmitter();

    public type;

    constructor (public bsModalRef: BsModalRef,
       private formBuilder: FormBuilder,
        private notificationService: NotificationService,
        private categoriesApiService: CategoriesApiService,
        private modalService: BsModalService,
        public neighborhoodsApiService: NeighborhoodsApiService ) {

    }

    ngOnInit () {
        console.log(this.parentObj);
        if (!this.parentObj['type']) {
            this.notificationService.get_Notification('info', "Type not present");
            this.cancel();
        } else {
            this.type = this.parentObj['type'];
        }
        this.validateForm();
    }

    validateForm () {
        this.updateDetailsForm = this.formBuilder.group({
            id:[],
            name: ['', Validators.required],
            isActive: ['true'],
            type:[this.type],
            imagesId:[]
        })

        if(this.parentObj.id){
          this.parentObj.isActive = this.parentObj.isActive + '';
          this.url = this.parentObj.images?.url;
          this.updateDetailsForm.patchValue(this.parentObj);
        }
    }

    get f() { return this.updateDetailsForm.controls; };


    method_for_updateDetails () {
        console.log(this.updateDetailsForm);
        if(this.updateDetailsForm.invalid){
            return false;
        }

        let data = {
          ...this.updateDetailsForm.value
        }
        console.log(data);
        const API = data.id ? this.categoriesApiService.patch_Categories(data):this.categoriesApiService.create_Categories(data);
        API.subscribe(
            data => {
                this.notificationService.get_Notification('success', 'Successfully Created');
                this.ok();
            }, error => {

            }
        )


    }



    ok() {
        this.bsModalRef.hide();
        this.action.emit(true);
    }

    cancel() {
        this.bsModalRef.hide();
        this.action.emit(false);
    }


    uploadCustomerImage(){
      let initialState: any;
        initialState = { parentObj: {  } };
        const confirmRef = this.modalService.show(ImageCropperComponent, {class: 'modal-dialog-centered modal-lg', initialState});
        confirmRef.content.action.subscribe((value) => {
            if (value){
              console.log(value);
              if (value.length) {
                // this.method_for_getting_addressList();
                this.url = value[0]['url']// (value[0]['id']);
                this.updateDetailsForm.get('imagesId').setValue(value[0]['id'])
              }
            } else {

            }
        });
    }
}
