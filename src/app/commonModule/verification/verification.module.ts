import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VerificationComponent } from './verification.component';
import { RouterModule, Routes } from '@angular/router';
import { routes } from '../../app.routing';
import { MaterialModule } from '../../material.module';


const route: Routes = [
  { path: '', component: VerificationComponent}
]


@NgModule({
  declarations: [
    VerificationComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(route),
    MaterialModule
  ]
})
export class VerificationModule { }
