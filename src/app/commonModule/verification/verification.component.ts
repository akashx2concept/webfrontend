import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserApiService } from '../../services/api/userApi.service';

@Component({
  selector: 'NA-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.scss']
})
export class VerificationComponent implements OnInit {
  hasVerfied:boolean;
  hasLoading: boolean = true;;
  constructor(private route: ActivatedRoute, private userApiService: UserApiService) { }

  ngOnInit(): void {
    this.route.params.subscribe(res => {
      this.verifyUser(res.tokenId);
    });
  }

  verifyUser(token){
    this.userApiService.post_verify_user({token}).subscribe(res => {
      this.hasVerfied =true;
      this.hasLoading = false;
    },error => {
      this.hasLoading = false;
      this.hasVerfied = false;
    });
  }

}
