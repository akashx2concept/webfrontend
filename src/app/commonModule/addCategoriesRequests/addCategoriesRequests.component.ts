import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { CategoriesApiService } from "../../services/api/CategoriesApi.service";
import { CategoriesRequestsApiService } from "../../services/api/CategoriesRequestsApi.service.";
import { CookiesService } from "../../services/commonServices/cookies.service";
import { NotificationService } from "../../services/commonServices/notification.service";
import { NeighborhoodsApiService } from "../../services/neighborhoodsApi.service";
import { ImageCropperComponent } from "../imageCropper/imageCropper.component";




@Component ({
    selector: 'NA-addCategoriesRequests',
    templateUrl: './addCategoriesRequests.component.html',
    styleUrls: [ './addCategoriesRequests.component.scss' ]
})


export class AddCategoriesRequestsComponent implements OnInit {

    public parentObj:any = {};
    public text;

    public updateDetailsForm: FormGroup;
    url = "";
    @Output() action = new EventEmitter();

    public type;

    constructor (public bsModalRef: BsModalRef,
       private formBuilder: FormBuilder,
        private notificationService: NotificationService,
        private categoriesRequestsApiService: CategoriesRequestsApiService,
        private modalService: BsModalService,
        public neighborhoodsApiService: NeighborhoodsApiService,
        private cookiesService: CookiesService ) {

    }

    ngOnInit () {
        console.log(this.parentObj);
        if (!this.parentObj['type']) {
            this.notificationService.get_Notification('info', "Type not present");
            this.cancel();
        } else {
            this.type = this.parentObj['type'];
        }
        this.validateForm();
    }

    validateForm () {
       const vendorId = (this.cookiesService.get_current_vendor_details())?.id;
       const userId = this.cookiesService.get_user_login_data().id;
        this.updateDetailsForm = this.formBuilder.group({
            id:[],
            name: ['', Validators.required],
            isActive: ['true'],
            type:[this.type],
            status: ['Requested'],
            userId: [userId],
            vendorsId: [vendorId]
        })

        if(this.parentObj.id){
          this.parentObj.isActive = this.parentObj.isActive + '';
          this.url = this.parentObj.images?.url;
          this.updateDetailsForm.patchValue(this.parentObj);
        }
    }

    get f() { return this.updateDetailsForm.controls; };


    method_for_updateDetails () {
        console.log(this.updateDetailsForm);
        if(this.updateDetailsForm.invalid){
            return false;
        }

        let data = {
          ...this.updateDetailsForm.value
        }
        console.log(data);
        const API = data.id ? this.categoriesRequestsApiService.patch_Categories_Request(data):this.categoriesRequestsApiService.create_Categories_Request(data);
        API.subscribe(
            data => {
                this.notificationService.get_Notification('success', 'Successfully Requested');
                this.ok();
            }, error => {

            }
        )


    }



    ok() {
        this.bsModalRef.hide();
        this.action.emit(true);
    }

    cancel() {
        this.bsModalRef.hide();
        this.action.emit(false);
    }


    uploadCustomerImage(){
      let initialState: any;
        initialState = { parentObj: {  } };
        const confirmRef = this.modalService.show(ImageCropperComponent, {class: 'modal-dialog-centered modal-lg', initialState});
        confirmRef.content.action.subscribe((value) => {
            if (value){
              console.log(value);
              if (value.length) {
                // this.method_for_getting_addressList();
                this.url = value[0]['url']// (value[0]['id']);
                this.updateDetailsForm.get('imagesId').setValue(value[0]['id'])
              }
            } else {

            }
        });
    }
}
