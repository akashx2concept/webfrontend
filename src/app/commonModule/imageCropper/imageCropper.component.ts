import { Component, OnInit, EventEmitter, Output } from "@angular/core";
import { BsModalRef } from "ngx-bootstrap/modal";
import { Dimensions, ImageCroppedEvent, ImageTransform } from 'ngx-image-cropper';
import {base64ToFile} from 'ngx-image-cropper';
import { ImageAPIService } from "../../services/api/imageApi.service";
import { NotificationService } from "../../services/commonServices/notification.service";



@Component({
    selector: 'NA-imageCropper',
    templateUrl: './imageCropper.component.html',
    styleUrls: [ './imageCropper.component.scss' ]
})


export class ImageCropperComponent implements OnInit {

    
    @Output() action = new EventEmitter();
    imageChangedEvent: any = '';

    public profilePicFormData;
    public uploadedImageData;

    croppedImage: any = '../../../../../assets/img/placeholder.png';
    canvasRotation = 0;
    rotation = 0;
    scale = 1;
    showCropper = false;
    containWithinAspectRatio = false;
    transform: ImageTransform = {};

    public filename;

    constructor ( public bsModalRef: BsModalRef, private notificationService: NotificationService,
        private imageAPIService: ImageAPIService ) {

    }

    ngOnInit () {

    }


    method_for_uploadImage () {
        if (this.profilePicFormData) {
            
            this.imageAPIService.upload_image(this.profilePicFormData).subscribe(
                data => {
                    
                    this.uploadedImageData = data;
                    // this.method_for_upodate_user();
                    this.ok(this.uploadedImageData);
                }, error => {
                    this.notificationService.get_Notification('error', "Image Not uploaded");
                }
            )
        }
    }

    
    fileChangeEvent(event: any): void {
        this.imageChangedEvent = event;

        var render = new FileReader();
        console.log(this.imageChangedEvent);

        render.readAsDataURL(event.target.files[0]);
        let fileList: FileList = event.target.files;
        let files: any = fileList[0];
        let filename = files.name;
        console.log(filename)
        this.filename = filename;
        // this.profilePicName = filename;
        // this.profilePicFormData = new FormData();
        // this.profilePicFormData.append('files', files);

    }


    imageCropped(event: ImageCroppedEvent) {
        
        this.croppedImage = event.base64;
        console.log(event, base64ToFile(event.base64), "44444444");

        const file = this.DataURIToBlob(this.croppedImage);
        console.log(file);
        this.profilePicFormData = new FormData();

        var fileExtension = '.' + this.filename.split('.').pop();
        var timeStamp = Math.floor(Date.now() / 1000);
        let filename = timeStamp + fileExtension;


        this.profilePicFormData.append('file', file, filename) 
        // formData.append('profile_id', this.profile_id) //other param
        // formData.append('path', 'temp/') //other param

        console.log(filename, " ---- filename")
    }   

    
    DataURIToBlob(dataURI: string) {
        const splitDataURI = dataURI.split(',')
        const byteString = splitDataURI[0].indexOf('base64') >= 0 ? atob(splitDataURI[1]) : decodeURI(splitDataURI[1])
        const mimeString = splitDataURI[0].split(':')[1].split(';')[0]

        const ia = new Uint8Array(byteString.length)
        for (let i = 0; i < byteString.length; i++)
            ia[i] = byteString.charCodeAt(i)

        return new Blob([ia], { type: mimeString })
    }


    imageLoaded() {
        this.showCropper = true;
        console.log('Image loaded');
    }

    cropperReady(sourceImageDimensions: Dimensions) {
        console.log('Cropper ready', sourceImageDimensions);
    }

    loadImageFailed() {
        console.log('Load failed');
    }

    rotateLeft() {
        this.canvasRotation--;
        this.flipAfterRotate();
    }

    rotateRight() {
        this.canvasRotation++;
        this.flipAfterRotate();
    }

    private flipAfterRotate() {
        const flippedH = this.transform.flipH;
        const flippedV = this.transform.flipV;
        this.transform = {
            ...this.transform,
            flipH: flippedV,
            flipV: flippedH
        };
    }


    flipHorizontal() {
        this.transform = {
            ...this.transform,
            flipH: !this.transform.flipH
        };
    }

    flipVertical() {
        this.transform = {
            ...this.transform,
            flipV: !this.transform.flipV
        };
    }

    resetImage() {
        this.scale = 1;
        this.rotation = 0;
        this.canvasRotation = 0;
        this.transform = {};
    }

    zoomOut() {
        this.scale -= .1;
        this.transform = {
            ...this.transform,
            scale: this.scale
        };
    }

    zoomIn() {
        this.scale += .1;
        this.transform = {
            ...this.transform,
            scale: this.scale
        };
    }

    toggleContainWithinAspectRatio() {
        this.containWithinAspectRatio = !this.containWithinAspectRatio;
    }

    updateRotation() {
        this.transform = {
            ...this.transform,
            rotate: this.rotation
        };
    }

    



    ok(obj) {
        this.bsModalRef.hide();
        this.action.emit(obj);
    }
    
    cancel() {
        this.bsModalRef.hide();
        this.action.emit(false);
    }

}