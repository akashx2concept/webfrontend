import { Component, Inject, OnInit, ViewChild, ɵConsole } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { StripeService, StripeCardNumberComponent } from 'ngx-stripe';
import { StripeCardElementOptions, StripeElementsOptions, PaymentIntent,} from '@stripe/stripe-js';
import { NeighborhoodsApiService } from '../../services/neighborhoodsApi.service';
import { StripePaymentApiService } from '../../services/api/stripePaymentApi.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { NotificationService } from '../../services/commonServices/notification.service';


@Component({
    selector: 'NA-customerPayment',
    templateUrl: './customerPayment.component.html',
    styleUrls: [ './customerPayment.component.scss' ]
})


export class CustomerPaymentComponent implements OnInit {

    @ViewChild(StripeCardNumberComponent) card: StripeCardNumberComponent;

    cardOptions: StripeCardElementOptions = {
        style: {
          base: {            
            iconColor: '#666EE8',
            color: '#31325F',
            fontWeight: '300',
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSize: '18px',
            '::placeholder': {
              color: '#CFD7E0',
            },
          },
        },
        hidePostalCode: true,
        iconStyle: 'solid'
      };
    
      elementsOptions: StripeElementsOptions = {
        locale: 'en',
      };
    
      stripeTest: FormGroup;
    
      constructor(
        public dialogRef: MatDialogRef<CustomerPaymentComponent>,
        @Inject(MAT_DIALOG_DATA) public parentObj: any,
        private http: HttpClient,
        private fb: FormBuilder,
        private stripeService: StripeService,
        private neighborhoodsApiService: NeighborhoodsApiService,
        private stripePaymentApiService: StripePaymentApiService,
        private notificationService: NotificationService
      ) {}
    
      ngOnInit(): void {
        this.stripeTest = this.fb.group({
          name: ['Angular v10', [Validators.required]],
          amount: [1001, [Validators.required, Validators.pattern(/\d+/)]],
        });
      }
    
      pay(): void {
        if (this.stripeTest.valid) {
        let data =  this.parentObj
          this.stripePaymentApiService.StripePayments_customerPlaceOrder(data)
            .pipe(
              switchMap((pi) =>
                this.stripeService.confirmCardPayment(pi.client_secret, {
                  payment_method: {
                    card: this.card.element,
                    billing_details: {
                      name: this.stripeTest.get('name').value,
                    },
                  },
                })
              )
            )
            .subscribe((result) => {
              if (result.error) {
                // Show error to your customer (e.g., insufficient funds)
                console.log(result.error.message);
                // alert("error")
                this.notificationService.get_Notification('error', result.error.message);
                this.method_For_payment_success (result.paymentIntent);
              } else {
                // The payment has been processed!
                if (result.paymentIntent.status === 'succeeded') {
                  // Show a success message to your customer
                  console.log("success");
                  // alert("success");
                  this.method_For_payment_success (result.paymentIntent);
                  this.notificationService.get_Notification("success", "Payment Successfully");
                  this.onNoClick();
                }
              }
            });
        } else {
          console.log(this.stripeTest);
        }
      }

      method_For_payment_success (paymentIntent) {
        var dataDto = {
          userId: this.parentObj['userId'],
          paymentIntentId: paymentIntent['id']
        }
        console.log(dataDto);
        this.stripePaymentApiService.Orders_paymentStatus(dataDto).subscribe(
          data => {
            
          }, error => {

          }
        )
      }


      onNoClick() {
        setTimeout (() => {
          this.dialogRef.close();
        })        
      }

    // ngOnInit () {
    //     this.stripeTest = this.fb.group({
    //         name: ['', [Validators.required]]
    //     });
    // }


    // createToken(): void {
    //     const name = this.stripeTest.get('name').value;
    //     this.stripeService.createToken(this.card.element, { name }).subscribe((result) => {
    //         if (result.token) {
    //           // Use the token
    //           console.log(result.token.id);
    //         } else if (result.error) {
    //           // Error creating the token
    //           console.log(result.error.message);
    //         }
    //       });
    //   }


}