import { Component, Input, OnInit } from "@angular/core";
import { StripePaymentApiService } from "../../services/api/stripePaymentApi.service";
import { VendorsApiService } from "../../services/api/vendorsApi.service";
import { CookiesService } from "../../services/commonServices/cookies.service";
import { NotificationService } from "../../services/commonServices/notification.service";




@Component({
    selector: 'NA-venderStripeConnect',
    templateUrl: './venderStripeConnect.component.html',
    styleUrls: [ './venderStripeConnect.component.scss' ]
})


export class VenderStripeConnectComponent implements OnInit {

    public popupIns;
    public final_url_with_token;

    // public social_login_url;

    public stripPaymentUrlData;

    @Input ('vendorId') vendorId;

    public vendorData;
    public getConnectedAccountData;

    constructor (
      private stripePaymentApiService: StripePaymentApiService,
      private vendorsApiService: VendorsApiService,
      private notificationService: NotificationService,
      private cookiesService: CookiesService ) {

    }

    ngOnInit () {
      // this.method_for_getting_venderOnboard_url();
      if(!this.cookiesService.get_stripe_status()){
        this.method_for_getting_vendorDetails ();
      }else {
        this.method_for_getting_vendorDetails (true);
      }

    }

    method_for_getting_vendorDetails (vendorConnected = false) {
      let que:any = {
        where: {
          id: this.vendorId
        }
      }
      this.vendorsApiService.Vendors_findOne(JSON.stringify(que)).subscribe(
        data => {
          this.vendorData = data;
          if (this.vendorData['stripeConnectedAccountId'] && !vendorConnected) {
            this.method_for_getting_connecting_accoutnData ();
          }
        }, error => {

        }
      )
    }

    method_for_getting_connecting_accoutnData () {
      let da = {
        vendorId: this.vendorId
      }
      this.stripePaymentApiService.StripePayments_getConnectedAccountData(da).subscribe(
        data => {
          this.getConnectedAccountData = data;
          // && this.getConnectedAccountData['details_submitted'] && this.getConnectedAccountData['payouts_enabled'] 
          if (this.getConnectedAccountData['charges_enabled'] && this.getConnectedAccountData['payouts_enabled'] ) {
            this.method_for_getting_loginStripe_url ();
            this.method_for_updating_vendor ()
          } else {
            this.notificationService.get_Notification('info', "Some details is not verified in stripe");
            this.method_for_getting_loginStripe_url ();
          }
        }, error => {

        }
      )
    }

    method_for_getting_loginStripe_url () {
      let da = {
        vendorId: this.vendorId
      }
      this.stripePaymentApiService.StripePayments_getConnectedAccount_loginLink(da).subscribe(
        data => {
          this.cookiesService.set_stripe_status()
          this.method_for_login_popupHandle(data['url'])
        }, error => {
          if (this.vendorData['stripeConnectedAccountId']) {
            this.method_for_getting_venderRefreshOnboard_url ();
          }
        }
      )
    }

    method_for_getting_venderRefreshOnboard_url () {
      let dto = {
        accountId: this.vendorData['stripeConnectedAccountId'],
        vendorsId: this.vendorId
      }

      this.stripePaymentApiService.StripePayments_onboardVendorRefresh(dto).subscribe(
        data => {
          // this.method_for_getting_connecting_accoutnData();
          this. method_for_login_popupHandle (data['url'])
        }, error => {

        }
      )
    }





    method_for_vendor_onboarding() {
        let dataDto = {
            vendorId: this.vendorId
        }

        this.stripePaymentApiService.StripePayments_onboardVendor(dataDto).subscribe(
            data => {
                this.stripPaymentUrlData = data['status'];
                this.method_for_login_popupHandle(this.stripPaymentUrlData['setupUrl']);
            }, error => {

            }
        )
    }

    method_for_login_popupHandle (social_login_url) {
      this.popupIns = window.open(social_login_url, '_blank');
      let self = this;
      let closeNewWindow = setInterval(function(){
        if(self.popupIns.closed){
          if(!self.cookiesService.get_stripe_status()){
            self.method_for_getting_vendorDetails();
          }
          clearInterval(closeNewWindow);
        } else if(self.final_url_with_token != null) {
          self.method_for_getting_vendorDetails();
          self.popupIns.opener.customeNotify(b);
          var b = null;
        }
      }, 5000);
      window["customeNotify"] = function(e){
        self.final_url_with_token = e;
        if(e != null || undefined){
          console.log(e)
          self.method_for_getting_vender_stripeDetails();
          // self.popupIns.close();
          self.cookiesService.set_stripe_status();
        }
        let closeNewWindow = setInterval(function(){
            if(e == null || undefined){
              self.popupIns.close();
              self.method_for_getting_vendorDetails();
              clearInterval(closeNewWindow);
            }
        }, 1000);

      }

  }





    method_for_getting_vender_stripeDetails () {
      let stripeQuery = {
        where: {
          vendorsId: this.vendorId
        }
      }
      this.stripePaymentApiService.StripePayment(JSON.stringify(stripeQuery)).subscribe(
        data => {
          if (data.length) {
            this.method_for_getting_vendorDetails();
            // this.method_for_getting_onboardVendorRefresh(data[0]);
          }
        }, error => {

        }
      )
    }

    method_for_updating_vendor () {
      var data = {
        id: this.vendorId,
        stripeConnected: true,
      }
      this.vendorsApiService.update_Vendors(data).subscribe(
        data => {
          // this.method_for_getting_vendorDetails ();
        }, error => {

        }
      )
    }

  //   method_for_getting_onboardVendorRefresh (data) {
  //     let dto = {
  //       accountId: data['accountId'],
  //       vendorsId: data['vendorsId']
  //     }

  //     this.stripePaymentApiService.StripePayments_onboardVendorRefresh(dto).subscribe(
  //       data => {
  //         this.method_for_getting_connecting_accoutnData();
  //       }, error => {

  //       }
  //     )

  //   }






}


