import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthApiService } from "../../services/api/authApi.service";
import { CookiesService } from "../../services/commonServices/cookies.service";




@Component ({
    selector: 'NA-headerSideBar',
    templateUrl: './headerSideBar.component.html',
    styleUrls: [ './headerSideBar.component.scss' ]
})


export class HeaderSideBarComponent implements OnInit {


  public loginUserData;


    constructor (private router: Router, private authApiServvice: AuthApiService, private cookieService : CookiesService) {

    }

    ngOnInit () {
      this.loginUserData = this.cookieService.get_user_login_data();
    }


    method_for_redirect (value) {
      if (this.loginUserData['role'] == 'Root') {
        this.router.navigate(['home/profile/address']);
      } else if (this.loginUserData['role'] == 'Vendor') {
        this.router.navigate(['vendor/profile/address']);
      }
       
    }


    method_for_chengePassword_routing () {
      if (this.loginUserData['role'] == 'Root') {
        this.router.navigate(['home/change-password']);
      } else if (this.loginUserData['role'] == 'Vendor') {
        this.router.navigate(['vendor/change-password']);
      }
    }

    logout(){
        this.authApiServvice.logout().subscribe(res => {
          this.cookieService.clearAll();
            this.router.navigate(['/']);
        }, error => {
          this.cookieService.clearAll();
          this.router.navigate(['/']);
        });
    }
}
