import { Component, ElementRef, forwardRef, Input, NgZone, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { CKEditorComponent } from 'ngx-ckeditor';

const VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  // tslint:disable-next-line: no-use-before-declare
  useExisting: forwardRef(() => NaCkeditorComponent),
  multi: true
};
@Component({
  selector: 'NA-na-ckeditor',
  templateUrl: './na-ckeditor.component.html',
  styleUrls: ['./na-ckeditor.component.scss']
})
export class NaCkeditorComponent extends CKEditorComponent implements OnInit {
  @Input()
    config: any = {
        height: '400',
        removePlugins: 'elementspath'
    };
    @Input()
    debounce = '300';
    @Input()
    set uploadUrl(url: string) {
        this.config.uploadUrl = url;
    }

    constructor(zone: NgZone,
        hostEl: ElementRef) {
        super(zone, hostEl);
        // this.ready.subscribe((event) => {
        //     this.configureCKEditor(event.editor);
        // });
    }

    ngOnInit() {
        this.ready.subscribe((event) => {
            this.configureCKEditor(event.editor);
        });
    }



    configureCKEditor(editor: any) {
        // Handle pasted image
        editor.on('fileUploadRequest', (event) => {
            const xhr = event.data.fileLoader.xhr;
            // xhr.setRequestHeader('Authorization', 'Bearer ' + this.authHolderService.getJwtToken());
        });

        editor.on('fileUploadResponse', (event) => {
            event.stop();
            const data = event.data;
            const response = JSON.parse(data.fileLoader.xhr.response);
            data.url = response.url;


            // "change" event won't fire after the image has been pasted, so that has to be handled specifically
            data.fileLoader.on('update', () => {
                this.change.emit(editor.getData());
            });
        });

        // TODO Handle input in "source" mode
        //     editor.on("key", () => {
        //         this.applyChanges(editor.getData());
        //     });
    }

}
