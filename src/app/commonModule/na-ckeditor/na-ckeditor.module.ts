import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NaCkeditorComponent } from './na-ckeditor.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    NaCkeditorComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    NaCkeditorComponent
  ]
})
export class NaCkeditorModule { }
