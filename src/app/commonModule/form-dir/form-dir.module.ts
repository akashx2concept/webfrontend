import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormDirective } from './form-dir.directive';



@NgModule({
  declarations: [
    FormDirective
  ],
  imports: [
    CommonModule
  ],exports: [
    FormDirective
  ],
  entryComponents: [FormDirective]
})
export class FormDirModule { }
