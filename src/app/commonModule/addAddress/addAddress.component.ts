import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BsModalRef } from "ngx-bootstrap/modal";
import { AddressApiService } from "../../services/api/addressApi";
import { CityApiService } from "../../services/api/cityApi";
import { CountryApiService } from "../../services/api/countryApi";
import { StateApiService } from "../../services/api/stateApi";
import { NotificationService } from '../../services/commonServices/notification.service';


@Component({
    selector: 'NA-addAddress',
    templateUrl: './addAddress.component.html',
    styleUrls: [ './addAddress.component.scss' ]
})


export class AddAddressComponent implements OnInit {

    private parentObj:any = {};
    public text;

    public updateDetailsForm:FormGroup; 

    @Output() action = new EventEmitter();


    public countryList = [];
    public stateList = [];
    public cityList = [];

    public selectedCountry;
    public selectedState;
    public selectedCity;

    public userId;

    public updateId;
    
    constructor(public bsModalRef: BsModalRef, private formBuilder: FormBuilder,
        private countryApiService: CountryApiService, private addressApiService: AddressApiService,
        private stateApiService: StateApiService, private cityApiService: CityApiService,
        private notificationService: NotificationService ) { 

    }

    ngOnInit () {

        if (!this.parentObj['userId']) { 
            this.notificationService.get_Notification('info', "UserId not present");
            this.cancel();
        } else {
            this.userId = this.parentObj['userId'];
            if (this.parentObj['id']) {
                this.updateId = this.parentObj['id'];
                this.method_for_gettingAddress ();
            }
        }
        this.validateForm();
    }


    

    validateForm(){
        this.updateDetailsForm = this.formBuilder.group({
          street1: ['', Validators.required],
          street2: ['', Validators.required],
          state: ['', Validators.required],
          city: ['', Validators.required],
          country: ['', Validators.required],
          postal_code: ['', Validators.required],
        })

        this.method_for_getting_country ();
    }
    
    get f() { return this.updateDetailsForm.controls; };



    method_for_updateDetails () {
        console.log(this.updateDetailsForm.value);
        if(this.updateDetailsForm.invalid){
            return false;
        }
        let data = {
            street1: this.updateDetailsForm.value['street1'],
            street2: this.updateDetailsForm.value['street2'],
            postalCode: this.updateDetailsForm.value['postal_code'],
            userId: this.userId,
            countryId: this.updateDetailsForm.value['country'],
            stateId: this.updateDetailsForm.value['state'],
            cityId: this.updateDetailsForm.value['city']
        }

        if (this.updateId) {
            data['id'] = this.updateId
        }

        console.log(data);

        if (!this.updateId) {
            this.addressApiService.create_Addresses(data).subscribe(
                data => {
                    this.notificationService.get_Notification('success', 'Successfully Created');
                    this.ok();
                }, error => {
    
                }
            )
        } else {
            this.addressApiService.update_Addresses(data).subscribe(
                data => {
                    this.notificationService.get_Notification('success', 'Successfully Updated');
                    this.ok();
                }, error => {
    
                }
            )
        }
    
    }

    method_for_gettingAddress () {
        var query:any = {
            where: {
                id: this.updateId
            }
        }
        query = encodeURI(JSON.stringify(query));
        this.addressApiService.get_Addresses(query).subscribe(
            data => {
                let address = data[0];
                this.method_for_change_in_country ({id: address['countryId']});
                this.method_for_change_in_state ({id: address['stateId']});
                setTimeout (() => {
                    this.updateDetailsForm.patchValue ({
                        street1: address['street1'],
                        street2: address['street2'],
                        state: address['stateId'],
                        city: address['cityId'],
                        country: address['countryId'],
                        postal_code: address['postalCode'],
                    })
                })                
            }, error => {
                
            }
        )
    }

    
    method_for_getting_country (searchVal?) {
        console.log(searchVal)
        let query:any = {
            where: {
                isActive: true,
                isDeleted: false
            },
        }
        query = encodeURI(JSON.stringify(query));
        this.countryApiService.get_countries(query).subscribe(
            data => {
                this.countryList = data;
            }, error => {

            }
        )
    }

    method_for_change_in_country (value) {
        console.log(value);
        this.selectedCountry = value;
        let query:any = {
            where: {
                countryId: value['id'],
                isActive: true,
                isDeleted: false
            },
        }
        query = encodeURI(JSON.stringify(query));
        this.stateApiService.get_states(query).subscribe(
            data => {
                this.stateList = data;
            }, error => {

            }
        )
    }


    method_for_change_in_state (value) {
        console.log(value);
        this.selectedState = value;
        let query:any = {
            where: {
                stateId: value['id'],
                isActive: true,
                isDeleted: false
            },
        }
        query = encodeURI(JSON.stringify(query));
        this.cityApiService.get_cities(query).subscribe(
            data => {
                this.cityList = data;
            }, error => {

            }
        )

    }




    method_for_change_in_city (value) {
        console.log(value);
        this.selectedCity = value;;
    }

    ok() {
        this.bsModalRef.hide();
        this.action.emit(true);
    }
    
    cancel() {
        this.bsModalRef.hide();
        this.action.emit(false);
    }

}