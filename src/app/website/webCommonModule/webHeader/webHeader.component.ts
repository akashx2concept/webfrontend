import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { AuthApiService } from "../../../services/api/authApi.service";
import { CookiesService } from "../../../services/commonServices/cookies.service";
import { WebAuthInternalService } from "../../service/webAuthInternal.service";
import { UserLoginComponent } from "../../user-auth/user-login/user-login.component";
import { UserRegisterComponent } from "../../user-auth/user-register/user-register.component";



@Component({
    selector: 'NA-webHeader',
    templateUrl: './webHeader.component.html',
    styleUrls: [ './webHeader.component.scss' ]
})


export class WebHeaderComponent implements OnInit {


    public loginUserData;

    public userRole;

    constructor (public webAuthInternalService: WebAuthInternalService, private cookiesService: CookiesService,
        private authApiService: AuthApiService, private cookieService: CookiesService, private router: Router ) {

        this.loginUserData = this.cookiesService.get_user_login_data();

        if (this.loginUserData && this.loginUserData['role'] == "User") {
            this.userRole = "User";
        }

    }

    ngOnInit () {

    }

    logout(){
        this.authApiService.logout().subscribe(
            res => {
                this.cookieService.clearAll();
                this.router.navigate(['/']);

                setTimeout(() => {
                    location.reload();
                })

                // this.router.navigate(['/']);
        }, error => {
                this.cookieService.clearAll();
                setTimeout(() => {
                    location.reload();
                })
                // this.router.navigate(['/']);
        });
    }


    // userLogin(){
    //   this.dialgo.open(UserLoginComponent, {
    //     data: {},
    //     width: '529px',

    //   })
    // }

    // userRegister(){
    //   this.dialgo.open(UserRegisterComponent, {
    //     data: {},
    //     width: '529px',
    //     // height: '100vh'
    //   })
    // }
}
