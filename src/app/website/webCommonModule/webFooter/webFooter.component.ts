import { Component, OnInit } from "@angular/core";
import { CookiesService } from "../../../services/commonServices/cookies.service";



@Component({
    selector: 'NA-webFooter',
    templateUrl: './webFooter.component.html',
    styleUrls: [ './webFooter.component.scss' ] 
})


export class WebFooterComponent implements OnInit {


    public loginUserData;

    constructor (private cookiesService: CookiesService) {
        this.loginUserData = this.cookiesService.get_user_login_data();
    }


    ngOnInit () {

    }
    
}