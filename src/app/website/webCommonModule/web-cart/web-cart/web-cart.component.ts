import { Component, OnInit } from '@angular/core';
import { FavoriteVendorsApiService } from '../../../../services/api/favoriteVendorsApi.service';
import { ProductApiService } from '../../../../services/api/productApi.service';
import { VendorsApiService } from '../../../../services/api/vendorsApi.service';
import { CookiesService } from '../../../../services/commonServices/cookies.service';
import { NeighborhoodsApiService } from '../../../../services/neighborhoodsApi.service';
import { WebAuthInternalService } from '../../../service/webAuthInternal.service';
import { WebVendorListInternalService } from './web-vendorListInternal.service';

@Component({
  selector: 'NA-web-cart',
  templateUrl: './web-cart.component.html',
  styleUrls: ['./web-cart.component.scss']
})
export class WebCartComponent implements OnInit {

  public vendorList:any = [];

  public loginUserData;

  constructor(private vendorsApiService: VendorsApiService, public neighborhoodsApiService : NeighborhoodsApiService,
    private productApiService: ProductApiService, private favoriteVendorsApiService: FavoriteVendorsApiService,
    private cookiesService: CookiesService, private webAuthInternalService: WebAuthInternalService, 
    public webVendorListInternalService: WebVendorListInternalService ) {

      this.loginUserData = this.cookiesService.get_user_login_data();
  }

  ngOnInit(): void {
    this.vendorGettingLIst();
  }


  vendorGettingLIst () {
    this.webVendorListInternalService.getVendorList();
  }

  // getVendorList(){
  //   var query:any = {
  //     where: {
  //       isActive:true
  //     },
  //     include: [
  //       { relation: 'bannerImage',  scope: { include: [ { relation: "bannerImage" } ], order: "id Desc"}  },
  //       { relation: 'logoImage' },
  //       { relation: 'categories' },
  //       { relation: 'products', scope: { limit: 1 } },
  //     ],
  //     limit: 6
  //   }

  //   if (this.loginUserData) {
  //     query['include'].push( { relation: "favoriteVendors", scope: { where: { userId: this.loginUserData['id'] } } } )
  //   }

  //   query = JSON.stringify(query);
  //   this.vendorsApiService.get_Vendors(query).subscribe(
  //     res => {
  //       console.log(res);
  //       this.vendorList  = res;
  //       if (this.loginUserData) {
  //         for (let q=0; q<=this.vendorList.length-1; q++) {
  //           this.method_for_getting_price(this.vendorList[q]['id'])
  //         }
  //       }
  //     }, error => {

  //     }
  //   )
  // }


  // method_for_getting_price (vendorId) {
  //     var query:any = { where: { vendorsId: vendorId }, order: "unitPrice ASC", limit: 1 };
  //     query = JSON.stringify(query);
  //     this.productApiService.Products_ProductPrice(query).subscribe(
  //       data => {
  //         if (data.length) {
  //           let priceData = data[0];
  //           for (var z=0; z<=this.vendorList.length-1; z++) {
  //             if (this.vendorList[z]['id'] == priceData['vendorsId']) {
  //               this.vendorList[z]['priceForOne'] = priceData['unitPrice']
  //             }
  //           }
  //         }
  //       }, error => {

  //       }
  //     )
  // }

  method_for_create_favroite_vendor (vendor) {
    if (!this.loginUserData) {
      this.webAuthInternalService.userLogin();
    } else {
      let dataDto = {
          vendorsId: vendor['id'],
          userId: this.loginUserData['id']
      };
      this.favoriteVendorsApiService.create_Vendors_FavoriteVendor(dataDto).subscribe(
        data => {
          this.vendorGettingLIst ();
        }, error => {

        }
      )
    } 
  }


  method_for_delete_favorite_vendor (favoriteVendors) {
    if (favoriteVendors.length) {
      let favData = favoriteVendors[0];
      this.favoriteVendorsApiService.delete_Vendors_FavoriteVendor(favData['id']).subscribe(
        data => {
          this.vendorGettingLIst ();
        }, error => {

        }
      )
    }
  }

 

}
