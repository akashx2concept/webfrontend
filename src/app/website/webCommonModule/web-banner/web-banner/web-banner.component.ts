import { Component, OnInit } from '@angular/core';
import { WebVendorListInternalService } from '../../web-cart/web-cart/web-vendorListInternal.service';

@Component({
  selector: 'NA-web-banner',
  templateUrl: './web-banner.component.html',
  styleUrls: ['./web-banner.component.scss']
})

export class WebBannerComponent implements OnInit {

  constructor (public webVendorListInternalService: WebVendorListInternalService) { 

  }

  ngOnInit() {

  }

  method_for_change_in_filter () {
    this.webVendorListInternalService.getVendorList();
  }

}
