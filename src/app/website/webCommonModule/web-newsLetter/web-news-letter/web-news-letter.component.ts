import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SubscribeEmailService } from '../../../../services/api/subscribe-email.service';
import { NotificationService } from '../../../../services/commonServices/notification.service';

@Component({
  selector: 'NA-web-news-letter',
  templateUrl: './web-news-letter.component.html',
  styleUrls: ['./web-news-letter.component.scss']
})
export class WebNewsLetterComponent implements OnInit {
  subscribeEmailForm: FormGroup
  constructor (private fb: FormBuilder, private subscribeEmailService: SubscribeEmailService, private notificationService: NotificationService) {
    this.initForm();

  }

  initForm(){
    this.subscribeEmailForm = this.fb.group({
      email: ['', [Validators.required, Validators.email, Validators.pattern(this.subscribeEmailService.emailPattern)]]
    })
  }

  get f(){
    return this.subscribeEmailForm.controls['email'];
  }

  ngOnInit () {

  }

  subscribeEmail(){
      if(this.subscribeEmailForm.invalid){
        return false;
      }

      this.subscribeEmailService.subscribe_email(this.subscribeEmailForm.value)
      .subscribe(res => {
          this.notificationService.get_Notification('success', 'Email has been subscribed');
          this.subscribeEmailForm.reset();
      })
  }

}
