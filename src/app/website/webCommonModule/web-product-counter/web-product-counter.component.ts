import { AfterViewInit, Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

const VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  // tslint:disable-next-line: no-use-before-declare
  useExisting: forwardRef(() => WbnProductCounterComponent),
  multi: true
};
@Component({
  selector: 'NA-web-product-counter',
  templateUrl: './web-product-counter.component.html',
  styleUrls: ['./web-product-counter.component.scss'],
  providers: [VALUE_ACCESSOR]
})
export class WbnProductCounterComponent implements OnInit,AfterViewInit, ControlValueAccessor {
  @Input() disabled = false;
  @Input() computedBy = 1;
  constructor() { }
  textDisabled = true;
  quantity:number = 1;

  ngOnInit(): void {
    setTimeout(() => {
      this.textDisabled = true;
    }, 600);
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    if(this.computedBy > 1 && this.quantity == 1){
      this.quantity = this.computedBy;
    }
  }

  onChange = (value: any) => { return; };
  onTouched = (value: any) => { return; };
  setDisabledState = (isDisabled: boolean)  => {

  }

  writeValue(value: any): void {
    this.quantity = value || 0;
    this.onChange(this.quantity)
    return;
  };

  registerOnChange(fn: any): void {
    this.onChange = fn;
}

registerOnTouched(fn: any){
  this.onTouched = fn
}
  progress(type = 0){
    if(this.quantity != this.computedBy && type === 0){
      this.quantity = this.quantity - this.computedBy;
    }else if(type == 1) {
      this.quantity = +this.quantity + (+this.computedBy);
    }
    if(this.quantity < this.computedBy){
      this.quantity = this.computedBy;
    }
    this.onChange(this.quantity);
  }
}
