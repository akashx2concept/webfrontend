import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WbnProductCounterComponent } from './web-product-counter.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [WbnProductCounterComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [WbnProductCounterComponent]
})
export class WebProductCounterModule { }
