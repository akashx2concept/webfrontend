import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AddAddressComponent } from '../../commonModule/addAddress/addAddress.component';
import { ImageCropperComponent } from '../../commonModule/imageCropper/imageCropper.component';
import { AddressApiService } from '../../services/api/addressApi';
import { FavoriteVendorsApiService } from '../../services/api/favoriteVendorsApi.service';
import { OrderApiService } from '../../services/api/orderApi.service';
import { ProductApiService } from '../../services/api/productApi.service';
import { UserApiService } from '../../services/api/userApi.service';
import { CookiesService } from '../../services/commonServices/cookies.service';
import { NeighborhoodsApiService } from '../../services/neighborhoodsApi.service';
import { UserProfileEditComponent } from './user-profile-edit/user-profile-edit.component';

@Component({
  selector: 'NA-web-profile',
  templateUrl: './web-profile.component.html',
  styleUrls: ['./web-profile.component.scss']
})
export class WebProfileComponent implements OnInit {

  currentTab: string = 'order-history';

  public userForm;

  public loginUserData;

  public loginUser_profileImage = "../../../assets/img/web-icons/avatar-ellipse.svg";

  private confirmRef: BsModalRef;

  public favroiteVendor = [];
  public orderList = [];

  public addressArray = [];

  constructor (private cookiesService: CookiesService, private userApiService: UserApiService,
    private modalService: BsModalService, public neighborhoodsApiService: NeighborhoodsApiService,
    private favoriteVendorsApiService: FavoriteVendorsApiService, private productApiService: ProductApiService,
    private orderApiService: OrderApiService, private addressApiService: AddressApiService, private dialog: MatDialog ) {
    this.loginUserData = this.cookiesService.get_user_login_data();
  }

  ngOnInit(): void {
    this.method_for_getting_userData ();
    this.method_for_getting_favroite_vendor ();
    this.method_for_getting_order_data ();
    this.method_for_getting_Address ();
  }


  method_for_add_address (item?) {
    let initialState: any;
    if (item) initialState = { parentObj: { userId: this.loginUserData['id'], id: item['id'] } };
    else initialState = { parentObj: { userId: this.loginUserData['id'] } };
    this.confirmRef = this.modalService.show(AddAddressComponent, {class: 'modal-dialog-centered modal-md', initialState});
    this.confirmRef.content.action.subscribe((value) => {
        if (value){
            this.method_for_getting_Address();
        } else {

        }
    });
  }


  method_for_getting_Address () {
    var query:any = {
      where: {
        userId: this.loginUserData['id'],
        isActive: true,
        isDeleted: false
      },
      include: [
        { relation: 'country' },
        { relation: 'city' },
        { relation: 'state' },
      ]
    }
    query = encodeURI(JSON.stringify(query));
    this.addressApiService.get_Addresses(query).subscribe(
      data => {
        this.addressArray = data;
      }, error => {

      }
    )
  }


  method_for_getting_order_data () {
    let query:any = {
      where: {
        userId: this.loginUserData['id'],
        status: "OrderPlace"
      },
      include: [
        { relation: "vendors", scope: { include: [ {relation:"country"}, {relation:"city"}, {relation:"state"}, {relation: "categories"}, { relation: 'bannerImage',  scope: { include: [ { relation: "bannerImage" } ], order: "id Desc"}  } ] } },
        { relation: "orderItems", scope: { include: [ {relation: "products"} ] } },
        { relation: "orderAddresses" }
      ],
      order: "orderDate Desc",
      limit: 100
    };
    query = encodeURI(JSON.stringify(query));
    this.orderApiService.get_Orders(query).subscribe(
      data => {
        this.orderList = data;
      }, error => {

      }
    )
  }

  method_for_getting_favroite_vendor () {
    var query:any = {
      where: {
        userId: this.loginUserData['id']
      },
      include: [
        {
          relation: "vendors",
          scope: {
            include: [
              {relation:"country"},
              {relation:"city"},
              {relation:"state"},
              {relation: "categories"},
              {relation: "bannerImage", scope: { include: [{relation: "bannerImage"}] }},
              { relation: "favoriteVendors", scope: { where: { userId: this.loginUserData['id'] } } }
            ]
          }
        },
      ]
    }
    query = encodeURI(JSON.stringify(query));
    this.favoriteVendorsApiService.get_Vendors_FavoriteVendor(query).subscribe(
      data => {
        this.favroiteVendor = data;
        for (let q=0; q<=this.favroiteVendor.length-1; q++) {
          this.method_for_getting_price(this.favroiteVendor[q]['vendors']['id'])
        }
      }, error => {

      }
    )
  }

  method_for_getting_price (vendorId) {
    var query:any = { where: { vendorsId: vendorId }, order: "unitPrice ASC", limit: 1 };
    query = JSON.stringify(query);
    this.productApiService.Products_ProductPrice(query).subscribe(
      data => {
        if (data.length) {
          let priceData = data[0];
          for (var z=0; z<=this.favroiteVendor.length-1; z++) {
            if (this.favroiteVendor[z]['vendors']['id'] == priceData['vendorsId']) {
              this.favroiteVendor[z]['vendors']['priceForOne'] = priceData['unitPrice']
            }
          }
        }
      }, error => {

      }
    )
  }

  method_for_delete_favorite_vendor (favoriteVendors) {
    if (favoriteVendors.length) {
      let favData = favoriteVendors[0];
      this.favoriteVendorsApiService.delete_Vendors_FavoriteVendor(favData['id']).subscribe(
        data => {
          this.method_for_getting_favroite_vendor ();
        }, error => {

        }
      )
    }
  }

  method_for_getting_userData () {
    var query:any = {
      where: {
        id: this.loginUserData['id']
      },
      include: [ {relation: "profilePic"} ]
    }
    query = encodeURI(JSON.stringify(query));
    this.userApiService.get_users_findOne (query).subscribe(
      data => {
        this.loginUserData = data;
        if (this.loginUserData['profilePicId']) {
          this.loginUser_profileImage = `${this.neighborhoodsApiService.config.imageBaseUrl}${this.loginUserData['profilePic']['url']}`
        }
        this.cookiesService.set_user_login_data(this.loginUserData);
      }, error => {

      }
    )
  }


  method_for_upload_profile_pic () {
    let initialState: any;
    initialState = { parentObj: {  } };
    this.confirmRef = this.modalService.show(ImageCropperComponent, {class: 'modal-dialog-centered modal-lg', initialState});
    this.confirmRef.content.action.subscribe((value) => {
        if (value){
          console.log(value);
          if (value.length) {
            var createdData = {
              id: this.loginUserData['id'],
              profilePicId: value[0]['id']
            }
            this.method_for_update_user(createdData);
          }
        } else {

        }
    });
  }

  method_for_update_user (data) {
    this.userApiService.patch_users(data).subscribe(
      data => {
        this.method_for_getting_userData();
      }, error => {

      }
    )
  }

  editProfile(){
    this.dialog.open(UserProfileEditComponent, {
      width: '400px',
      data: {
        ...this.loginUserData
      }
    })
    .afterClosed()
    .subscribe(res => {
      if(res){
        this.method_for_update_user(res);
      }
    })

  }

}
