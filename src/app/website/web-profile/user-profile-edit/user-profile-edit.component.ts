import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'NA-user-profile-edit',
  templateUrl: './user-profile-edit.component.html',
  styleUrls: ['./user-profile-edit.component.scss']
})
export class UserProfileEditComponent implements OnInit {
  
  updateProfile: FormGroup
  constructor(
    private fb:FormBuilder,private dialogRef: MatDialogRef<UserProfileEditComponent>, @Inject(MAT_DIALOG_DATA) private data:any) {
    console.log(data);
    this.initForm();
  }

  ngOnInit(): void {

  }

  initForm(){
    this.updateProfile = this.fb.group({
      id: [''],
      firstName: ['', [Validators.required]],
      email: [{
        value: '',
        disabled:true
      }],
      lastName: [],
      middleName: []
    });
    this.updateProfile.patchValue(this.data);
  }

  get f(){
    return this.updateProfile.controls;
  }

  updateProfileData(){
    if(this.updateProfile.invalid){
      return false;
    }
    this.dialogRef.close(this.updateProfile.value);
    console.log(this.updateProfile.value);
  }

  onNoClick() {
    setTimeout (() => {
      this.dialogRef.close();
    })        
  }

}
