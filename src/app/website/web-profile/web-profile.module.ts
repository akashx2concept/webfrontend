import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { WebProfileComponent } from './web-profile.component';
import { MaterialModule } from '../../material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { UserProfileEditComponent } from './user-profile-edit/user-profile-edit.component';
import { FormDirModule } from '../../commonModule/form-dir/form-dir.module';


const routes: Routes = [
  {
    path : '',
    component: WebProfileComponent
  }
]
@NgModule({
  declarations: [
    WebProfileComponent,
    UserProfileEditComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormDirModule,
    RouterModule.forChild(routes)
  ],
  entryComponents: [UserProfileEditComponent]
})
export class WebProfileModule { }
