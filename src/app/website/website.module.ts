import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebsiteComponent } from './website.component';
import { RouterModule, Routes } from '@angular/router';
import { GoogleMapsModule } from '@angular/google-maps';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TermConditionComponent } from './termCondition/termCondition.component';
// Import web
import { WebHeaderComponent } from './webCommonModule/webHeader/webHeader.component';
import { WebFooterComponent } from './webCommonModule/webFooter/webFooter.component';
import { WebCartComponent } from './webCommonModule/web-cart/web-cart/web-cart.component';
import { WebAboutusComponent } from './webCommonModule/web-aboutus/web-aboutus/web-aboutus.component';
import { WebNewsLetterComponent } from './webCommonModule/web-newsLetter/web-news-letter/web-news-letter.component';
import { WebBannerComponent } from './webCommonModule/web-banner/web-banner/web-banner.component';
import { WebContainerComponent } from './webCommonModule/webContainer/web-container.component';
import { UserAuthModule } from './user-auth/user-auth.module';
import { AuthGuardService } from '../services/commonServices/auth-guard/auth-guard.service';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgOptionHighlightModule } from '@ng-select/ng-option-highlight';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AddAddressComponent } from '../commonModule/addAddress/addAddress.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { DebounceModule } from 'ngx-debounce';

const route:Routes =  [
    {
      path: '',
      component: WebsiteComponent,
      children: [
        {
          path: '',
          component: WebContainerComponent
        },
        {
          path: 'become-a-vendor',
          loadChildren: () => import('./become-a-vendor/become-a-vendor.module').then(m => m.BecomeAVendorModule)
        },
        {
          path: 'term-and-condition',
          loadChildren: () => import('./terms-and-condition/terms-and-condition.module').then(m => m.TermsAndConditionModule)
        },
        {
          path: 'team',
          loadChildren: () => import('./team/team.module').then(m => m.TeamModule)
        },
        {
          path: 'gallery',
          loadChildren:() => import('./gallery/gallery.module').then(m => m.GalleryModule)
        },
        {
          path: 'contact-us',
          loadChildren:() => import('./contact-us/contact-us.module').then(m => m.ContactUsModule)
        },
        {
          path: 'privacy-policy',
          loadChildren:() => import('./privacy-policy/privacy-policy.module').then(m => m.PrivacyPolicyModule)
        },
        {
          path: 'disclaimer',
          loadChildren: () => import('./disclaimer/disclaimer.module').then(m => m.DisclaimerModule)
        },
        {
          path: 'faq',
          loadChildren:() => import('./faq/faq.module').then(m => m .FaqModule)
        },
        {
          path: 'user-profile',
          loadChildren: () => import('./web-profile/web-profile.module').then(m => m.WebProfileModule)
        },
        {
          path: 'select-your-product/:id',
          loadChildren: () => import('./web-vendor/web-vendor.module').then(m => m.WebVendorModule),
          canActivate: [AuthGuardService],
        },
        {
          path: 'termsCondition',
          component: TermConditionComponent
        },
        {
          path: 'cart',
          loadChildren:() => import('./web-user-cart/web-user-cart.module').then(m => m.WebUserCartModule)
        },
        {
          path: "store",
          loadChildren: () => import('./allVendor/allVendor.modeule').then(m => m.AllVendorModule)
        },
        {
          path: "change-password",
          loadChildren:() => import('../home/change-password/change-password.module').then(m => m.ChangePasswordModule)
        },
        {
          path: 'forgot-password',
          loadChildren:() => import('./forgot-password/forgot-password.module').then(m => m.ForgotPasswordModule)
        },
        {
          path: 'forgotPassword/:token',
          loadChildren:() => import('./reset-password/reset-password.module').then(m => m.ResetPasswordModule)
        }
      ]
    }
]


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(route),
    GoogleMapsModule,
    FormsModule,
    MaterialModule,
    UserAuthModule,
    NgSelectModule,
    NgOptionHighlightModule,
    FormsModule, ReactiveFormsModule,
    ModalModule.forRoot(),
    ImageCropperModule,
    DebounceModule
  ],
  declarations: [
    WebsiteComponent,
    TermConditionComponent,
    WebHeaderComponent,
    WebFooterComponent,
    WebCartComponent,
    WebAboutusComponent,
    WebNewsLetterComponent,
    WebBannerComponent,
    WebContainerComponent,
    AddAddressComponent
  ],
  exports: [

  ]
})
export class WebsiteModule { }
