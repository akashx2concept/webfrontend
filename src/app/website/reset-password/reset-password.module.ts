import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, RouterModule, Routes } from '@angular/router';
import { ResetPasswordComponent } from './reset-password.component';
import { MaterialModule } from '../../material.module';
import { ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
{
  path: '',
  component: ResetPasswordComponent
}
]

@NgModule({
  declarations: [ResetPasswordComponent],
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ]
})
export class ResetPasswordModule { }
