import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserApiService } from '../../services/api/userApi.service';
import { NotificationService } from '../../services/commonServices/notification.service';

@Component({
  selector: 'NA-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  resetPasswordForm: FormGroup;
  token;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private userApiService: UserApiService,
    private notificationService: NotificationService
  ) {
    this.initForm();
  }

  initForm(){
    this.resetPasswordForm = this.fb.group({
      resetPasswordToken: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      userId: ['', [Validators.required]],
      newPassword: ['', [
        Validators.required, Validators.minLength(8),
          Validators.maxLength(25),
          Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{7,}')
      ]],
      confirmNewPassword:  ['',  [Validators.required, Validators.minLength(8),
        Validators.maxLength(25),
        Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{7,}')
      ]]
    }, { validators: this.checkPasswords })
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(res => {
      console.log(res.token);
      this.token = res.token;
      if(res.token){
        this.verifyResetPasswordToken(res.token);
      }
    })
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    const password = group.get('newPassword').value;
    const confirmPassword = group.get('confirmNewPassword').value;

    return password === confirmPassword ? null : { notSame: true }
  }


  verifyResetPasswordToken(token){
    const dto = {token};
    this.userApiService.post_verify_reset_password_token(dto).subscribe(res => {
      res.email = res.emailId;
      this.resetPasswordForm.patchValue(res);
    });
  }

  resetPassword(){
    if(this.resetPasswordForm.invalid){
      return true;
    }

    this.userApiService.post_reset_password(this.resetPasswordForm.value)
    .subscribe(res => {
      this.notificationService.get_Notification('success', 'Your password has been reset');
      this.cancel();
    })
  }

  cancel(){
    this.router.navigate(['/']);
  }

  get f() { return this.resetPasswordForm.controls; };

}

