import { Component, OnInit } from '@angular/core';
import { AdminSettingAPIService } from '../../services/api/adminSettingApi.service';

@Component({
  selector: 'NA-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.scss']
})
export class PrivacyPolicyComponent implements OnInit {

  public privacyPolicy;

  constructor ( private adminSettingAPIService: AdminSettingAPIService ) {

  }

  ngOnInit () {
    this.method_for_getting_privacyPolicy ();
  }



  method_for_getting_privacyPolicy () {
    
    this.adminSettingAPIService.get_Adminsetting_privacyPolicy().subscribe(
      data => {
       
          this.privacyPolicy = data['metaValue'];
       
      }, error => {

      }
    )
  }
}
