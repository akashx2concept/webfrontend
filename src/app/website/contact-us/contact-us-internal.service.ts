import { Injectable } from "@angular/core";
import { ContactUsService } from "../../services/api/contact-us.service";
import { ProductApiService } from "../../services/api/productApi.service";
import { SubscribeEmailService } from "../../services/api/subscribe-email.service";
import { UserApiService } from "../../services/api/userApi.service";
import { VendorsApiService } from "../../services/api/vendorsApi.service";





@Injectable({
    providedIn: 'root'
})


export class ContactUsInternalService {


    public filter:any = {
        search: null,
        skip: 0,
        page_limit: 10
    }


    public contactList = [];

    constructor (private contactUsService: ContactUsService) {

    }

    method_for_getting_list_of_contact_us () {
        var query:any = {

        };
        query = encodeURI(JSON.stringify(query));

        this.contactUsService.get_contact_us(query).subscribe(
            (data : any) => {
                this.contactList = data;
            }, error => {

            }
        )
    }


}
