import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes , RouterModule } from '@angular/router';
import { BreadcrumbModule } from '../../../commonModule/breadcrumb/breadcrumb.module';
import { ConfirmActionModule } from '../../../commonModule/confirm-action/confirm-action.module';
import { MaterialModule } from '../../../material.module';
import { ContactUsListComponent } from './contact-us-list.component';

const routes: Routes = [
  {
    path: '', component: ContactUsListComponent
  }
]



@NgModule({
  declarations: [
    ContactUsListComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    BreadcrumbModule,
    ConfirmActionModule,
    RouterModule.forChild(routes)
  ]
})
export class ContactUsListModule { }
// get_subscrie_email
