import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { ConfirmActionComponent } from '../../../commonModule/confirm-action/confirm-action.component';
import { ContactUsService } from '../../../services/api/contact-us.service';
import { ProductApiService } from '../../../services/api/productApi.service';
import { StripePaymentApiService } from '../../../services/api/stripePaymentApi.service';
import { CookiesService } from '../../../services/commonServices/cookies.service';
import { NotificationService } from '../../../services/commonServices/notification.service';
import { ContactUsInternalService } from '../contact-us-internal.service';


declare const Stripe;


@Component({
  selector: 'NA-contact-us-list',
  templateUrl: './contact-us-list.component.html',
  styleUrls: ['./contact-us-list.component.scss']
})
export class ContactUsListComponent implements OnInit {




  constructor(
    public contactUsService: ContactUsService,
    public contactUsInternalService: ContactUsInternalService,
    private notificationService: NotificationService,
    private dialog: MatDialog) {

   }

  ngOnInit(): void {
    this.contactUsInternalService.method_for_getting_list_of_contact_us();
  }

  deleteContactUs(item){
    this.contactUsService.delete_contact_us(item.id)
    .subscribe(res => {
      this.notificationService.get_Notification('success', 'Request has been deleted');
      this.contactUsInternalService.method_for_getting_list_of_contact_us();
    })
  }

}
