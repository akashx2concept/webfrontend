import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ContactUsService } from '../../services/api/contact-us.service';
import { NotificationService } from '../../services/commonServices/notification.service';

@Component({
  selector: 'NA-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {

  contactUsForm:FormGroup;

  userForm:any

  constructor(private fb:FormBuilder,
     private contactusService: ContactUsService,
     private notificationService: NotificationService) {
        this.initForm();
     }

  ngOnInit(): void {
  }

  initForm(){
    this.contactUsForm = this.fb.group({
      "firstName": ['', Validators.required],
      "lastName": ['', Validators.required],
      "email": ['', [ Validators.required, Validators.email,Validators.pattern(this.contactusService.emailPattern)]],
      "phoneNo": ['', [Validators.required, Validators.minLength(5), Validators.maxLength(17)]],
      "massage": ['', Validators.required]
    })
  }

  create_contact_us(){
    if(this.contactUsForm.invalid){
      return false;
    }
    this.contactusService.create_contact_us(this.contactUsForm.value)
    .subscribe(res => {
      this.notificationService.get_Notification('success', 'Your request has been submitted successfully. We will contact you soon!!!');
      this.contactUsForm.reset();
    })
  }

  get f(){
    return this.contactUsForm.controls;
  }

}
