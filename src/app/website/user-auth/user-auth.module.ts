import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserLoginComponent } from './user-login/user-login.component';
import { UserRegisterModule } from './user-register/user-register.module'
import { MaterialModule } from '../../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    UserLoginComponent
  ],
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule,
    RouterModule,
    MaterialModule,
    UserRegisterModule
  ],
  entryComponents:[UserLoginComponent],
  exports: [UserLoginComponent,UserRegisterModule]
})
export class UserAuthModule { }
