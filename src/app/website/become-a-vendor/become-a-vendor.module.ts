import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BecomeAVendorComponent } from './become-a-vendor.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';
import { VendorCreateComponent } from './vendor-create/vendor-create.component';
import { GoogleMapsModule } from '@angular/google-maps';
import { NgOptionHighlightModule } from '@ng-select/ng-option-highlight';
import { NgSelectModule } from '@ng-select/ng-select';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { ImageCropperModule } from 'ngx-image-cropper';
import { FormDirModule } from '../../commonModule/form-dir/form-dir.module';

const routes: Routes = [
  {
    path: '',
    component: BecomeAVendorComponent
  },
  {
    path: 'request-vendor',
    component: VendorCreateComponent,
    data: {
      email: ''
    }
  }
]

@NgModule({
  declarations: [
    BecomeAVendorComponent,
    VendorCreateComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    GooglePlaceModule,
    NgSelectModule,
    NgOptionHighlightModule,
    GoogleMapsModule,
    ImageCropperModule,
    FormDirModule,
    RouterModule.forChild(routes)
  ]
})
export class BecomeAVendorModule { }
