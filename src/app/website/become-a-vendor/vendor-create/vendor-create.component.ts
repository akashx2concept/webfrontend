import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthApiService } from '../../../services/api/authApi.service';
import { BecomeAVendorApiService } from '../../../services/api/becomeAVendorApi.service';
import { CategoriesApiService } from '../../../services/api/CategoriesApi.service';
import { CityApiService } from '../../../services/api/cityApi';
import { CountryApiService } from '../../../services/api/countryApi';
import { StateApiService } from '../../../services/api/stateApi';
import { UniqueEmaiService } from '../../../services/api/unique-email.service';
import { UserApiService } from '../../../services/api/userApi.service';
import { CookiesService } from '../../../services/commonServices/cookies.service';
import { NotificationService } from '../../../services/commonServices/notification.service';

@Component({
  selector: 'NA-vendor-create',
  templateUrl: './vendor-create.component.html',
  styleUrls: ['./vendor-create.component.scss']
})
export class VendorCreateComponent implements OnInit {
  step = 0;
  storeInformation: FormGroup;
  supplierInformation: FormGroup;

  public countryList = [];
  public stateList = [];
  public cityList = [];

  vaendorCategoriesList = []

  selectedCountry;
  selectedState;
  selectedCity;

  center: google.maps.LatLngLiteral = {lat: 24, lng: 12};
  zoom = 8;
  markerOptions: google.maps.MarkerOptions = {draggable: false};
  markerPositions: google.maps.LatLngLiteral[] = [];

  showMap = false;
  public width;

  public latitude;
  public longitude;

  public designationList = [
      { id: 1, name: "Business Owner" },
      { id: 2, name: "Authorized Representative" }
  ];

  public storeAddressType_List = [
      { id: 1, name: "Same as business address" },
      { id: 2, name: "New Address" },
      { id: 3, name: "No Physical Store" }
  ]

  constructor(
     private router: Router,
     private fb:FormBuilder,
     private activatedRoute: ActivatedRoute,
     private countryApiService: CountryApiService,
     private stateApiService: StateApiService,
     private cityApiService: CityApiService,
     private categoriesApiService: CategoriesApiService,
     private becomeAVendorApiService: BecomeAVendorApiService,
     private notificationService: NotificationService,
     private uniqueEmaiService: UniqueEmaiService,
     private cookiesService: CookiesService,
     private authApiService: AuthApiService,
     private userApiService: UserApiService
     ) {
      this.initForm()
      this.method_for_getting_country();
      this.method_for_getting_vendorCategories();
      }


  initForm(){
    this.supplierInformation = this.fb.group({
        "firstName": ['', [Validators.required]],
        "middleName": [''],
        "lastName": ['', [Validators.required]],
        "email": ['', [Validators.required, Validators.email, Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')]],
        "designation": ['', [Validators.required]],
        "userId": [],

    });
    this.storeInformation = this.fb.group({
        "businessName": ['', [Validators.required]],
        "venderMobileNo": ['', [Validators.required]],
        "aboutUs": ['', [Validators.required]],
        "RegistrationNo": [''],
        "businessAddress": ['', [Validators.required]],
        "businessGoogleAddress": ['', [Validators.required]],
        "postalCode": ['', [Validators.required]],
        "storeName": ['', [Validators.required]],
        "storeAddressType": ['', [Validators.required]],
        "storeAddress": ['', [Validators.required]],
        "latitude": ['', [Validators.required]],
        "longitude": ['', [Validators.required]],
        "urlSlug": [''],
        "currentStatus": ['Requested'],
        "countryId": ['', [Validators.required]],
        "stateId": ['', [Validators.required]],
        "cityId": ['', [Validators.required]],
        "categoriesId": ['', [Validators.required]]
    });
    this.f.setAsyncValidators([this.uniqueEmaiService.uniqueValidator.bind(this.uniqueEmaiService)]);
  }
  get f(){
    return this.supplierInformation.controls['email'];
  }

  ngOnInit(): void {
    this.activatedRoute.fragment.subscribe(res => {
      if(res){
        this.supplierInformation.get('email').setValue(res);
        if(this.validateEmail(res)){
          // this.supplierInformation.get('email').disable();
        }else{
          this.f.setAsyncValidators([this.uniqueEmaiService.uniqueValidator.bind(this.uniqueEmaiService)]);
        }
      }
    })
    const loginUserData = this.cookiesService.get_user_login_data();

    if (loginUserData) {
        loginUserData.userId = loginUserData.id;
        console.log(loginUserData);
        this.supplierInformation.patchValue(loginUserData);
        this.supplierInformation.disable();
        this.supplierInformation.get('designation').enable();
    }
  }

  validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}


  method_for_getting_country (searchVal?) {
    let query:any = {
        where: {
          isActive: true,
          isDeleted: false
        },
    }
    query = encodeURI(JSON.stringify(query));
    this.countryApiService.get_countries(query).subscribe(
        (data) => {
            this.countryList = data;
        }, (error) => {
          console.log(error)
        }
    )
}

method_for_change_in_country (value) {
    console.log(value);
    this.selectedCountry = value;
    let query:any = {
        where: {
            countryId: value['id'],
            isActive: true,
            isDeleted: false
        },
    }
    query = encodeURI(JSON.stringify(query));
    this.stateApiService.get_states(query).subscribe(
        data => {
            this.stateList = data;
        }, error => {

        }
    )
}

method_for_change_in_state (value) {
    console.log(value);
    this.selectedState = value;
    let query:any = {
        where: {
            stateId: value['id'],
            isActive: true,
            isDeleted: false
        },
    }
    query = encodeURI(JSON.stringify(query));
    this.cityApiService.get_cities(query).subscribe(
        data => {
            this.cityList = data;
        }, error => {

        }
    )
}

method_for_change_in_city (value) {
    this.selectedCity = value;
}

method_for_getting_vendorCategories () {
  let query = {
      where: {

      }
  }
  this.categoriesApiService.get_Categories(JSON.stringify(query)).subscribe(
      data => {
          this.vaendorCategoriesList = data;
      }, error => {

      }
  )
}

markerClicked (event) {
  console.log(event);
}

handleAddressChange (address) {
  console.log(address);
  this.markerPositions = [];
  this.center  = <google.maps.LatLngLiteral>{lat:address.geometry.location.lat(), lng:address.geometry.location.lng()};
  this.markerPositions.push({lat:address.geometry.location.lat(), lng:address.geometry.location.lng()});
  this.zoom = 12;
  this.latitude = address.geometry.location.lat();
  this.longitude = address.geometry.location.lng()
  this.storeInformation.patchValue({
      businessAddress: address['formatted_address'],
      businessGoogleAddress: address['formatted_address'],
      storeAddress: address['formatted_address'],
      latitude : this.latitude,
      longitude: this.longitude
  });
  console.log(this.latitude, "==== latitude");
  console.log(this.longitude, "==== longitude");
  console.log(this.storeInformation.value, "==== Vendor");
}
addMarker(event: google.maps.MapMouseEvent) {

}
  prevStep(val){
    this.step = val;
  }

  become_a_vendor(){
    if(this.storeInformation.invalid){
      this.prevStep(0);
      return false;
    }
    if(this.supplierInformation.invalid){
      this.prevStep(1);
      return false;
    }
    const query = {
      ...this.storeInformation.getRawValue(),
      ...this.supplierInformation.getRawValue()
    };
    this.becomeAVendorApiService.create_become_a_vendor(query)
    .subscribe(res => {
      this.notificationService.get_Notification('success', 'Request has been submitted successfully. Pending for approval.');
      if(query.userId){
        this.updateUserData();
      }else{
        this.router.navigate(['/']);
      }
      // this.router.navigate(['/']);
    });
  }


  updateUserData(){
    const data = {
      id: this.supplierInformation.getRawValue().userId,
      isVendorRequestSubmitted: true
    }
    this.userApiService.patch_users(data).subscribe(res => {
      this.authApiService.logout()
      .subscribe(r => {
        this.cookiesService.clearAll();
        this.router.navigate(['/']);
        setTimeout(()=>{
          window.location.reload();
        },300);
      })
    })
  }

  goToSupplierInfo(){
    if(this.supplierInformation.invalid){
      return true;
    }
    this.prevStep(1);
  }
}
