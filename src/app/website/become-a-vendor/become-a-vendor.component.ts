import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { debounceTime, map } from 'rxjs/operators';
import { UniqueEmaiService } from '../../services/api/unique-email.service';
import { CookiesService } from '../../services/commonServices/cookies.service';

@Component({
  selector: 'NA-become-a-vendor',
  templateUrl: './become-a-vendor.component.html',
  styleUrls: ['./become-a-vendor.component.scss']
})
export class BecomeAVendorComponent implements OnInit {
  emailForm: FormGroup;
  constructor(private route: Router,
     private activatedRoute: ActivatedRoute,
     private uniqueEmaiService: UniqueEmaiService,
     private fb : FormBuilder, private cookiesService: CookiesService) {
       this.initForm();
      }

  ngOnInit(): void {
    const loginUserData = this.cookiesService.get_user_login_data();

    if (loginUserData) {
      this.route.navigate(['request-vendor'],{
        relativeTo:this.activatedRoute
      })
    }
  }

initForm(){
  this.emailForm = this.fb.group({
    email: ['', [Validators.required, Validators.email, Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')]]
  });
  this.f.setAsyncValidators([this.uniqueEmaiService.uniqueValidator.bind(this.uniqueEmaiService)]);
}
  signUp(){
    if(this.emailForm.invalid){
      return false;
    }
    this.route.navigate(['request-vendor'],{
        relativeTo:this.activatedRoute,
        fragment: this.emailForm.value.email
    })
  }

  get f(){
    return this.emailForm.controls['email'];
  }


}
