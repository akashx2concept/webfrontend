import { Injectable } from "@angular/core";
import { ProductApiService } from "../../services/api/productApi.service";
import { UserApiService } from "../../services/api/userApi.service";
import { VendorsApiService } from "../../services/api/vendorsApi.service";
import { CookiesService } from "../../services/commonServices/cookies.service";




@Injectable({
    providedIn: 'root'
})


export class AllVendorListInternalService {

    public loginUserData;
    public vendorList = [];


    public searchVendor;


    constructor (private productApiService: ProductApiService, private vendorsApiService: VendorsApiService,
        private cookiesService: CookiesService, private userApiService: UserApiService) {

    }



    getVendorList(){
        this.loginUserData = this.cookiesService.get_user_login_data();

        var query:any = {
          where: {
            isActive:true,
            // stripeConnected: true
          },
          include: [
            { relation: 'bannerImage',  scope: { include: [ { relation: "bannerImage" } ], order: "id Desc"}  },
            { relation: 'logoImage' },
            { relation: 'categories' },
            { relation: 'products', scope: { where: { isActive: true, isDeleted: false, status: true }, limit: 1 } },
          ],

        }

        if (this.loginUserData) {
          query['include'].push( { relation: "favoriteVendors", scope: { where: { userId: this.loginUserData['id'] } } } )
        }

        if (this.searchVendor) {
            query['where'] = {
                and: [
                    {
                        isActive: true,
                    },
                    {
                        or: [
                            { businessName: { like: '%' + this.searchVendor + '%' }},
                        ]
                    }
                ]
            }

            // ['businessName'] = { like: `%${this.searchVendor}%` }
        }

        query = encodeURI(JSON.stringify(query));
        this.vendorsApiService.get_Vendors(query).subscribe(
          res => {
            console.log(res);
            this.vendorList  = res;
            if (this.loginUserData) {
              for (let q=0; q<=this.vendorList.length-1; q++) {
                this.method_for_getting_price(this.vendorList[q]['id'])
              }
            }
          }, error => {

          }
        )
      }


      method_for_getting_price (vendorId) {
          var query:any = { where: { vendorsId: vendorId }, order: "unitPrice ASC", limit: 1 };
          query = JSON.stringify(query);
          this.productApiService.Products_ProductPrice(query).subscribe(
            data => {
              if (data.length) {
                let priceData = data[0];
                for (var z=0; z<=this.vendorList.length-1; z++) {
                  if (this.vendorList[z]['id'] == priceData['vendorsId']) {
                    this.vendorList[z]['priceForOne'] = priceData['unitPrice']
                  }
                }
              }
            }, error => {

            }
          )
      }



}
