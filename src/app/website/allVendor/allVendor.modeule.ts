import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { MaterialModule } from "../../material.module";
import { AllVendorComponent } from "./allVendor.component";


const routes: Routes = [
    {
        path: '',
        component: AllVendorComponent
    }
]


@NgModule({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        MaterialModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        AllVendorComponent
    ],
    exports: [
        AllVendorComponent
    ]
})

export class AllVendorModule {

}