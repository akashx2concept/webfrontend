import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { CartItemApiService } from "../../services/api/cartItemApi.service";
import { CategoriesApiService } from "../../services/api/CategoriesApi.service";
import { FavoriteVendorsApiService } from "../../services/api/favoriteVendorsApi.service";
import { ProductApiService } from "../../services/api/productApi.service";
import { VendorsApiService } from "../../services/api/vendorsApi.service";
import { CookiesService } from "../../services/commonServices/cookies.service";
import { NotificationService } from "../../services/commonServices/notification.service";
import { NeighborhoodsApiService } from "../../services/neighborhoodsApi.service";
import { WebAuthInternalService } from "../service/webAuthInternal.service";
import { AllVendorListInternalService } from "./allVendorListInternal.service";



@Component({
    selector: 'NG-allVendor',
    templateUrl: './allVendor.component.html',
    styleUrls: [ './allVendor.component.scss' ]
})


export class AllVendorComponent implements OnInit {
   categoriesArray = [];
   selectedCategoriesId = 0
   vendorList = [];
   searchVendor = '';
   public loginUserData;
    constructor (private activatedRoute: ActivatedRoute, private categoriesApiService: CategoriesApiService,
      private productApiService: ProductApiService,
      private cartItemApiService: CartItemApiService, private cookiesService: CookiesService,
      private router: Router, private notificationService: NotificationService,
      private vendorsApiService: VendorsApiService,
      public allVendorListInternalService: AllVendorListInternalService,
      public neighborhoodsApiService: NeighborhoodsApiService,
      private favoriteVendorsApiService: FavoriteVendorsApiService,
      private webAuthInternalService: WebAuthInternalService,) {
        this.loginUserData = this.cookiesService.get_user_login_data();

    }

   ngOnInit(): void {
    // this.vendorGettingLIst();
    this.method_for_getting_vendors_categories();
  }


  vendorGettingLIst () {
    this.allVendorListInternalService.getVendorList();
  }

  method_for_getting_vendors_categories() {
    let query: any = {
      where: {
        type: "Vendor",
        isActive: true,
      },
      include: [
        {
          relation: "vendors",
          scope: {
            where: {
              isActive: true,
              isDeleted: false,
              status: true,
              stripeConnected: true
            },
            include: [
              { relation: 'bannerImage',  scope: { include: [ { relation: "bannerImage" } ], order: "id Desc"}  },
            { relation: 'logoImage' },
            { relation: 'categories' },
            { relation: 'products', scope: { where: { isActive: true, isDeleted: false, status: true }, limit: 1 } }
            ],
          },
        },
      ]
    };

    if (this.searchVendor) {
      query['include'][0]['scope']['where'] = {
          isActive: true,
          isDeleted: false,
          stripeConnected: true,
          and: [
              {
                  isActive: true,
              },
              {
                  or: [
                      { businessName: { like: '%' + this.searchVendor + '%' }},
                  ]
              }
          ]
      }

      // ['businessName'] = { like: `%${this.searchVendor}%` }
  }
  if (this.loginUserData) {
    query['include'][0]['scope']['include'].push( { relation: "favoriteVendors", scope: { where: { userId: this.loginUserData['id'] } } } )
  }
    query = encodeURI(JSON.stringify(query));
    this.categoriesApiService.get_Categories(query).subscribe(
      (data) => {
        let totalCount = 0;
        let totalVendors = [];
        this.categoriesArray = [];
        for (let a = 0; a <= data.length - 1; a++) {
          let d = {
            id: data[a]["id"],
            isActive: data[a]["isActive"],
            name: data[a]["name"],
            vendorCount: data[a]["vendors"].length,
            vendors: data[a]["vendors"],
            isSelected: false,
          };
          if (data[a]["vendors"].length) {
            totalVendors = totalVendors.concat(d["vendors"]);
            totalCount = totalCount + d["vendorCount"];
            this.categoriesArray.push(d);
          }
        }
        let allPush = {
          id: 0,
          isActive: true,
          name: "All",
          vendorCount: totalCount,
          vendors: totalVendors,
          isSelected: true,
        };
        this.selectedCategoriesId = 0;
        // this.method_For_getting_product();
        this.categoriesArray.unshift(allPush);
        this.vendorList = [...totalVendors];
        this.set_price();
      },
      (error) => {}
    );
  }

  select_vednors_on_the_basis_of_categories(category) {
    this.selectedCategoriesId = category.id;
    this.vendorList = [...category.vendors];
    this.set_price();
  }

  resetFilter(){
    this.searchVendor = '';
    this.method_for_getting_vendors_categories()
  }

  method_for_create_favroite_vendor (vendor) {
    if (!this.loginUserData) {
      this.webAuthInternalService.userLogin();
    } else {
      let dataDto = {
          vendorsId: vendor['id'],
          userId: this.loginUserData['id']
      };
      this.favoriteVendorsApiService.create_Vendors_FavoriteVendor(dataDto).subscribe(
        data => {
          this.method_for_getting_vendors_categories ();
        }, error => {

        }
      )
    }
  }


  method_for_delete_favorite_vendor (favoriteVendors) {
    if (favoriteVendors.length) {
      let favData = favoriteVendors[0];
      this.favoriteVendorsApiService.delete_Vendors_FavoriteVendor(favData['id']).subscribe(
        data => {
          this.method_for_getting_vendors_categories ();
        }, error => {

        }
      )
    }
  }

  set_price(){
    if (this.loginUserData) {
      for (let q=0; q<=this.vendorList.length-1; q++) {
        this.method_for_getting_price(this.vendorList[q]['id'])
      }
    }
  }

  method_for_getting_price (vendorId) {
    var query:any = { where: { vendorsId: vendorId }, order: "unitPrice ASC", limit: 1 };
    query = JSON.stringify(query);
    this.productApiService.Products_ProductPrice(query).subscribe(
      data => {
        if (data.length) {
          let priceData = data[0];
          for (var z=0; z<=this.vendorList.length-1; z++) {
            if (this.vendorList[z]['id'] == priceData['vendorsId']) {
              this.vendorList[z]['priceForOne'] = priceData['unitPrice']
            }
          }
        }
      }, error => {

      }
    )
}
}
