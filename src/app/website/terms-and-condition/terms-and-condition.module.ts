import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TermAndConditionComponent } from './term-and-condition.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: TermAndConditionComponent
  }
]

@NgModule({
  declarations: [
    TermAndConditionComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class TermsAndConditionModule { }
