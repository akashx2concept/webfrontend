import { Component, OnInit } from '@angular/core';
import { AdminSettingAPIService } from '../../services/api/adminSettingApi.service';

@Component({
  selector: 'NA-term-and-condition',
  templateUrl: './term-and-condition.component.html',
  styleUrls: ['./term-and-condition.component.scss']
})
export class TermAndConditionComponent implements OnInit {

  public termCondition;

  constructor ( private adminSettingAPIService: AdminSettingAPIService ) {

  }

  ngOnInit () {
    this.method_for_getting_termCondition ();
  }



  method_for_getting_termCondition () {
    
    this.adminSettingAPIService.get_Adminsetting_termAndCondition().subscribe(
      data => {
    
          this.termCondition = data['metaValue'];
      }, error => {

      }
    )
  }


}
