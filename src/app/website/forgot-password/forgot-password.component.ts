import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { UserApiService } from '../../services/api/userApi.service';
import { NotificationService } from '../../services/commonServices/notification.service';

@Component({
  selector: 'NA-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  forgotPasswordForm: FormGroup;
  emailPattern = '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';

  constructor(
    private dialgo: MatDialog, private fb: FormBuilder, private router: Router, private userApiService: UserApiService, private notificationSerivice: NotificationService) {
    this.forgotPasswordForm = undefined;
    this.initForm();
  }

  initForm(){
    this.forgotPasswordForm = this.fb.group({
      email: ['', [Validators.required,Validators.email, Validators.pattern(this.emailPattern)]]
    })
  }

  ngOnInit(): void {
    this.dialgo.closeAll();
  }



  cancel(){
    this.router.navigate(['/']);
  }

  forgotPassword(){
    if(this.forgotPasswordForm.invalid){
      return false;
    }

    this.userApiService.post_request_reset_password(this.forgotPasswordForm.value)
    .subscribe(res => {
      this.notificationSerivice.get_Notification('success', 'Please check email. If it is exist in our database you will get a reset password link.');
      this.cancel();
    })

  }

}
