import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { CartItemApiService } from "../../services/api/cartItemApi.service";
import { CategoriesApiService } from "../../services/api/CategoriesApi.service";
import { ProductApiService } from "../../services/api/productApi.service";
import { VendorsApiService } from "../../services/api/vendorsApi.service";
import { CookiesService } from "../../services/commonServices/cookies.service";
import { NotificationService } from "../../services/commonServices/notification.service";
import { NeighborhoodsApiService } from "../../services/neighborhoodsApi.service";

@Component({
  selector: "NA-web-vendor",
  templateUrl: "./web-vendor.component.html",
  styleUrls: ["./web-vendor.component.scss"],
})
export class WebVendorComponent implements OnInit {
  currentTab: string = "about";

  public categoriesArray = [];

  public userForm: any;

  public vendorId;
  public loginUserData;

  public productListArr = [];

  public selectedCategoriesId;

  public productSearch;

  public cartItemsArray = [];

  public cart_priceSubTotal = 0;
  public vendorData;

  constructor(
    private activatedRoute: ActivatedRoute, private categoriesApiService: CategoriesApiService,
    private productApiService: ProductApiService, private neighborhoodsApiService: NeighborhoodsApiService,
    private cartItemApiService: CartItemApiService, private cookiesService: CookiesService,
    private router: Router, private notificationService: NotificationService,
    private vendorsApiService: VendorsApiService
  ) {
    this.vendorId = this.activatedRoute.snapshot.params.id;

    this.loginUserData = this.cookiesService.get_user_login_data();
  }

  ngOnInit() {
    this.method_for_getting_product_on_bases_of_categories();
    this.method_For_getting_cartItem();
    this.method_for_getting_vendorsDetails ();
  }

  method_for_getting_vendorsDetails () {
    var query:any = {
      where: { id: this.vendorId },
      include: [ {relation:"country"}, {relation:"city"}, {relation:"state"}, {relation: "categories"} ]
    }
    query['include'].push( { relation: "favoriteVendors", scope: { where: { userId: this.loginUserData['id'] } } } )
    
    query = encodeURI(JSON.stringify(query));
    this.vendorsApiService.get_Vendors(query).subscribe(
      data => {
        if (data.length) {
          this.vendorData = data[0];
        }
      }, error => {
        
      }
    )
  }

  method_For_getting_product() {
    var query: any = {
      where: {
        vendorsId: this.vendorId,
        isActive: true,
        isDeleted: false,
        status: true,
        avaliableStock: {gt: "0"}
      },
      include: [
        { relation: "categories" },
        {
          relation: "productImages",
          scope: { include: [{ relation: "productImage" }] },
        },
        { relation: "productPrices" },
        { relation: "productTags" },
      ],
    };

    if (this.selectedCategoriesId != 0) {
      query["where"]["categoriesId"] = this.selectedCategoriesId;
    }

    if (this.productSearch) {
      query["where"]["productName"] = { like: `%${this.productSearch}%` };
    }

    query = encodeURI(JSON.stringify(query));
    this.productApiService.get_Products(query).subscribe(
      (data) => {
        this.productListArr = data;
      },
      (error) => {}
    );
  }

  method_for_select_categories(item) {
    for (let a = 0; a <= this.categoriesArray.length - 1; a++) {
      if (this.categoriesArray[a]["id"] == item["id"]) {
        this.categoriesArray[a]["isSelected"] = true;
        this.selectedCategoriesId = this.categoriesArray[a]["id"];
        this.method_For_getting_product();
      } else {
        this.categoriesArray[a]["isSelected"] = false;
      }
    }
  }

  method_for_getting_product_on_bases_of_categories() {
    let query: any = {
      where: {
        type: "Product",
        isActive: true,
      },
      include: [
        {
          relation: "products",
          scope: {
            where: {
              vendorsId: this.vendorId,
              isActive: true,
              isDeleted: false,
              status: true,
            },
          },
        },
      ],
    };
    query = encodeURI(JSON.stringify(query));
    this.categoriesApiService.get_Categories(query).subscribe(
      (data) => {
        let totalCount = 0;
        for (let a = 0; a <= data.length - 1; a++) {
          let d = {
            id: data[a]["id"],
            isActive: data[a]["isActive"],
            name: data[a]["name"],
            productCount: data[a]["products"].length,
            isSelected: false,
          };
          if (data[a]["products"].length) {
            totalCount = totalCount + d["productCount"];
            this.categoriesArray.push(d);
          }
        }
        let allPush = {
          id: 0,
          isActive: true,
          name: "All",
          productCount: totalCount,
          isSelected: true,
        };
        this.selectedCategoriesId = 0;
        this.method_For_getting_product();
        this.categoriesArray.unshift(allPush);
      },
      (error) => {}
    );
  }

  method_For_getting_cartItem() {
    var query: any = {
      where: {
        userId: this.loginUserData["id"],
      },
      include: [
        { relation: "user" },
        { relation: "vendors" },
        {
          relation: "products",
          scope: {
            include: [
              { relation: "productPrices" },
              {
                relation: "productImages",
                scope: { include: [{ relation: "productImage" }] },
              },
            ],
          },
        },
      ],
    };
    query = encodeURI(JSON.stringify(query));
    this.cartItemApiService.get_CartItems(query).subscribe(
      (data) => {
        this.cartItemsArray = data;
        this.cart_priceSubTotal = 0;

        if (this.cartItemsArray.length) {
          for (let q = 0; q <= this.cartItemsArray.length - 1; q++) {
            console.log(this.cartItemsArray[q]["products"]);
            if (this.cartItemsArray[q]["products"]["productPrices"].length) {
              this.cart_priceSubTotal = this.cart_priceSubTotal + parseInt(this.cartItemsArray[q]["products"]["productPrices"][0]["unitPrice"]) * parseInt(this.cartItemsArray[q]['quantity']);
            }
          }
        }
      },
      (error) => {}
    );
  }

  method_for_add_item_in_cart (productId) {
    let query:any = {
      where: {
        userId: this.loginUserData['id'],
        vendorsId: this.vendorId,
        productsId: productId
      }
    }
    query = encodeURI(JSON.stringify(query));
    this.cartItemApiService.get_CartItems(query).subscribe(
      data => {
        if (data.length) {
          let qwer = data[0];
          let dataDto = {  id: qwer['id'], quantity: parseInt(qwer['quantity']) + 1, userId: this.loginUserData['id'], vendorsId: this.vendorId, productsId: productId }
          this.cartItemApiService.update_CartItems(dataDto).subscribe(
            data => {
              this.method_For_getting_cartItem();
            }, error => {
      
            })
        } else {
          let dataDto = { quantity: "1", userId: this.loginUserData['id'], vendorsId: this.vendorId, productsId: productId }
          this.cartItemApiService.create_CartItems(dataDto).subscribe(
            data => {
              this.method_For_getting_cartItem();
            }, error => {
      
            })
        }
      }, error => {

    })  
  }

  method_for_add_remove_item_quenty_from_card (value, item) {
    if (value == 'minus') {
      item['quantity'] = parseInt(item['quantity']) - 1;
      if (item['quantity'] == 0) this.method_for_remove_item_from_cart(item.id);
      else this.update_card_item(item);
    } else if (value == 'add') {
      item['quantity'] = parseInt(item['quantity']) + 1;
      this.update_card_item(item);
    }
  }

  update_card_item (dataDto) {
    this.cartItemApiService.update_CartItems(dataDto).subscribe(
      data => {
        this.method_For_getting_cartItem();
      }, error => {

      }
    )
  }


  method_for_remove_item_from_cart (id) {
    this.cartItemApiService.delete_CartItems(id).subscribe(
      data => {
        this.notificationService.get_Notification('success', "Successfully remove item from the Cart");
        this.method_For_getting_cartItem();
      }, error => {

      }
    )
  }

  method_for_routing() {
    this.router.navigate(["cart"]);
  }
}
