import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebVendorComponent } from './web-vendor.component';
import { RouterModule, Routes } from '@angular/router';
import { MaterialModule } from '../../material.module';
import { WebProductCounterModule } from '../webCommonModule/web-product-counter/web-product-counter.module';
import { DebounceModule } from 'ngx-debounce';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


const routes: Routes = [
  {
    path: '',
    component: WebVendorComponent
  }
]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    WebProductCounterModule,
    DebounceModule,
    FormsModule, ReactiveFormsModule
  ],
  declarations: [
    WebVendorComponent
  ],
})
export class WebVendorModule { }
