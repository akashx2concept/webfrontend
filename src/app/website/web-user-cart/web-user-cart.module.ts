import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebUserCartComponent } from './web-user-cart.component';
import { WebProductCounterModule } from '../webCommonModule/web-product-counter/web-product-counter.module';
import { RouterModule, Routes } from '@angular/router';
import { MaterialModule } from '../../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxStripeModule } from 'ngx-stripe';
import { CustomerPaymentComponent } from '../../commonModule/customerPayment/customerPayment.component';

const routes: Routes = [
  {
    path: '',
    component: WebUserCartComponent
  }
]

@NgModule({
  declarations: [
    WebUserCartComponent,
    CustomerPaymentComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    WebProductCounterModule,
    MaterialModule, 
    FormsModule, ReactiveFormsModule,
    NgxStripeModule.forRoot('pk_live_51IdCiXE1e1bineRZuVr8b8ye2k7QJIUJzsg5ep1cZIZTG3b3htE6pDr7smwkdjCc3dhCMpkmfeQZSez3YtZ1vRNM0045okudo7'),
  ],
  entryComponents: [
    CustomerPaymentComponent
  ]
})
export class WebUserCartModule { }
