import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AddAddressComponent } from '../../commonModule/addAddress/addAddress.component';
import { AddressApiService } from '../../services/api/addressApi';
import { CookiesService } from '../../services/commonServices/cookies.service';
import { NotificationService } from '../../services/commonServices/notification.service';
import { NeighborhoodsApiService } from '../../services/neighborhoodsApi.service';
import * as _ from "lodash";
import { StripePaymentApiService } from '../../services/api/stripePaymentApi.service';
import { MatDialog } from '@angular/material/dialog';
import { CustomerPaymentComponent } from '../../commonModule/customerPayment/customerPayment.component';
import { CartItemApiService } from '../../services/api/cartItemApi.service';

@Component({
  selector: 'NA-web-user-cart',
  templateUrl: './web-user-cart.component.html',
  styleUrls: ['./web-user-cart.component.scss']
})


export class WebUserCartComponent implements OnInit {



  public loginUserData;
  public cartItemsArray = [];
  public addressArray = [];

  public cart_priceSubTotal = 0;
  public cart_grandTotal = 0;

  private confirmRef: BsModalRef;

  public delivery_address:any;

  public paymentCard_data = [];


  constructor ( private cookiesService: CookiesService, private cartItemApiService: CartItemApiService,
    public neighborhoodsApiService: NeighborhoodsApiService, private addressApiService: AddressApiService,
    private modalService: BsModalService, private notificationService: NotificationService,
    private stripePaymentApiService: StripePaymentApiService, public dialog: MatDialog ) {
    this.loginUserData = this.cookiesService.get_user_login_data();
  }

  ngOnInit () {
    this.method_For_getting_cartItem();
    this.method_for_getting_Address();
    this.StripePayments_getStripeCustomerCard();
  }


  method_for_getting_Address () {
    var query:any = {
      where: {
        userId: this.loginUserData['id'],
        isActive: true,
        isDeleted: false
      },
      include: [
        { relation: 'country' },
        { relation: 'city' },
        { relation: 'state' },
      ]
    }
    query = encodeURI(JSON.stringify(query));
    this.addressApiService.get_Addresses(query).subscribe(
      data => {
        this.addressArray = data;
        this.delivery_address = (data.filter(add => add.isDefault ))[0]
      }, error => {

      }
    )
  }


  method_for_delete_address (item) {
    let dataDto = {
      id: item['id'],
      isActive: false,
      isDeleted: true
    }
    this.addressApiService.update_Addresses(dataDto).subscribe(
      data => {
        this.method_for_getting_Address();
      }, error => {

      }
    )
  }

  method_for_add_address (item?) {
    let initialState: any;
    if (item) initialState = { parentObj: { userId: this.loginUserData['id'], id: item['id'] } };
    else initialState = { parentObj: { userId: this.loginUserData['id'] } };
    this.confirmRef = this.modalService.show(AddAddressComponent, {class: 'modal-dialog-centered modal-md', initialState});
    this.confirmRef.content.action.subscribe((value) => {
        if (value){
            this.method_for_getting_Address();
        } else {

        }
    });
  }

  method_for_making_default_address (value, item) {
    console.log(value);
    console.log(item);
    if (value) {
      this.delivery_address = item;
      for (let q=0; q<=this.addressArray.length-1; q++) {
        if (item.id == this.addressArray[q]['id']) this.addressArray[q]['isDefault'] = true;
        else this.addressArray[q]['isDefault'] = false;
      }
    } else {
      this.delivery_address = null;
      for (let q=0; q<=this.addressArray.length-1; q++) {
          this.addressArray[q]['isDefault'] = false;
      }
    }
    // this.method_for_getting_Address ();
  }




  method_For_getting_cartItem () {
    var query:any = {
      where: {
        userId: this.loginUserData['id']
      },
      include: [
        { relation: "user" },
        { relation: "vendors" },
        { relation: "products", scope: { include: [{ relation: "productPrices" }, { relation: "productImages", scope: { include: [ { relation: "productImage" }] }} ] }},
      ]
    }
    query = encodeURI(JSON.stringify(query));
    this.cartItemApiService.get_CartItems(query).subscribe(
      data => {
        this.cartItemsArray = data;
        this.cart_priceSubTotal = 0;
        this.cart_grandTotal = 0;

        if (this.cartItemsArray.length) {
          for (let q=0; q<=this.cartItemsArray.length-1; q++) {
            console.log(this.cartItemsArray[q]['products']);
            if (this.cartItemsArray[q]['products']['productPrices'].length) {
              this.cart_priceSubTotal = this.cart_priceSubTotal + parseInt(this.cartItemsArray[q]['products']['productPrices'][0]['unitPrice']) * parseInt(this.cartItemsArray[q]['quantity']);
              this.cart_grandTotal = this.cart_grandTotal + parseInt(this.cartItemsArray[q]['products']['productPrices'][0]['sellingPrice']) * parseInt(this.cartItemsArray[q]['quantity']);
            }
          }
        }
      }, error => {

      }
    )
  }


  method_for_add_remove_item_quenty_from_card (value, item) {
    if (value == 'minus') {
      item['quantity'] = parseInt(item['quantity']) - 1;
      if (item['quantity'] == 0) this.method_for_delete_cardItem(item.id);
      else this.update_card_item(item);
    } else if (value == 'add') {
      item['quantity'] = parseInt(item['quantity']) + 1;
      this.update_card_item(item);
    }
  }

  update_card_item (dataDto) {
    this.cartItemApiService.update_CartItems(dataDto).subscribe(
      data => {
        this.method_For_getting_cartItem();
      }, error => {

      }
    )
  }

  method_for_delete_cardItem (id) {
    this.cartItemApiService.delete_CartItems(id).subscribe(
      data => {
        this.method_For_getting_cartItem();
      }, error => {

      }
    )

  }



  method_for_checkout () {
    let dataDto = {
      userId: this.loginUserData['id'],
      // userCardId: 'card_1J7OukE1e1bineRZ35CiH4kF',
      vendorId: 1,
      addressId: null,
      product: [],
      deliveryType: 'Delivery'
    }
    console.log(this.cartItemsArray);

    if (!this.delivery_address) {
      this.notificationService.get_Notification('info', "Please select address for delivery");
      return false
    }
    dataDto['addressId'] = this.delivery_address['id'];

    for (let a=0; a<=this.cartItemsArray.length-1; a++) {
      let obj = {
        productId: this.cartItemsArray[a]['productsId'],
        quantity: this.cartItemsArray[a]['quantity'],
        productPrices: this.cartItemsArray[a]['products']['productPrices'][0]['id']
      }
      dataDto.product.push(obj)
    }

    let uniqueVendor = _.uniqBy(this.cartItemsArray,'vendorsId');
    console.log(uniqueVendor, " --uniqueVendor")

    if (uniqueVendor.length == 1) {
      this.openDialog (dataDto)
    } else {
      this.notificationService.get_Notification('info', "Item present in the cart are not belong to same store please order from 1 store at a time")
    }


  }

  openDialog(data): void {

    const dialogRef = this.dialog.open(CustomerPaymentComponent, {
      width: '350px',
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      setTimeout(() => {
        this.method_For_getting_cartItem ();
      }, 1000)

      // this.notificationService.get_Notification("success", "Payment Successfully")
      // this.animal = result;
    });
  }


  StripePayments_getStripeCustomerCard () {
    var dataDto = {
      userId: this.loginUserData['id']
    }
    this.stripePaymentApiService.StripePayments_getStripeCustomerCard (dataDto).subscribe(
      data => {
        this.paymentCard_data = data['data'];
      }, error => {

      }
    )
  }

}
