import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

import { IconModule, IconSetModule, IconSetService } from '@coreui/icons-angular';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import { AppComponent } from './app.component';

// Import containers
// import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
// import { LoginComponent } from './views/login/login.component';
// import { RegisterComponent } from './views/register/register.component';

// const APP_CONTAINERS = [
//   DefaultLayoutComponent
// ];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';

// Import routing module
import { AppRoutingModule } from './app.routing';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts';
import { MaterialModule } from './material.module';
import { NotifierModule } from 'angular-notifier';
import { customNotifierOptions } from './services/commonServices/notificationConfig';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpErrorInterceptor } from './services/commonServices/httpInterceptor.service';
import { LoaderModule } from './commonModule/loader/loader.module';
import { FormDirModule } from './commonModule/form-dir/form-dir.module';

// import { NgxStripeModule } from 'ngx-stripe';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MaterialModule,
    NotifierModule.withConfig(customNotifierOptions),
    HttpClientModule,
    LoaderModule,
    FormDirModule
    // NgxStripeModule.forRoot('pk_test_51IdCiXE1e1bineRZmQBEy9nIRvX8O2rry6UsxQjvxLjS0EliBzUXy0eYuBboFlyL4WlyxfpqiZS8FhTBA8NxCMte005tOlGUUH'),
    // AppAsideModule,
    // AppBreadcrumbModule.forRoot(),
    // AppFooterModule,
    // AppHeaderModule,
    // AppSidebarModule,
    // PerfectScrollbarModule,
    // BsDropdownModule.forRoot(),
    // TabsModule.forRoot(),
    // ChartsModule,
    // IconModule,
    // IconSetModule.forRoot(),
  ],
  declarations: [
    AppComponent,
    // ...APP_CONTAINERS,
    P404Component,
    P500Component,
    // LoginComponent,
    // RegisterComponent,
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: PathLocationStrategy,
    },
    { provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true },
    IconSetService,
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
