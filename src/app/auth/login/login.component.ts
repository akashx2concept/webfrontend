import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from "@angular/forms";
import { ErrorStateMatcher } from "@angular/material/core";
import { AuthApiService } from "../../services/api/authApi.service";
import { CookiesService } from "../../services/commonServices/cookies.service";
import { GlobalDataService } from "../../services/commonServices/globalData.service";
import { NotificationService } from "../../services/commonServices/notification.service";
import { RoutingHandlingService } from "../../services/commonServices/routingHandling.service";

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
      const isSubmitted = form && form.submitted;
      return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}


@Component({
    selector: 'NA-login',
    templateUrl: './login.component.html',
    styleUrls: [ './login.component.scss' ]
})


export class LoginComponent implements OnInit {


    hide = true;

    public loginForm: FormGroup;
    public matcher = new MyErrorStateMatcher();


    constructor (
      private routingHandlingService: RoutingHandlingService, 
      private formBuilder: FormBuilder,
        private authApiService: AuthApiService,
         private cookiesService: CookiesService,
        private notificationService: NotificationService, 
        public globalDataService: GlobalDataService 
        ) {

    }

    ngOnInit () {
        this.validateForm ();
    }

    validateForm () {
        this.loginForm = this.formBuilder.group({
          email: ['', [Validators.required, Validators.email]],
          password: ['', [Validators.required, Validators.minLength(8),
            Validators.maxLength(25),
            Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{7,}')
          ]],
          remenberMe:[false],
        })
    }

    get f() { return this.loginForm.controls; };


    method_for_Login () {
        console.log(this.loginForm);
        if(this.loginForm.invalid){
          return false;
        }
        const loginDto = {
          email: this.loginForm.value['email'],
          password: this.loginForm.value['password']
        }

        this.authApiService.login(loginDto).subscribe(
            data => {
              this.cookiesService.set_user_token(data['id']);
              var dataDto = {id: data.userId}
              this.globalDataService.method_for_getting_login_user_data(dataDto);
              this.notificationService.get_Notification('success', 'Successfully Logged In');
              // this.method_for_redirect('homeDashboardModule');
            }, error => {
              this.notificationService.get_Notification('error', "Email password doesn't match");
            }
        )
    }




    method_for_redirect (value) {
        this.routingHandlingService.method_for_redirect(value);
    }


}
