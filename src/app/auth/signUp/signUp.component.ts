import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from "@angular/forms";
import { ErrorStateMatcher } from "@angular/material/core";
import { AuthApiService } from "../../services/api/authApi.service";
import { CookiesService } from "../../services/commonServices/cookies.service";
import { GlobalDataService } from "../../services/commonServices/globalData.service";
import { NotificationService } from "../../services/commonServices/notification.service";
import { RoutingHandlingService } from "../../services/commonServices/routingHandling.service";


/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
      const isSubmitted = form && form.submitted;
      return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}

@Component({
    selector: 'NA-signUp',
    templateUrl: './signUp.component.html',
    styleUrls: [ './signUp.component.scss' ]
})

export class SignUpComponent implements OnInit {

    public signUpForm:FormGroup;

    public matcher = new MyErrorStateMatcher();
    public hide = true;

    public signUpUserData;

    constructor (private formBuilder: FormBuilder, private notificationService: NotificationService,
        private authApiService: AuthApiService, private cookiesService: CookiesService,
        private routingHandlingService: RoutingHandlingService, private globalDataService: GlobalDataService ) {

    }

    ngOnInit () {
        this.validateForm();
    }


    validateForm(){
        this.signUpForm = this.formBuilder.group({
          firstName: ['', Validators.required],
          lastName: ['', Validators.required],
          email: ['', [Validators.required, Validators.email]],
          password: ['', [Validators.required, Validators.minLength(8),
            Validators.maxLength(25),
            Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{7,}')
          ]],
          termAndCondition:[false, Validators.required],
        })
    }

    get f() { return this.signUpForm.controls; };

    method_for_signUp () {
        if(this.signUpForm.invalid){
          return false;
        }
        if (!this.signUpForm.value['termAndCondition']) {
          this.notificationService.get_Notification('info', 'Please agree to our terms & conditions and privacy policy');
          return false;
        }

        // var signUpObj = {
        //     firstName: this.signUpForm.value['firstName'],
        //     lastName: this.signUpForm.value['lastName'],
        //     email: this.signUpForm.value['email'],
        //     password: this.signUpForm.value['password'],
        //     isEmailSubscribe: this.signUpForm.value['isEmailSubscribe'],
        //     role: "Root"
        // }
        // console.log(signUpObj)

        // this.authApiService.SignUp(signUpObj).subscribe(
        //     data => {
        //         this.signUpUserData = data;
        //         this.notificationService.get_Notification('success', 'Account successfully created');
        //         this.method_for_loginUser(signUpObj.email, signUpObj.password);
        //     }, error =>{

        //     }
        // )
    }



    method_for_loginUser (email, password) {
        var login_Obj = {
          email: email,
          password: password,
        }
        this.authApiService.login(login_Obj).subscribe(
          data => {
            this.cookiesService.set_user_token(data['id']);
            const dataDto = {id: data.userId}
            this.globalDataService.method_for_getting_login_user_data(dataDto);
            // this.routingHandlingService.method_for_redirect('homeDashboardModule');
          }, error => {
            this.notificationService.get_Notification('error', error.error.error.message);
          }
        )
    }

}
