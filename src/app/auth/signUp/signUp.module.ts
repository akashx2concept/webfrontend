import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { MaterialModule } from "../../material.module";
import { SignUpComponent } from "./signUp.component";



const routes: Routes = [
    {
        path: '',
        component: SignUpComponent
    }
]



@NgModule ({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        RouterModule.forChild(routes),
        MaterialModule
    ],
    declarations: [
        SignUpComponent
    ],
    exports: [
        SignUpComponent
    ]
})


export class SignUpModule {


}