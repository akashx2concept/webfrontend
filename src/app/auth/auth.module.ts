import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { AuthComponent } from './auth.component';
import { AuthHeaderComponent } from './auth-header/auth-header.component';
import { AppHeaderModule } from "@coreui/angular";


const routes: Routes = [
    {
        path: '',
        component: AuthComponent,
        children: [
            {
                path: 'login',
                loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
            },
            {
                path: 'signUp',
                loadChildren: () => import('./signUp/signUp.module').then(m => m.SignUpModule)
            }
        ]
    }
]


@NgModule({
    imports: [
        CommonModule,
        AppHeaderModule,
        FormsModule, ReactiveFormsModule,
        RouterModule.forChild (routes)
    ],
    declarations: [
        AuthComponent,
        AuthHeaderComponent
    ],
    exports: [
        AuthComponent
    ]
})

export class AuthModule {

}
