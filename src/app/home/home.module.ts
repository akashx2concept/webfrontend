import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./home.component";

import {
    AppAsideModule,
    AppBreadcrumbModule,
    AppHeaderModule,
    AppFooterModule,
    AppSidebarModule,
  } from '@coreui/angular';
import { HeaderSideBarComponent } from "../commonModule/headerSideBar/headerSideBar.component";
import { FooterComponent } from "../commonModule/footer/footer.component";

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { IconModule, IconSetModule, IconSetService } from '@coreui/icons-angular';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts';
import { BreadcrumbComponent } from "../commonModule/breadcrumb/breadcrumb.component";
// import { AddAddressComponent } from "../commonModule/addAddress/addAddress.component";

import { ModalModule } from 'ngx-bootstrap/modal';
import { MaterialModule } from "../material.module";

import { NgSelectModule } from '@ng-select/ng-select';
import { NgOptionHighlightModule } from '@ng-select/ng-option-highlight';
import { AddCategoriesComponent } from "../commonModule/addCategories/addCategories.component";
import { ImageCropperModule } from 'ngx-image-cropper';
import { ImageCropperComponent } from "../commonModule/imageCropper/imageCropper.component";
import { AuthGuardService } from "../services/commonServices/auth-guard/auth-guard.service";
import { BreadcrumbModule } from "../commonModule/breadcrumb/breadcrumb.module";
import { isMetaProperty } from "typescript";
import { AddCategoriesRequestsComponent } from "../commonModule/addCategoriesRequests/addCategoriesRequests.component";
import { FormDirModule } from "../commonModule/form-dir/form-dir.module";


const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        children: [
            {
                path: 'dashboard',
                loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),
                canActivate: [AuthGuardService],
            },
            {
                path: 'customer',
                loadChildren: () => import('./customers/customers.module').then(m => m.CustomersModule),
                canActivate: [AuthGuardService],
            },
            {
                path: 'vendor',
                loadChildren: () => import('./vendors/vendors.module').then(m => m.VendorsModule),
                canActivate: [AuthGuardService],
            },
            {
                path: 'profile',
                loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule),
                canActivate: [AuthGuardService],
            },
            {
              path: 'change-password',
              loadChildren: () => import('./change-password/change-password.module').then(m => m.ChangePasswordModule)
            },
            {
              path: 'payments',
              loadChildren: () => import('./payments/payments.module').then(m => m.PaymentsModule)
            },
            {
              path: 'sales-report',
              loadChildren: () => import('./sales-report/sales-report.module').then(m => m.SalesReportModule)
            },
            {
                path: 'categories',
                loadChildren: () => import ('./categories/categories.module').then(m => m.CategoriesModule),
                canActivate: [AuthGuardService],
            },
            {
              path: 'product/:vendorId',
              loadChildren: () => import('./product/product.module').then(m => m.ProductModule),
              canActivate: [AuthGuardService],
            },
            {
              path: 'setting',
              loadChildren:() => import('./setting/setting.module').then(m => m.SettingModule),
              canActivate: [AuthGuardService]
            },
            {
              path: 'subscribe-email',
              loadChildren:() => import('./subscribe-email/subscribe-email.module').then(m => m.SubscribeEmailModule)
            },
            {
              path: 'contact-us',
              loadChildren:() => import('../website/contact-us/contact-us-list/contact-us-list.module').then(m => m.ContactUsListModule)
            },
            {
              path: 'order',
              loadChildren: () => import('./order/order.module').then(m => m.OrderModule),
              canActivate: [AuthGuardService]
            }
        ]
    },
    {
        path: '',
        component: HomeComponent,
        children: [
            {
                path: 'vendor-detail',
                loadChildren:() => import('./vendors/vendor-detail/vendor-detail.module').then(m => m.VendorDetailModule),
                canActivate: [AuthGuardService],
            },
            {
                path: 'addVendors/:userId',
                loadChildren: () => import ('./vendors/addVendors/addVendors.module').then(m => m.AddVendorsModule),
                canActivate: [AuthGuardService],
            },
            {
                path: 'addVendors/:userId/:venderId',
                loadChildren: () => import ('./vendors/addVendors/addVendors.module').then(m => m.AddVendorsModule),
                canActivate: [AuthGuardService],
            },
            {
                path: 'productList',
                loadChildren: () => import('./product/product.module').then(m => m.ProductModule),
                canActivate: [AuthGuardService],
            },
            {
              path: 'categories-request',
              loadChildren: () => import ('./vendors/categories-requests/categories-requests.module').then(m => m.CategoriesRequestsModule),
              canActivate: [AuthGuardService]
            },
            {
                path: 'order',
                loadChildren: () => import('./order/order.module').then(m => m.OrderModule),
                canActivate: [AuthGuardService]
            },
            {
                path: 'payments',
                loadChildren: () => import('./payments/payments.module').then(m => m.PaymentsModule)
            },
            {
                path: 'change-password',
                loadChildren: () => import('./change-password/change-password.module').then(m => m.ChangePasswordModule)
            },
        ]
    },

]

@NgModule({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        RouterModule.forChild(routes),
        AppAsideModule,
        AppBreadcrumbModule.forRoot(),
        AppFooterModule,
        AppHeaderModule,
        AppSidebarModule,
        PerfectScrollbarModule,
        BsDropdownModule.forRoot(),
        TabsModule.forRoot(),
        ChartsModule,
        IconModule,
        IconSetModule.forRoot(),
        ModalModule.forRoot(),
        MaterialModule,
        NgSelectModule,
        NgOptionHighlightModule,
        ImageCropperModule,
        BreadcrumbModule,
        FormDirModule
    ],
    declarations: [
        HomeComponent,
        HeaderSideBarComponent,
        FooterComponent,
        // AddAddressComponent,
        AddCategoriesComponent,
        AddCategoriesRequestsComponent,
        ImageCropperComponent
    ],
    exports: [
        HomeComponent,
        HeaderSideBarComponent,
        FooterComponent,
        BreadcrumbComponent,
        // AddAddressComponent,
        AddCategoriesComponent,
        AddCategoriesRequestsComponent,
        ImageCropperComponent
    ]
})

export class HomeModule {

}
