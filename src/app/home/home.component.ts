import { Component, OnInit } from "@angular/core";
import { CookiesService } from "../services/commonServices/cookies.service";
import { Role_Root, Role_Vendor } from "../_nav";




@Component({
    selector: 'NA-home',
    templateUrl: './home.component.html',
    styleUrls: [ './home.component.scss' ]
})


export class HomeComponent implements OnInit {

    public sidebarMinimized = false;

    public loginUserData;

    public navItems = [];

    constructor (private cookiesService: CookiesService ) {

    }

    ngOnInit() {

        this.loginUserData = this.cookiesService.get_user_login_data();
        if (this.loginUserData['role'] == 'Root') {
            this.navItems = Role_Root;
        } else if (this.loginUserData['role'] == 'Vendor')  {
            this.navItems = Role_Vendor;
        }

    }


    toggleMinimize(e) {
        this.sidebarMinimized = e;
    }
}