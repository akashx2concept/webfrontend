import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, FormsModule, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AdminSettingAPIService } from '../../services/api/adminSettingApi.service';
import { NotificationService } from '../../services/commonServices/notification.service';


@Component({
  selector: 'NA-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {
  public counter = 0;
  settingForm:FormGroup;
  config = {
    height: '200',
    resize_enabled: 'true',
    resize_maxHeight: '500',
    resize_dir: 'vertical',
    removeButtons: 'Undo,Redo,Superscript,Subscript,Indent,Outdent,Cut,Copy,Paste,PasteText,PasteFromWord,-',
    // extraPlugins: 'divarea',
    removePlugins: 'elementspath,wsc,scayt,link,sourcearea,image,about,tabletools,table,horizontalrule,specialchar,maximize,stylescombo,format,blockquote,list'
};
  constructor(private settingApi: AdminSettingAPIService,
     private fb: FormBuilder,
     private  notificationService: NotificationService,
     private router: Router) {
    this.initForm();
    this.get_admin_setting();
  }

  ngOnInit(): void {
  }

  initForm(){
    this.settingForm = this.fb.group({
      transactionCharge: this.metData('transactionCharge'),
      commissionRate: this.metData('commissionRate'),
      enableCommission: this.metData('enableCommission'),
      termCondition: this.metData('termCondition'),
      privacyPolicy:this.metData('privacyPolicy')
    })
  }

  metData(key):FormGroup {
    return this.fb.group(
      {
        "id": [],
        "metaKey": [key],
        "metaValue": ['', Validators.required],
       }
    )
  }

  get_admin_setting(){
    const dto = {}
    this.settingApi.get_admin_setting(JSON.stringify(dto)).subscribe(res => {
      if(res.length){
        this.set_setting(res);
      }
    });
  }

  set_setting(res){
     const self = this;
     res.forEach(element => {
       console.log(element)
        this.settingForm.get([element['metaKey']]).patchValue(element);
     });
  }


  ValidateSetting(){

     if(this.settingForm.invalid){
       return false;
     }
     const data = this.settingForm.value;
     console.log(data);
     for(let k in data){
       console.log('####', data[k])
       this.createUpdateSetting(data[k]);
     }
  }

  createUpdateSetting(res){
    const API = res.id ? this.settingApi.update_admin_setting : this.settingApi.create_admin_setting;
    API.bind(this.settingApi)(res).subscribe(rec => {
      this.settingForm.get([rec['metaKey']]).patchValue(rec);
        this.counter++;
        if(this.counter == 5){
            this.notificationService.get_Notification('success', 'Admin Setting has been updated');
            this.counter = 0;
            // this.router.navigate(['/home/dashboard']);
        }
    });
  }

}
