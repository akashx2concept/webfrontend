import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingComponent } from './setting.component';
import { RouterModule, Routes } from '@angular/router';
import { BreadcrumbModule } from '../../commonModule/breadcrumb/breadcrumb.module';
import { MaterialModule } from '../../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NaCkeditorModule } from '../../commonModule/na-ckeditor/na-ckeditor.module';
import { CKEditorModule } from 'ngx-ckeditor';


const route: Routes = [
  {
    path: '',
    component: SettingComponent
  }

]

@NgModule({
  declarations: [
    SettingComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(route),
    BreadcrumbModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    NaCkeditorModule,
    CKEditorModule
  ]
})
export class SettingModule { }
