import { Injectable, OnInit } from "@angular/core";
import { OrderApiService } from "../../services/api/orderApi.service";
import { CookiesService } from "../../services/commonServices/cookies.service";
import { PaginationService } from "../../services/commonServices/pagination.service";



@Injectable({
    providedIn: 'root'
})


export class OrderInternalService  {

    public filter:any = {
        search: null,
        skip: 0,
        page_limit: 10,
        type: null
    }
    public vendorId;
    public orderList = [];
    public orderListCount = 0;

    constructor (private orderApiService: OrderApiService, public paginationService: PaginationService, private cookiesService: CookiesService) {

    }



    method_for_making_dto_filter () {
        console.log(this.filter, 'filter');
        // this.loginUserData = this.cookiesService.get_user_login_data(true);
        this.method_for_getting_count(this.filter);
    }


    method_for_getting_count (filter) {
        var query:any = {

        }
        const vendor = this.cookiesService.get_current_vendor_details();
        if(vendor && vendor.id){
          query['vendorsId'] = vendor.id;
        }

        query = encodeURI(JSON.stringify(query));

        this.orderApiService.get_Orders_count(query).subscribe(
            data => {
                this.orderListCount = data['count'];
                this.paginationService.set_count_data(data['count'], filter['skip']);
                this.method_for_getting_order(filter);
            }, error => {

            }
        )
    }




    method_for_getting_order (filter) {

        var query:any = {
          where:{},
            include: [
                { relation: 'user' },
                { relation: 'vendors' },
                { relation: 'orderItems', scope: { include: [ {relation: "products" } ] } }
            ],
        }

        const vendor = this.cookiesService.get_current_vendor_details();
        if(vendor && vendor.id){
          query['where']['vendorsId'] = vendor.id;
        }

        query['order'] = "createdAt DESC";
        query['limit'] = this.paginationService.selected_drop_down_data;
        query['skip'] = this.paginationService.skip_data;
        if(this.vendorId){
          query['where'] = {
            vendorsId: this.vendorId
          }
        }

        query = JSON.stringify(query);

        this.orderApiService.get_Orders(query).subscribe(
            data => {
                this.orderList = data;
            }, error => {

            }
        )

    }




}
