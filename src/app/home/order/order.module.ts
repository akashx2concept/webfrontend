import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderComponent } from './order.component';
import { RouterModule, Routes } from '@angular/router';
import { BreadcrumbModule } from '../../commonModule/breadcrumb/breadcrumb.module';
import { MaterialModule } from "../../material.module";

const routes:Routes = [
  {
    path: '',
    component: OrderComponent,
    children:[
      {
        path:'manage-order',
        loadChildren:() => import('./manage-order/manage-order.module').then(m => m.ManageOrderModule)
      }
    ]
  }
]

@NgModule({
  declarations: [
    OrderComponent
  ],
  imports: [
    CommonModule,
    BreadcrumbModule,
    RouterModule.forChild(routes),
    MaterialModule,
  ]
})
export class OrderModule { }
