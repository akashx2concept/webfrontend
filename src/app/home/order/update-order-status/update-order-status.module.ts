import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateOrderStatusComponent } from './update-order-status.component';
import { MaterialModule } from '../../../material.module';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    UpdateOrderStatusComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule
  ],
  exports: [UpdateOrderStatusComponent],
  entryComponents: [UpdateOrderStatusComponent]
})
export class UpdateOrderStatusModule { }
