import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { OrderApiService } from '../../../services/api/orderApi.service';
import { NotificationService } from '../../../services/commonServices/notification.service';

export interface DialogData {
  deliveryStatus: string;
  id: string;
}

@Component({
  selector: 'NA-update-order-status',
  templateUrl: './update-order-status.component.html',
  styleUrls: ['./update-order-status.component.scss']
})
export class UpdateOrderStatusComponent implements OnInit {
  deliveryStatus = '';

  constructor(
    public dialogRef: MatDialogRef<UpdateOrderStatusComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private orderApiService: OrderApiService,
    private notificationService: NotificationService) {
        this.deliveryStatus = '' + data.deliveryStatus;
    }

  ngOnInit(): void {
  }

  changeStatus(value){
      console.log(value);
      const reqObj = {id: this.data.id, deliveryStatus: value};
      this.orderApiService.orderStatus(reqObj).subscribe(
        data => {
          console.log(data);
          this.notificationService.get_Notification('success', data['massage']);
          this.dialogRef.close(true);
        }, error => {
          this.notificationService.get_Notification('error', error.error.massage);
        }
      )
      // if(this.data.deliveryStatus === 'Delivered' && value === 'Confirmed'){
      //   this.notificationService.get_Notification('error', 'Status could not change from delivered to Confirmed');
      //   return false;
      // }
      // if(this.data.deliveryStatus === value){
      //   this.notificationService.get_Notification('error', 'Please change status to update');
      //   return false;
      // }
      // if(value){
      //   const reqObj = {id: this.data.id, deliveryStatus: value}
      //   this.orderApiService.update_order_status(reqObj).subscribe(res => {
      //     this.notificationService.get_Notification('success', 'Order status has been updated successfully');
      //     this.dialogRef.close(true);
      //   })
      // }
  }

}
