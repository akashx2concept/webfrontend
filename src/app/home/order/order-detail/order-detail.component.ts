import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderApiService } from '../../../services/api/orderApi.service';
import { UpdateOrderStatusComponent } from '../update-order-status/update-order-status.component';

@Component({
  selector: 'NA-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.scss']
})
export class OrderDetailComponent implements OnInit {


  public orderId;
  public orderDetails:any;
  public orderItems = [];

  constructor (
    private activatedRoute: ActivatedRoute,
    private orderApiService: OrderApiService,
    private dialog: MatDialog, private router: Router) {

    this.orderId = this.activatedRoute.snapshot.paramMap.get('orderId');


   }

  ngOnInit(): void {
    this.method_for_getting_orderDetails()
  }


  method_for_redirect () {
    // this.router.navigate(['home/order/manage-order']);
    window.history.back();
  }


  method_for_getting_orderDetails () {
    var query:any = {
      where: {
        id: this.orderId
      },
      include: [
          { relation: 'user' },
          { relation: 'vendors' },
          { relation: 'orderItems' },
          { relation: 'orderAddresses' },
          { relation: 'paymentDetails' },
      ]
    }


    query = JSON.stringify(query);

    this.orderApiService.get_Orders(query).subscribe(
        data => {
            if (data.length) {
              this.orderDetails = JSON.parse(JSON.stringify(data[0]));
              this.orderItems = this.orderDetails['orderItems'];
              console.log(JSON.parse(this.orderDetails['paymentDetails'][0]['paymentObj']))
            }
        }, error => {

        }
    )
  }

  changeOrderStatus(){
    this.dialog.open(UpdateOrderStatusComponent, {
      data: {
        deliveryStatus : this.orderDetails.deliveryStatus,
        id: this.orderId
      },
      width: '320px'
    }).afterClosed().subscribe(res => {
      if(res){
        this.method_for_getting_orderDetails();
      }
    })
  }


  method_for_view_bill () {
    let data = {
      orderId: this.orderId
    }
    this.orderApiService.viewOrderBill(data).subscribe(
      data => {
        if (data && data['data'] && data['data'].length) {
          let url = data.data[0]['receipt_url'];;
          window.open(url, '_blank');
        }
      }, error => {

      }
    )
  }

}
