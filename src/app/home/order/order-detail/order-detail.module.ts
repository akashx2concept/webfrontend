import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderDetailComponent } from './order-detail.component';
import { BreadcrumbModule } from '../../../commonModule/breadcrumb/breadcrumb.module';
import { RouterModule, Routes } from '@angular/router';
import { MaterialModule } from '../../../material.module';
import { FormsModule } from '@angular/forms';
import { UpdateOrderStatusModule } from '../update-order-status/update-order-status.module';

const routes: Routes = [
  {
    path: "",
    component: OrderDetailComponent
  }
]


@NgModule({
  declarations: [
    OrderDetailComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    BreadcrumbModule,
    RouterModule.forChild(routes),
    MaterialModule,
    UpdateOrderStatusModule
  ]
})
export class OrderDetailModule { }
