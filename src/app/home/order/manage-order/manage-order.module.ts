import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageOrderComponent } from './manage-order.component';
import { BreadcrumbModule } from '../../../commonModule/breadcrumb/breadcrumb.module';
import { RouterModule, Routes } from '@angular/router';
import { DebounceModule } from 'ngx-debounce';
import { MaterialModule } from '../../../material.module';

const route: Routes = [
  {
    path: '',
    component: ManageOrderComponent
  },
  {
    path: ":orderId",
    loadChildren: () => import('../order-detail/order-detail.module').then(m => m.OrderDetailModule)
  }

]


@NgModule({
  declarations: [
    ManageOrderComponent
  ],
  imports: [
    CommonModule,
    BreadcrumbModule,
    RouterModule.forChild(route),
    DebounceModule,
    MaterialModule
  ]
})
export class ManageOrderModule { }
