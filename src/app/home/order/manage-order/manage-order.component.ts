import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserApiService } from '../../../services/api/userApi.service';
import { VendorsApiService } from '../../../services/api/vendorsApi.service';
import { CookiesService } from '../../../services/commonServices/cookies.service';
import { PaginationService } from '../../../services/commonServices/pagination.service';
import { OrderInternalService } from '../orderInternal.service';


@Component({
  selector: 'NA-manage-order',
  templateUrl: './manage-order.component.html',
  styleUrls: ['./manage-order.component.scss']
})


export class ManageOrderComponent implements OnInit {

  public params;
  public currentPageNo:any = 1;

  constructor( private activatedRoute: ActivatedRoute, public orderInternalService: OrderInternalService,
    private vendorsApiService: VendorsApiService,
    private router: Router, public paginationService: PaginationService,
    private cookiesService: CookiesService ) {

  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(res => {
      console.log(res);
      this.params = res;
    });
    const vendor = this.cookiesService.get_current_vendor_details();

    if(vendor && vendor.id){
      this.orderInternalService.vendorId = vendor.id;
      this.method_for_getting_orderList();
    }else{
      this.activatedRoute.queryParams.subscribe(res => {
        const vendorId = res.vendor;
        this.orderInternalService.vendorId = vendorId;
        this.method_for_getting_orderList();
      });
    }

  }

  ngOnDestroy(): void {
    this.orderInternalService.vendorId = undefined;
  }

  pageChanged (event) {
    this.currentPageNo = event;
    this.paginationService.paginationConfig.currentPage = event;
    this.orderInternalService.filter.skip = (this.orderInternalService.filter.page_limit * this.paginationService.paginationConfig.currentPage) - this.orderInternalService.filter.page_limit;
    this.method_for_getting_orderList();
  }

  method_for_change_pagination_dropdown () {
      setTimeout(() => {
        this.paginationService.paginationConfig.currentPage = 1
        this.currentPageNo = 1;
        this.orderInternalService.filter.skip = 0;
        this.paginationService.selected_drop_down_data = this.paginationService.paginationConfig.itemsPerPage;
        this.method_for_getting_orderList();
      })
  }


  method_for_change_in_filter() {
      this.paginationService.paginationConfig.currentPage = 1;
      this.orderInternalService.filter.skip  = 0;
      this.method_for_getting_orderList();
  }

  method_for_getting_orderList () {
    this.orderInternalService.method_for_making_dto_filter();
  }


  method_for_redirecting (value, item) {
    console.log(item);
    if (value == "user") {
      this.router.navigate(['home/customer/customer-detail', item.userId]);
    } else if (value == "vendor") {
      const query = {
        where: {
          id: item.vendorsId
        }
      }
      this.vendorsApiService.Vendors_findOne(JSON.stringify(query))
      .subscribe(res => {
        if(res.userId){
          this.router.navigate(['home/vendor/vendor-detail', res.userId]);
        }
      });

    } else if (value == "viewDetails") {
      if(this.router.url.includes('vendor/order/manage-order')){
        this.router.navigate(['vendor/order/manage-order', item.id]);
      }else{
        this.router.navigate(['home/order/manage-order', item.id]);
      }

    }
  }


}
