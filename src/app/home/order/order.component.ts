import { Component, OnInit } from "@angular/core";
import { PaginationService } from "../../services/commonServices/pagination.service";
import { CustomerInternalService } from "../customers/customerInternalService";

@Component({
  selector: 'NA-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  constructor(
    public customerIntrnalService: CustomerInternalService, 
    public paginationService: PaginationService

  ) { }

  ngOnInit(): void {
  }

}
