import { Component, OnInit } from '@angular/core';
import { DashboardInternalService } from '../../dashboard/dashboardInternal.service';

@Component({
  selector: 'NA-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss']
})
export class BarChartComponent implements OnInit {
// barChart
public barChartOptions: any = {
  scaleShowVerticalLines: false,
  responsive: true
};
public barChartLabels: string[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
public barChartType = 'bar';
public barChartLegend = true;

public barChartData: any[] = [
  // {data: [65, 59, 80, 81, 56, 55, 40, 81, 56, 55, 40, 80], label: 'Current Year'},
  // {data: [28, 48, 40, 19, 86, 27, 90, 28, 48, 40, 70, 120], label: 'Previous Year'}
];

public barChartColours: Array<any> = [
  { // dark grey
    backgroundColor: '#FCC015'
  },
  { // grey
    backgroundColor: 'rgb(73, 211, 204)'
  },
  {
    backgroundColor: "#2f353a"
  }

];
  constructor(private dashboardInternalService: DashboardInternalService) { }

  ngOnInit(): void {

    Promise.all([this.dashboardInternalService.method_for_orderSalesDashboardGraph()]).then((res) => {

      this.method_for_making_graph(res[0]);
    })


  }

  method_for_making_graph (data) {
    this.barChartLabels = data['lineChartLabels'];
    this.barChartData = [
      {data: data['lineTotalOrderPlaced'], label: 'Order Placed'},
      {data: data['lineTotalDeliveredOrder'], label: 'Delivered Order'},
      {data: data['lineTotalCancelOrder'], label: 'Cancel Order'},
    ];
  }



  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }
}
