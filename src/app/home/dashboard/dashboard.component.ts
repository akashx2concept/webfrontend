import { Component, OnInit } from "@angular/core";
import { DashboardInternalService } from "./dashboardInternal.service";




@Component({
    selector: 'NA-dashboard',
    templateUrl: 'dashboard.component.html',
    styleUrls: [ './dashboard.component.scss' ]
})


export class DashboardComponent implements OnInit {


    constructor (public dashboardInternalService: DashboardInternalService) {

    }

    ngOnInit () {

        this.method_for_getting_data ();
    }

    method_for_getting_data () {
        this.dashboardInternalService.method_for_getting_data();
    }

    
}