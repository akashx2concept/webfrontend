import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { BreadcrumbModule } from "../../commonModule/breadcrumb/breadcrumb.module";
import { ProductCategoryRequestListModule } from "../categories/productCategories/product-category-request/product-category-request-list/product-category-request-list.module";
import { VendorCategoryRequestListModule } from "../categories/venderCategories/vendor-category-request/vendor-category-request-list/vendor-category-request-list.module";
import { ChartJSModule } from "../chartjs/chartjs.module";
import { VendorRequestListModule } from "../vendors/vendor-request/vendor-request-list/vendor-request-list.module";
import { DashboardComponent } from "./dashboard.component";


const routes: Routes = [
    {
        path: '',
        component: DashboardComponent
    }
]


@NgModule ({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        BreadcrumbModule,
        VendorRequestListModule,
        VendorCategoryRequestListModule,
        ProductCategoryRequestListModule,
        ChartJSModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        DashboardComponent
    ],
    exports: [
        DashboardComponent
    ]
})

export class DashboardModule {

}
