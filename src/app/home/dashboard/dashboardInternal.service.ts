import { Injectable } from "@angular/core";
import { CategoriesApiService } from "../../services/api/CategoriesApi.service";
import { ProductApiService } from "../../services/api/productApi.service";
import { UserApiService } from "../../services/api/userApi.service";



@Injectable({
    providedIn: 'root'
})


export class DashboardInternalService {


    public totalVendorCount = 0;
    public totalUserCount = 0;
    public totalProductCount = 0;
    public totalVendorCategoriesCount = 0;
    public totalProductCategoriesCount = 0;

    constructor (private userApiService: UserApiService, private productApiService: ProductApiService, private categoriesApiService: CategoriesApiService) {

    }


    method_for_VendorProductDashboardGraph () {
        return new Promise((resolve, reject) => {
            let qu = {};
            this.userApiService.VendorProductDashboardGraph(qu).subscribe(
                data => {
                    // console.log(data);
                    resolve(data);
                }, error => {

                }
            )
        })        
    }

    method_for_orderSalesDashboardGraph () {
        return new Promise ((resolve, reject) => {
            var que = {};
            this.userApiService.orderSalesDashboardGraph(que).subscribe(
                data => {
                    resolve(data);
                }, error => {

                }
            )
        })
    }

    method_for_getting_data () {
        this.method_for_getting_total_vendors();
        this.method_for_getting_total_user();
        this.method_for_getting_total_products();
        this.method_for_getting_total_VendorCategories();
        this.method_for_getting_total_ProductCategories();
    }

    method_for_getting_total_vendors () {
        var query:any = { isActive: true,  role: 'Vendor'}
        query = encodeURI(JSON.stringify(query));
        this.userApiService.get_userCount(query).subscribe(
            data => {
                this.totalVendorCount = data['count'];
            }, error => {

            }
        )
    }

    method_for_getting_total_user () {
        var query:any = { isActive: true,  role: 'User'}
        query = encodeURI(JSON.stringify(query));
        this.userApiService.get_userCount(query).subscribe(
            data => {
                this.totalUserCount = data['count'];
            }, error => {

            }
        )
    }

    method_for_getting_total_products () {
        var query:any = { isActive: true };
        query = encodeURI(JSON.stringify(query));
        this.productApiService.get_Products_count(query).subscribe(
            data => {
                this.totalProductCount = data['count'];
            }, error => {

            }
        )
    }


    method_for_getting_total_VendorCategories () {
        var query:any = { isActive: true, type: "Vendor" };
        query = encodeURI(JSON.stringify(query));
        this.categoriesApiService.get_Categories_count(query).subscribe(
            data => {
                this.totalVendorCategoriesCount = data['count'];
            }, error => {

            }
        )
    }

    method_for_getting_total_ProductCategories () {
        var query:any = { isActive: true, type: "Product" };
        query = encodeURI(JSON.stringify(query));
        this.categoriesApiService.get_Categories_count(query).subscribe(
            data => {
                this.totalProductCategoriesCount = data['count'];
            }, error => {

            }
        )
    }


}