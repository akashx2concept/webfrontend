import { Injectable } from "@angular/core";
import { CategoriesApiService } from "../../../services/api/CategoriesApi.service";
import { UserApiService } from "../../../services/api/userApi.service";
import { PaginationService } from "../../../services/commonServices/pagination.service";




@Injectable({
    providedIn: 'root'
})


export class ProductCategoriesInternalService {

    public filter:any = {
        search: null,
        skip: 0,
        page_limit: 10,
        type: null
    }

    public productVendorCount = 0;
    public productVendorCategorieList = []

    constructor (private userApiService: UserApiService, private paginationService: PaginationService,
        private categoriesApiService: CategoriesApiService ) {

    }


    method_for_making_dto_filter () {
        console.log(this.filter, 'filter');
        // this.loginUserData = this.cookiesService.get_user_login_data(true);
        this.method_for_getting_count(this.filter);
    }


    method_for_getting_count (filter) { 
        var query:any = {
            isActive: true,
            type: this.filter.type
        }

        query = encodeURI(JSON.stringify(query));

        this.categoriesApiService.get_Categories_count(query).subscribe(
            data => {
                this.productVendorCount = data['count'];
                this.paginationService.set_count_data(data['count'], filter['skip']);
                this.method_for_getting_list_of_categories(filter);
            }, error => {

            }
        )
    }


    method_for_getting_list_of_categories (filter) {
        var query:any = {
            where: {
                isActive: true,
                type: this.filter.type
            },
            include: [
                {relation: 'images'}
            ]
        };
        
        query['order'] = "createdAt DESC";
        query['limit'] = this.paginationService.selected_drop_down_data;
        query['skip'] = this.paginationService.skip_data;

        query = encodeURI(JSON.stringify(query));

        this.categoriesApiService.get_Categories(query).subscribe(
            data => {
                this.productVendorCategorieList = data;
            }, error => {

            }
        )

    }
  

    
}