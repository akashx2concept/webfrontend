import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { BreadcrumbModule } from "../../../commonModule/breadcrumb/breadcrumb.module";
import { FormDirModule } from "../../../commonModule/form-dir/form-dir.module";
import { MaterialModule } from "../../../material.module";
import { ProductCategoryRequestComponent } from "./product-category-request/product-category-request.component";
import { ProductCategoryRequestModule } from "./product-category-request/product-category-request.module";
import { ProductCategoriesComponent } from "./productCategories.component";


const routes: Routes = [
    {
        path: '',
        component: ProductCategoriesComponent
    },
    {
      path: 'product-category-request',
      component:  ProductCategoryRequestComponent
    }
]


@NgModule ({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        MaterialModule,
        RouterModule.forChild(routes),
        BreadcrumbModule,
        ProductCategoryRequestModule,
        FormDirModule
    ],
    declarations: [
        ProductCategoriesComponent
    ],
    exports: [
        ProductCategoriesComponent
    ]
})

export class ProductCategoriesModule {

}
