import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductCategoryRequestComponent } from './product-category-request.component';
import { BreadcrumbModule } from '../../../../commonModule/breadcrumb/breadcrumb.module';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { MaterialModule } from '../../../../material.module';
import { ProductCategoryRequestListModule } from './product-category-request-list/product-category-request-list.module';
import { FormDirModule } from '../../../../commonModule/form-dir/form-dir.module';


// const routes: Routes = [
//    {
//      path: '',
//      component: ProductCategoryRequestComponent
//    }
// ]

@NgModule({
  declarations: [
    ProductCategoryRequestComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    BreadcrumbModule,
    ProductCategoryRequestListModule,
    FormDirModule,
    RouterModule
    // RouterModule.forChild(routes)
  ],
  exports:[ProductCategoryRequestComponent]
})
export class ProductCategoryRequestModule { }
