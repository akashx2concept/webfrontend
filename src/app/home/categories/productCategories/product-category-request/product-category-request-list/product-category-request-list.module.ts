import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductCategoryRequestListComponent } from './product-category-request-list.component';
import { MaterialModule } from '../../../../../material.module';




@NgModule({
  declarations: [
    ProductCategoryRequestListComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports:[ProductCategoryRequestListComponent]
})
export class ProductCategoryRequestListModule { }
