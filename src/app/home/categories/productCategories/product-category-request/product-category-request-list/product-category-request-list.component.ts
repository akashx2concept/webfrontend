import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CategoriesApiService } from '../../../../../services/api/CategoriesApi.service';
import { CategoriesRequestsApiService } from '../../../../../services/api/CategoriesRequestsApi.service.';
import { CookiesService } from '../../../../../services/commonServices/cookies.service';
import { NotificationService } from '../../../../../services/commonServices/notification.service';
import { NeighborhoodsApiService } from '../../../../../services/neighborhoodsApi.service';

@Component({
  selector: 'NA-product-category-request-list',
  templateUrl: './product-category-request-list.component.html',
  styleUrls: ['./product-category-request-list.component.scss']
})
export class ProductCategoryRequestListComponent implements OnInit {
  venderList = [];
  hasLoaded = false;
  haslistRefreshSubscription: Subscription;
  logedInUser;
  constructor(
        private cookiesService: CookiesService,
        private categoriesRequestsApiService: CategoriesRequestsApiService,
        public neighborhoodsApiService: NeighborhoodsApiService,
        private categoriesApiService: CategoriesApiService,
        private notificationService: NotificationService
  ) {
    this.logedInUser = this.cookiesService.get_user_login_data();
  }

  ngOnInit(): void {
    this.method_for_getting_list ();
    this.haslistRefreshSubscription = this.categoriesRequestsApiService.haslistRefresh.subscribe(res => {
      if(res.type === 'Product'){
        this.method_for_getting_list();
      }
    })
  }
  method_for_getting_list () {
    let query = {
        where: {
            type: 'Product'
        }
        // include:[
        //     {relation: 'images'}
        // ]
    }

    const vendors = this.cookiesService.get_current_vendor_details();
    if(vendors?.id){
      query.where['vendorsId'] = vendors.id
    }

    this.categoriesRequestsApiService.get_Categories_Request(JSON.stringify(query)).subscribe(
        data => {
            this.venderList = data;
            this.hasLoaded = true;
        }, error => {

        }
    )
  }

  // type = 1 for approve and 2 for reject
approveAndReject(item, type){
  if(type == 1){
    const itemCategory = {...item};
    delete itemCategory.id;
    this.categoriesApiService.create_Categories(itemCategory).subscribe(res => {
      item.categoriesId = res.id;
      this.approveAndRejectRequest(item,1);
    });
  }else if(type == 2){
    // reject logic
    item.status =  'Rejected';
    this.approveAndRejectRequest(item,2);
  }
}

approveAndRejectRequest(item, type){
item.status =  type == 1 ? 'Approve' : 'Rejected';
const message =  type == 1 ? 'Request has been Approved' : 'Request has been rejected';
this.categoriesRequestsApiService.patch_Categories_Request(item).subscribe(res => {
  this.notificationService.get_Notification('success', message);
  this.method_for_getting_list();
});
}

  ngOnDestroy(): void {
    if(this.haslistRefreshSubscription){
      this.haslistRefreshSubscription.unsubscribe();
    }
  }

}
