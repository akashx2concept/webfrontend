import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { FormDirModule } from "../../commonModule/form-dir/form-dir.module";
import { MaterialModule } from "../../material.module";
import { AuthGuardService } from "../../services/commonServices/auth-guard/auth-guard.service";
import { CategoriesComponent } from "./categories.component";




const routes: Routes = [
    {
        path: '',
        component: CategoriesComponent,
        children: [
            {
                path: 'venderCategories',
                loadChildren: () => import('./venderCategories/venderCategories.module').then(m => m.VenderCategoriesModule),
                canActivate: [AuthGuardService],
            },
            {
                path: 'productCategories',
                loadChildren: () => import('./productCategories/productCategories.module').then(m => m.ProductCategoriesModule),
                canActivate: [AuthGuardService],
            }
        ]
    }
]


@NgModule({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        MaterialModule,
        RouterModule.forChild(routes),
        FormDirModule
    ],
    declarations: [
        CategoriesComponent
    ],
    exports: [
        CategoriesComponent
    ]
})

export class CategoriesModule {

}
