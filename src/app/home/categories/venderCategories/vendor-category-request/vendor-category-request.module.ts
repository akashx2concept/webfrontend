import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { VendorCategoryRequestListComponent } from './vendor-category-request-list/vendor-category-request-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BreadcrumbModule } from '../../../../commonModule/breadcrumb/breadcrumb.module';
import { MaterialModule } from '../../../../material.module';
import { VendorCategoryRequestListModule } from './vendor-category-request-list/vendor-category-request-list.module';
import { VendorCategoryRequestComponent } from './vendor-category-request.component';
import { FormDirModule } from '../../../../commonModule/form-dir/form-dir.module';


// const routes: Routes = [
//   {
//     path: '',
//     component: VendorCategoryRequestListComponent
//   }
// ]


@NgModule({
  declarations: [
    VendorCategoryRequestComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    BreadcrumbModule,
    VendorCategoryRequestListModule,
    FormDirModule,
    RouterModule
    // RouterModule.forChild(routes)
  ],
  exports: [VendorCategoryRequestComponent]
})
export class VendorCategoryRequestModule { }
