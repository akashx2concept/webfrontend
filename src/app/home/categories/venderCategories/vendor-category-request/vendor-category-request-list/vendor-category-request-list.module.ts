import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendorCategoryRequestListComponent } from './vendor-category-request-list.component';
import { MaterialModule } from '../../../../../material.module';



@NgModule({
  declarations: [
    VendorCategoryRequestListComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    VendorCategoryRequestListComponent
  ]
})
export class VendorCategoryRequestListModule { }
