import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AddCategoriesComponent } from '../../../../commonModule/addCategories/addCategories.component';
import { AddCategoriesRequestsComponent } from '../../../../commonModule/addCategoriesRequests/addCategoriesRequests.component';
import { CategoriesRequestsApiService } from '../../../../services/api/CategoriesRequestsApi.service.';
import { CookiesService } from '../../../../services/commonServices/cookies.service';
import { NeighborhoodsApiService } from '../../../../services/neighborhoodsApi.service';

@Component({
  selector: 'NA-vendor-category-request',
  templateUrl: './vendor-category-request.component.html',
  styleUrls: ['./vendor-category-request.component.scss']
})
export class VendorCategoryRequestComponent implements OnInit {
  confirmRef: BsModalRef;
  hasListUpdated = false;
  logedInUser;
  constructor(
        private modalService: BsModalService,
        private cookiesService: CookiesService,
        private categoriesRequestsApiService: CategoriesRequestsApiService,
        public neighborhoodsApiService: NeighborhoodsApiService
    ) {
      this.logedInUser = this.cookiesService.get_user_login_data();
     }

  ngOnInit(): void {
  }
  method_for_adding_categories () {
    let initialState: any;
    let text = `Are you sure you want to delete from the list?`;
    initialState = { parentObj: { type: 'Vendor' } };
    this.confirmRef = this.modalService.show(AddCategoriesRequestsComponent, {class: 'modal-dialog-centered modal-md', initialState});
    this.confirmRef.content.action.subscribe((value) => {
        if (value){
            // this.method_for_getting_list();
            this.categoriesRequestsApiService.requestGenerated = {type: 'Vendor', value:true};
        } else {

        }
    });
}
}
