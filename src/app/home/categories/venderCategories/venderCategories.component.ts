import { Component, OnInit } from "@angular/core";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { AddCategoriesComponent } from "../../../commonModule/addCategories/addCategories.component";
import { CategoriesApiService } from "../../../services/api/CategoriesApi.service";
import { CookiesService } from "../../../services/commonServices/cookies.service";
import { PaginationService } from "../../../services/commonServices/pagination.service";
import { NeighborhoodsApiService } from "../../../services/neighborhoodsApi.service";
import { ProductCategoriesInternalService } from "../productCategories/productCategoriesInternal.service";




@Component({
    selector: 'NA-venderCategories',
    templateUrl: './venderCategories.component.html',
    styleUrls: [ './venderCategories.component.scss' ]
})


export class VenderCategoriesComponent implements OnInit {

    private confirmRef: BsModalRef;

    public loginUserData;

    public venderList;

    public currentPageNo:any = 1;

    constructor (private modalService: BsModalService,
        private cookiesService: CookiesService,
        private categoriesApiService: CategoriesApiService,
        public neighborhoodsApiService: NeighborhoodsApiService, public productCategoriesInternalService: ProductCategoriesInternalService,
        public paginationService: PaginationService ) {
        this.loginUserData = this.cookiesService.get_user_login_data();
    }


    ngOnInit () {
        this.method_for_getting_list();
    }


    method_for_adding_categories () {
        let initialState: any;
        let text = `Are you sure you want to delete from the list?`;
        initialState = { parentObj: { type: 'Vendor' } };
        this.confirmRef = this.modalService.show(AddCategoriesComponent, {class: 'modal-dialog-centered modal-md', initialState});
        this.confirmRef.content.action.subscribe((value) => {
            if (value){
                this.method_for_getting_list();
            } else {

            }
        });
    }

    method_for_updating_categories (category) {
      let initialState: any;
      let text = `Are you sure you want to delete from the list?`;
      initialState = { parentObj: { ...category } };
      this.confirmRef = this.modalService.show(AddCategoriesComponent, {class: 'modal-dialog-centered modal-md', initialState});
      this.confirmRef.content.action.subscribe((value) => {
          if (value){
              this.method_for_getting_list();
          } else {

          }
      });
  }

  pageChanged (event) {
    this.currentPageNo = event;
    this.paginationService.paginationConfig.currentPage = event; 
    this.productCategoriesInternalService.filter.skip = (this.productCategoriesInternalService.filter.page_limit * this.paginationService.paginationConfig.currentPage) - this.productCategoriesInternalService.filter.page_limit; 
    this.method_for_getting_list();
  }

  method_for_change_pagination_dropdown () {
      setTimeout(() => {
        this.paginationService.paginationConfig.currentPage = 1
        this.currentPageNo = 1;
        this.productCategoriesInternalService.filter.skip = 0;
        this.paginationService.selected_drop_down_data = this.paginationService.paginationConfig.itemsPerPage;
        this.method_for_getting_list();
      })
  }


  method_for_change_in_filter() {
      this.paginationService.paginationConfig.currentPage = 1;
      this.productCategoriesInternalService.filter.skip  = 0;
      this.method_for_getting_list();
  }

    method_for_getting_list () {
        this.productCategoriesInternalService.filter['type'] = "Vendor";
        this.productCategoriesInternalService.method_for_making_dto_filter();
        // let query = {
        //     where: {
        //         type: 'Vendor'
        //     },
        //     include:[
        //         {relation: 'images'}
        //     ]
        // }

        // this.categoriesApiService.get_Categories(JSON.stringify(query)).subscribe(
        //     data => {
        //         this.venderList = data;
        //     }, error => {

        //     }
        // )

    }

}
