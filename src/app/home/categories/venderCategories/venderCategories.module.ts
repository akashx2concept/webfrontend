import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { BreadcrumbModule } from "../../../commonModule/breadcrumb/breadcrumb.module";
import { FormDirModule } from "../../../commonModule/form-dir/form-dir.module";
import { MaterialModule } from "../../../material.module";
import { VenderCategoriesComponent } from "./venderCategories.component";
import { VendorCategoryRequestComponent } from "./vendor-category-request/vendor-category-request.component";
import { VendorCategoryRequestModule } from "./vendor-category-request/vendor-category-request.module";



const routes: Routes = [
    {
        path: '',
        component: VenderCategoriesComponent
    },
    {
      path: 'vendor-category-request',
      component: VendorCategoryRequestComponent
    }
]


@NgModule({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        MaterialModule,
        BreadcrumbModule,
        VendorCategoryRequestModule,
        RouterModule.forChild(routes),
        FormDirModule
    ],
    declarations: [
        VenderCategoriesComponent
    ],
    exports: [
        VenderCategoriesComponent
    ]
})

export class VenderCategoriesModule {


}
