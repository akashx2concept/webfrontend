import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.scss']
})
export class ProductViewComponent implements OnInit {
  hasList = true;
  constructor() { }

  ngOnInit(): void {
  }

  addForm(){
    this.hasList = !this.hasList;
  }

}
