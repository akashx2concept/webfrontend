import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule, Routes } from '@angular/router';
import { ProductDetailComponent } from './product-detail.component';
import { MaterialModule } from '../../../material.module';


const route:Routes = [
  {
    path: '',
    component: ProductDetailComponent
  }
]

@NgModule({
  declarations: [
    ProductDetailComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule.forChild(route)
  ]
})
export class ProductDetailModule { }
