import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ImageCropperComponent } from '../../../commonModule/imageCropper/imageCropper.component';
import { OrderApiService } from '../../../services/api/orderApi.service';
import { ProductApiService } from '../../../services/api/productApi.service';
import { ProductImageApiService } from '../../../services/api/productImageApi.service';
import { NotificationService } from '../../../services/commonServices/notification.service';
import { NeighborhoodsApiService } from '../../../services/neighborhoodsApi.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {
  productId;


  private confirmRef: BsModalRef;

  public productDetails;

  public productImage = [];

  public tags: string;

  public stockSalesHistory_list = [];

  constructor(private route: Router, private activatedRoute: ActivatedRoute,
    private productAPIService: ProductApiService, private notificatonService: NotificationService,
    private modalService: BsModalService, private productImageApiService: ProductImageApiService,
    public neighborhoodsApiService: NeighborhoodsApiService, private orderApiService: OrderApiService ) {

  }

  ngOnInit(): void {
    this.productId = this.activatedRoute.snapshot.params.id;
    this.method_for_getting_product();
    this.method_For_getting_sales_history ();
  }

  method_For_getting_sales_history () {
    var query:any = {
      where: {
        productsId: this.productId
      },
      include: [
        {relation: "orders"},
        {relation: "products"},
      ]
    };
    query = JSON.stringify(query)
    this.orderApiService.get_OrderItems(query).subscribe (
      data => {
        this.stockSalesHistory_list = data;
      }, error => {

      }
    )
  }


  method_for_getting_product () {
    let query = {
      where: {
        id: this.productId
      },
      include: [
        { relation: 'productPrices' },
        { relation: 'vendors' },
        { relation: 'categories' },
        { relation: 'productTags' },
        { relation: 'productImages', scope: { include: [ { relation: 'productImage' } ],  order: "id Desc" } }
      ]
    };
    this.productAPIService.get_Products(JSON.stringify(query)).subscribe(
      data => {
        if (data.length) {
          this.productDetails = data[0];
          if (this.productDetails['productImages'].length) {
            this.productImage = this.productDetails['productImages'];
          }
          if(this.productDetails.productTags.length){
            this.tags = this.productDetails.productTags.map(rec => rec.tag).join(', ');
          }else {
            this.tags = 'NA';
          }
        }
      }, error => {

      }
    )
  }

  uploadProfilePic () {
    let initialState: any;
    initialState = { parentObj: {  } };
    this.confirmRef = this.modalService.show(ImageCropperComponent, {class: 'modal-dialog-centered modal-lg', initialState});
    this.confirmRef.content.action.subscribe((value) => {
        if (value){
          console.log(value);
          if (value.length) {
            // this.method_for_getting_addressList();
            this.method_for_product_Image(value[0]['id']);
          }
        } else {

        }
    });
  }

  method_for_product_Image (imageId) {
    let dataDto = {
      productsId: this.productId,
      productImageId: imageId
    }
    this.productImageApiService.create_productImage(dataDto).subscribe(
      data =>  {
        this.method_for_getting_product();
      }, error => {

      }
    )
  }


  editProduct(){
    this.route.navigate(['edit-product', this.productDetails.id], {relativeTo: this.activatedRoute.parent.parent});
  }

  back(){
    this.route.navigate(['./'], {relativeTo: this.activatedRoute.parent.parent});
  }

  delete_product(){
      this.productAPIService.delete_product(this.productId).subscribe(res => {
          this.notificatonService.get_Notification('error', 'Product has been deleted');
          this.back();
      })
  }

}
