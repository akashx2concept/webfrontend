import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { ConfirmActionComponent } from '../../../commonModule/confirm-action/confirm-action.component';
import { ProductApiService } from '../../../services/api/productApi.service';
import { StripePaymentApiService } from '../../../services/api/stripePaymentApi.service';
import { CookiesService } from '../../../services/commonServices/cookies.service';
import { NotificationService } from '../../../services/commonServices/notification.service';
import { ProductInternalService } from '../productInternalService';

declare const Stripe;


@Component({
  selector: 'NA-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {



  public updateId;

  public loginUserData;
  public loginVendorData;

  public userRole;


  // public stripe = Stripe('pk_test_51IdCiXE1e1bineRZmQBEy9nIRvX8O2rry6UsxQjvxLjS0EliBzUXy0eYuBboFlyL4WlyxfpqiZS8FhTBA8NxCMte005tOlGUUH');


  constructor(public productInternalService: ProductInternalService,
    private productApiService: ProductApiService,
    private activatedRoute: ActivatedRoute,
    private cookiesService: CookiesService,
    private notificationService: NotificationService,
    private stripePaymentApiService: StripePaymentApiService,
    private dialog: MatDialog ) {

    this.updateId = this.activatedRoute.snapshot.paramMap.get('vendorId');

    this.loginUserData = this.cookiesService.get_user_login_data();

      if (!this.updateId) {
        this.loginUserData = this.cookiesService.get_user_login_data();
        this.loginVendorData = this.cookiesService.get_current_vendor_details();
        this.updateId = this.loginVendorData['id'];
      }

      this.userRole = this.loginUserData['role'];


   }

  ngOnInit(): void {
    this.productInternalService.vendorId = this.updateId;
    this.productInternalService.method_for_getting_list_of_products();
    // this.route.params.subscribe(res => {
    //     this.productInternalService.vendorId = res.vendorId;
    //     this.productInternalService.method_for_getting_list_of_products();
    // });
  }

  // [routerLink]="['product-detail', item.id]"




  // let stripe = Stripe('pk_test_51IdCiXE1e1bineRZmQBEy9nIRvX8O2rry6UsxQjvxLjS0EliBzUXy0eYuBboFlyL4WlyxfpqiZS8FhTBA8NxCMte005tOlGUUH');
    // // checkoutButton = document.getElementById('checkout-button');

    // console.log(stripe);

    // stripe.redirectToCheckout({ sessionId: "cs_test_a1OUw1vzNasljyjVFxMkka69cfLP7mJGKPrgVGnvRa4AbZJb3mvtocSPzE" })


    method_for_getting_pay () {

      // let data = {
      //   userId: "43",
      //   userCardId: "card_1Ixzy9E1e1bineRZ4FRdnV5f",
      //   vendorId: 3,
      //   addressId: 1,
      //   product: [
      //     { productId: 45, quantity: 1, productPrices: 52},
      //     { productId: 44, quantity: 1, productPrices: 51},
      //   ]
      // }


      // this.stripePaymentApiService.StripePayments_customerPlaceOrder(data).subscribe(
      //   data => {
      //     console.log(this.stripe);

      //     this.stripe.redirectToCheckout({ sessionId: data['id'] }).then((res) => {
      //       console.log(res);
      //     }).catch((err) => {
      //       console.log(err);
      //     })
      //   }, error => {

      //   }
      // )

    // checkoutButton = document.getElementById('checkout-button');






    }

    deleteProduct(product){
        this.dialog.open(ConfirmActionComponent,{
          width:'400px',
          data: {}
        })
        .afterClosed()
        .subscribe(res => {
          product.isDeleted = true;
          // product.isActive = true;
          const dto = (({ id, isDeleted }) => ({ id, isDeleted }))(product);
          this.productApiService.patch_product(dto)
          .subscribe(res => {
              this.notificationService.get_Notification('success', 'Product has been deleted');
              this.productInternalService.method_for_getting_list_of_products();
          });
        })
    }

}
