import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { Observable } from 'rxjs';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { map, startWith } from 'rxjs/operators';
import { CategoriesApiService } from '../../../services/api/CategoriesApi.service';
import { NotificationService } from '../../../services/commonServices/notification.service';
import { ProductApiService } from '../../../services/api/productApi.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { forEachChild } from 'typescript';
import { CookiesService } from '../../../services/commonServices/cookies.service';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent implements OnInit {
  visible = true;
  selectable = true;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  tagCtrl = new FormControl();
  filteredtag: Observable<string[]>;
  tags = [];
  deltedTags = [];
  productId;

  productCategory = []

  pricingInfo:FormGroup;
  productDetail: FormGroup;
  moreInfo: FormGroup;
  @ViewChild('tagInput') tagInput: ElementRef<HTMLInputElement>;

  constructor(private fb: FormBuilder,
     private categoriesApiService: CategoriesApiService,
     private notificationService: NotificationService,
     private productApiService: ProductApiService,
     private router: Router,
     private route: ActivatedRoute,
     private cookesService: CookiesService) {
    this.filteredtag = this.tagCtrl.valueChanges.pipe(
      startWith(null),
      map((tag: string | null) =>
        tag ? this._filter(tag) : this.tags.slice()
      )
    );
    this.initForm();
    this.method_for_getting_list();
  }

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our tag
    if (value) {
      this.tags.push({tag:value});
    }

    // Clear the input value
    // event?.chipInput!.clear();

    this.tagCtrl.setValue(null);
  }

  remove(tag: string): void {
    const index = this.tags.findIndex((v) => v.tag == tag);
    if(index >=0 && this.tags[index].id){
      this.deltedTags.push({...this.tags[index], isDeleted:true, isActive:false});
    }
    if (index >= 0) {
      this.tags.splice(index, 1);
    }
    // console.log(this.deltedTags);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.tags.filter(
      t => t.tag.toLowerCase().indexOf(filterValue) === 0
    );
  }


initForm(){
  this.productDetail = this.fb.group({
    id:[],
    productName:['', Validators.required],
    categoriesId:['', Validators.required],
    description: [''],
    allergens: [],
    tags: []
  })
  this.pricingInfo = this.fb.group({
      productPrices: this.fb.array([this.getProductBiiling()],Validators.required),
      avaliableStock: ['', Validators.required],
      vatAvailable: [false, Validators.required],
      vatRate: [0, Validators.required]
  })

  this.moreInfo = this.fb.group({
    status: ['true',Validators.required],
    ageRange: [],
    expiryDate: []
  })
}

getProductBiiling(){
   return this.fb.group({
    id: [],
    productUnit:['', Validators.required],
    unitType:['', Validators.required],
    unitPrice: ['', Validators.required]
   })
}

get productBillList() : FormArray {
  return this.pricingInfo.get("productPrices") as FormArray
}

addProductbill(){
  this.productBillList.push(this.getProductBiiling());
}

removeProduct(i){
  this.productBillList.removeAt(i);
}


  ngOnInit(): void {
    this.route.params.subscribe(res => {
      this.productId = res.id;
      if(this.productId){
        this.method_for_getting_product();
      }
    })
  }

  method_for_getting_list () {
    let query = {
        where: {
            type: 'Product',
            isActive: true
        }
    }

    this.categoriesApiService.get_Categories(JSON.stringify(query)).subscribe(
        data => {
            this.productCategory = data;
            // console.log(this.productCategory);
        }, error => {

        }
    )

}

createUpdateProduct(){
   if(this.pricingInfo.invalid || this.productDetail.invalid || this.moreInfo.invalid){
    this.notificationService.get_Notification('error', "Inavlid request to create product");
     return false;
   }
   let tags = [];
   let price = ""
   if(this.tags.length){
     tags = [...this.tags, ...this.deltedTags];
   }
   if(this.pricingInfo.value.price){
     price = this.pricingInfo.value.price;
   }

   const reqObj = {
     ...this.pricingInfo.value,
     ...this.productDetail.value,
     ...this.moreInfo.value,
     tags,
     price,
     vendorsId: this.route.parent.snapshot.paramMap.get('vendorId')
   }
   reqObj.price = reqObj.productPrices;
   reqObj.vatAvailable = reqObj.vatAvailable == 'yes' ? true : false;
   if(reqObj.ageRange){
    reqObj.ageRange = 18;
   }
   if(reqObj.expiryDate){
    reqObj.expiryDate = moment(reqObj.expiryDate).utc().valueOf();
   }
   if(!reqObj.vendorsId){
    const vendor = this.cookesService.get_current_vendor_details();
    reqObj.vendorsId = vendor.id;
   }
   this.recalculatePrice(reqObj)
  //  console.log(reqObj);
  //  return false;
  const API = reqObj.id ? this.productApiService.put_product : this.productApiService.create_product;
   API.bind(this.productApiService)(reqObj).subscribe(
    data => {
        !reqObj.id ? this.notificationService.get_Notification('success', "Successfully Created") : this.notificationService.get_Notification('success', "Successfully Updated")
        this.backToDashbaord();
    }, error => {

    }
)
}

method_for_getting_product () {
  let query = {
    where: {
      id: this.productId
    },
    include: [
      { relation: 'productPrices' },
      { relation: 'vendors' },
      { relation: 'categories' },
      { relation: 'productTags' }
      // { relation: 'productImages', scope: { include: [ { relation: 'productImage' } ],  order: "id Desc" } }
    ]
  };
  this.productApiService.get_Products(JSON.stringify(query)).subscribe(
    data => {
      if (data.length) {
        // this.productDetails = data[0];
        const detail = data[0];
        detail.vatAvailable = detail.vatAvailable ? 'yes': 'no';
        detail.ageRange = detail.ageRange ? true : null;
        detail.expiryDate = detail.expiryDate ? moment(+detail.expiryDate).toISOString(): null;
        detail.status = detail.status + '';
        this.tags = detail.productTags;
        this.productDetail.patchValue(data[0]);
        if(detail.productPrices){
          for(let i = 1; i < detail.productPrices.length ; i++){
            this.addProductbill();
          }
          this.pricingInfo.patchValue(detail);
          if(detail.productPrices.length){
            // this.pricingInfo.disable();
          }
        }
        this.moreInfo.patchValue(detail);
        // this.pricingInfo.patchValue(detail);
        // if (this.productDetails['productImages'].length) {
        //   this.productImage = this.productDetails['productImages'];
        // }
        // if(this.productDetails.productTags.length){
        //   this.tags = this.productDetails.productTags.map(rec => rec.tag).join(', ');
        // }else {
        //   this.tags = 'NA';
        // }
        // console.log(detail);
      }
    }, error => {

    }
  )
}

recalculatePrice(obj){

  const vatRate = obj.vatRate ? obj.vatRate/100 : 0
  if(obj.price.length){
     obj.price.forEach(priceInfo => {
      priceInfo.sellingPrice = +priceInfo.unitPrice + (+priceInfo.unitPrice*vatRate);
    });
  }
}

backToDashbaord(){
    if(!this.productId){
      this.router.navigate(['../'],{relativeTo:this.route});
    }else{
      this.router.navigate(['../../','product-detail', this.productId],{relativeTo:this.route});
    }

  }
}
