import { Injectable } from "@angular/core";
import { ProductApiService } from "../../services/api/productApi.service";
import { UserApiService } from "../../services/api/userApi.service";
import { VendorsApiService } from "../../services/api/vendorsApi.service";





@Injectable({
    providedIn: 'root'
})


export class ProductInternalService {


    public filter:any = {
        search: null,
        skip: 0,
        page_limit: 10
    }

    vendorId;

    public productList = [];

    constructor (private productApiService: ProductApiService) {

    }

    method_for_getting_list_of_products () {
        var query:any = {
            where: {
              vendorsId: this.vendorId,
              isActive: true,
              isDeleted: false,
              status: true
            },
            include: [
                { relation: "categories" },
                { relation: 'vendors' },
                { relation: 'productTags' },
                { relation: 'productPrices' }
            ]
        };
        query['order'] = "createdAt DESC";

        // var query:any = {
        //     where: {
        //         isActive: true,
        //         or: [
        //             { productName: { like: '%pro%' } }
        //         ]
        //     },
        // }

        query = encodeURI(JSON.stringify(query));

        this.productApiService.get_Products(query).subscribe(
            data => {
                this.productList = data;
            }, error => {

            }
        )
    }


}
