import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ProductViewComponent } from './product-view/product-view.component';
import { ProductFormComponent } from './product-form/product-form.component';
import { ProductListComponent } from './product-list/product-list.component';
import { Routes , RouterModule } from '@angular/router';
import { MaterialModule } from '../../material.module';
import { AuthGuardService } from '../../services/commonServices/auth-guard/auth-guard.service';
import { BreadcrumbModule } from '../../commonModule/breadcrumb/breadcrumb.module';
import { ConfirmActionModule } from '../../commonModule/confirm-action/confirm-action.module';
import { FormDirModule } from '../../commonModule/form-dir/form-dir.module';

const routes: Routes = [
    {
      path: '',
      component: ProductViewComponent,
      children: [
        {
          path: '', component: ProductListComponent
        },
        {
          path: 'edit-product/:id',
          component: ProductFormComponent,
          canActivate: [AuthGuardService],
        },
        {
          path: 'add-product',
          component: ProductFormComponent,
          canActivate: [AuthGuardService],
        },
        {
          path:'product-detail/:id',
          loadChildren:() => import('./product-detail/product-detail.module').then(m => m.ProductDetailModule),
          canActivate: [AuthGuardService],
        }
      ]
    }
]



@NgModule({
  declarations: [
    ProductViewComponent,
    ProductListComponent,
    ProductFormComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    BreadcrumbModule,
    ConfirmActionModule,
    RouterModule.forChild(routes),
    FormDirModule
  ]
})
export class ProductModule { }
