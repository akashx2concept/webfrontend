import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangePasswordComponent } from './change-password.component';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { BreadcrumbModule } from '../../commonModule/breadcrumb/breadcrumb.module';
import { MaterialModule } from '../../material.module';

const routes: Routes = [
    {path: '', component: ChangePasswordComponent}
]

@NgModule({
  declarations: [
    ChangePasswordComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    BreadcrumbModule,
    RouterModule.forChild(routes)
  ]
})
export class ChangePasswordModule { }
