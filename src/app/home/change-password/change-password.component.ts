import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Router } from '@angular/router';
import { UserApiService } from '../../services/api/userApi.service';
import { CookiesService } from '../../services/commonServices/cookies.service';
import { NotificationService } from '../../services/commonServices/notification.service';

@Component({
  selector: 'NA-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  public changePasswordForm: FormGroup;
  userRole;
  matcher = new MyErrorStateMatcher();
  constructor(private route: Router, private fb: FormBuilder, private userApiService: UserApiService, private notificationService: NotificationService, private cookiesService: CookiesService) {
    this.initForm();
  }

  ngOnInit(): void {

      const loginUserData = this.cookiesService.get_user_login_data();

      this.userRole = loginUserData['role'];
  }

  initForm(){
    this.changePasswordForm = this.fb.group({
      oldPassword: ['',  [Validators.required, Validators.minLength(8),
        Validators.maxLength(25),
        Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{7,}')
      ]],
      newPassword: ['',  [Validators.required, Validators.minLength(8),
        Validators.maxLength(25),
        Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{7,}')
      ]],
      cnfPassword:  ['',  [Validators.required, Validators.minLength(8),
        Validators.maxLength(25),
        Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{7,}')
      ]]
    },  { validators: this.checkPasswords })
  }

  get f() { return this.changePasswordForm.controls; };

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    const password = group.get('newPassword').value;
    const confirmPassword = group.get('cnfPassword').value;

    return password === confirmPassword ? null : { notSame: true }
  }

  changePassword(){
    console.log(this.changePasswordForm);
    if(this.changePasswordForm.invalid){
      return false;
    }
    console.log(this.changePasswordForm);
    this.userApiService.change_password(this.changePasswordForm.value).subscribe(res => {
      this.notificationService.get_Notification('success', 'Password has been changed');
      this.changePasswordForm.reset();
    }, err => {
      console.log(err);
    })
  }
  cancel(){
    if(this.userRole == 'Root' || this.userRole == 'Vendor'){
      this.route.navigate(['/home/dashboard']);
    }else{
      this.route.navigate(['/']);
    }
  }
}

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control?.invalid && control?.parent?.dirty);
    const invalidParent = !!(control?.parent?.invalid && control?.parent?.dirty);

    return invalidCtrl || invalidParent;
  }
}
