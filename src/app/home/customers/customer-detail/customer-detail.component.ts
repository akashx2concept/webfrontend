import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ImageCropperComponent } from '../../../commonModule/imageCropper/imageCropper.component';
import { NeighborhoodsApiService } from '../../../services/neighborhoodsApi.service';
import { CustomerInternalService } from '../customerInternalService';

@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.scss']
})
export class CustomerDetailComponent implements OnInit {
  customerDetails;
  private confirmRef: BsModalRef;
  actionList = [
    {
      id:'',
      name: 'Take Action'
    },
    {
      id:'1',
      name: 'Inactive'
    },
    {
      id:'2',
      name: 'Active'
    },
    {
      id:'3',
      name: 'Suspend'
    }
  ]
  constructor(public neighborhoodsApiService: NeighborhoodsApiService,
    private modalService: BsModalService,
    private activatedRoute: ActivatedRoute, private router: Router,
    private customerInternalService: CustomerInternalService) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(res => {
      console.log(res);
      this.getCustomerDetail(res.customerId);
    });
  }


  method_for_redirect () {
    this.router.navigate(['home/customer'])
  }

  getCustomerDetail(id){
    this.customerInternalService.method_for_getting_slected_customer(id)
    .subscribe(res => {
      this.customerDetails = res;
      console.log(this.customerDetails);
    })

  }


  uploadCustomerImage(){
    let initialState: any;
      initialState = { parentObj: {  } };
      this.confirmRef = this.modalService.show(ImageCropperComponent, {class: 'modal-dialog-centered modal-lg', initialState});
      this.confirmRef.content.action.subscribe((value) => {
          if (value){
            console.log(value);
            if (value.length) {
              // this.method_for_getting_addressList();
              this.method_for_update_customer(value[0]['id']);
            }
          } else {

          }
      });
  }

  method_for_update_customer(imgId){
     const dto = {
       id:this.customerDetails.id,
       profilePicId: imgId
     }
     this.customerInternalService.update_proflie(dto).subscribe(res => {
       this.getCustomerDetail(this.customerDetails.id);
     })
  }
}
