import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerDetailComponent } from './customer-detail.component';
import { RouterModule, Routes } from '@angular/router';
import { BreadcrumbModule } from '../../../commonModule/breadcrumb/breadcrumb.module';
import { MaterialModule } from '../../../material.module';
import { CustomerOrderComponent } from '../customer-order/customer-order.component';


const route: Routes = [
  {
    path: '', component:CustomerDetailComponent
  }
]


@NgModule({
  declarations: [
    CustomerDetailComponent,
    CustomerOrderComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(route),
    BreadcrumbModule,
    MaterialModule
  ]
})
export class CustomerDetailModule { }
