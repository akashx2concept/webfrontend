import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddCustomerComponent } from './add-customer.component';
import { RouterModule, Routes } from '@angular/router';
import { MaterialModule } from '../../../material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FormDirModule } from '../../../commonModule/form-dir/form-dir.module';

const route: Routes = [
  {path: '', component: AddCustomerComponent}
]

@NgModule({
  declarations: [
    AddCustomerComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(route),
    MaterialModule,
    ReactiveFormsModule,
    FormDirModule
  ]
})
export class AddCustomerModule { }
