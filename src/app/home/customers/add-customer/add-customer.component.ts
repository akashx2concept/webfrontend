import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AddressApiService } from '../../../services/api/addressApi';
import { CityApiService } from '../../../services/api/cityApi';
import { CountryApiService } from '../../../services/api/countryApi';
import { StateApiService } from '../../../services/api/stateApi';
import { UserApiService } from '../../../services/api/userApi.service';
import { NotificationService } from '../../../services/commonServices/notification.service';
import { CustomerInternalService } from '../customerInternalService';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.scss']
})
export class AddCustomerComponent implements OnInit {
  imageURL: string;
  customerForm: FormGroup;

  public countryList = [];
  public stateList = [];
  public cityList = [];
  title;
  action = 'Create';

  constructor(private countryApiService: CountryApiService,
    private router: Router,
    private route: ActivatedRoute,
    private stateApiService: StateApiService,
    private cityApiService: CityApiService,
    private notificationService: NotificationService,
    private customerInternalService: CustomerInternalService,
    private fb: FormBuilder, private userApiService: UserApiService,
    private addressApiService: AddressApiService) {
      this.initForm()
    }

  ngOnInit(): void {
    this.method_for_getting_country();
    this.route.params.subscribe(res => {
      if(res.id){
        this.getCustomerInfo(res.id);
        this.title = "Update Customer";
        this.action = "Update"
        this.customerForm.removeControl('password');
        this.customerForm.removeControl('cnf_password');
      }else {
        this.title = "Create Customer";
      }

    })
  }


  initForm(){
    this.customerForm = this.fb.group(
      {
        id:[],
        firstName: ['', [Validators.required, this.noWhitespaceValidator]],
        middleName: [''],
        lastName: ['', [Validators.required,this.noWhitespaceValidator]],
        isActive: [true],
        isDeleted: false,
        role: ['User'],
        email: ['',[Validators.required,Validators.email, this.noWhitespaceValidator]],
        phoneNo:['', [Validators.required, this.noWhitespaceValidator]],
        addresses: this.fb.array([this.getAddress()]),
        password: ['', [Validators.required, Validators.minLength(8),
          Validators.maxLength(25),
          Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{7,}'),
          this.noWhitespaceValidator
        ]],
        cnf_password: ['',[Validators.required, Validators.minLength(8),
          Validators.maxLength(25),
          Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{7,}'),
          this.noWhitespaceValidator
        ]]
        // "profilePicId": 0
      }
    )
    console.log(this.getAddresses.length);
  }



  getAddress(): FormGroup{
    return this.fb.group(
      {
        "id": [],
        "street1": ['',[Validators.required,this.noWhitespaceValidator]],
        "street2": [],
        "postalCode": ['',[Validators.required,this.noWhitespaceValidator]],
        "isDefault": true,
        "userId": [],
        "countryId": ['',[Validators.required]],
        "stateId": ['',[Validators.required]],
        "cityId": ['']
      }
    )
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
}

  get getAddresses(): FormArray {
      return this.customerForm.get('addresses') as FormArray
  }

  get getCustomerForm(){
    return this.customerForm.controls;
  }
  addUpdateCustomer(){
    console.log(this.customerForm.value)
      if(this.customerForm.invalid){
          return false;
      }

      let userDto = {
          ...this.customerForm.value
      }
      if((this.action !== 'Update') && (userDto.password !== userDto.cnf_password)){
        this.notificationService.get_Notification('error', 'Password  and confirm password is missmatched');
        return false;
      }
      userDto['isNewUser'] = false;
      delete userDto.address;
      const API = userDto.id ? this.userApiService.patch_users : this.userApiService.post_users;
      API.bind(this.userApiService)(userDto).subscribe(
        data => {
            this.addAddress(data.id);
        });
  }

  addAddress(userId){
    const addressDTO  = {
      ...this.customerForm.value.addresses[0],
      userId
    }
    const API = addressDTO.id ? this.addressApiService.update_Addresses : this.addressApiService.create_Addresses;
    API.bind(this.addressApiService)(addressDTO).subscribe(res => {
      this.notificationService.get_Notification('success', 'Customer has been created');
      this.router.navigate(['/home/customer']);
    });
  }

  getCustomerInfo(userId){
    const query = {
      where:{
        id:userId
      },
      include: [
        {
          relation: 'addresses'
        }
      ]
    }
    this.userApiService.get_user(decodeURI(JSON.stringify(query))).subscribe(userDetail => {
      console.log(userDetail);
      // userDetail.address
      const address = userDetail[0].addresses[0];
      if(address?.countryId){
        this.method_for_change_in_country(address.countryId);
      }
      if(address?.stateId){
        this.method_for_change_in_state(address.stateId);
      }
      this.customerForm.patchValue(userDetail[0]);
    })
  }

  // Image Preview
  showPreview(event) {
    const file = (event.target as HTMLInputElement).files[0];

    // File Preview
      const reader = new FileReader();
      reader.onload = () => {
        this.imageURL = reader.result as string;
      }
      reader.readAsDataURL(file)
  }

  method_for_getting_country (searchVal?) {
    console.log(searchVal)
    let query:any = {
        where: {
          isActive: true,
          isDeleted: false
        },
    }
    query = encodeURI(JSON.stringify(query));
    this.countryApiService.get_countries(query).subscribe(
        data => {
            this.countryList = data;
        }, error => {

        }
    )
}

method_for_change_in_country (value) {
    console.log(value);
    // this.selectedCountry = value;
    let query:any = {
        where: {
            countryId: value,
            isActive: true,
            isDeleted: false
        },
    }
    query = encodeURI(JSON.stringify(query));
    this.stateApiService.get_states(query).subscribe(
        data => {
            this.stateList = data;
        }, error => {

        }
    )
}

  method_for_change_in_state (value) {
      console.log(value);
      // this.selectedState = value;
      let query:any = {
          where: {
              stateId: value,
              isActive: true,
              isDeleted: false
          },
      }
      query = encodeURI(JSON.stringify(query));
      this.cityApiService.get_cities(query).subscribe(
          data => {
              this.cityList = data;
          }, error => {

          }
      )
  }

  method_for_change_in_city (value) {
      // this.selectedCity = value;
  }

}
