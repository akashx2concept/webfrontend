import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { DebounceModule } from "ngx-debounce";
import { BreadcrumbModule } from "../../commonModule/breadcrumb/breadcrumb.module";
import { MaterialModule } from "../../material.module";
import { AuthGuardService } from "../../services/commonServices/auth-guard/auth-guard.service";
import { CustomersComponent } from "./customers.component";


const routes: Routes = [
    {
        path: '',
        component: CustomersComponent
    },
    {
      path: 'edit-customer/:id',
      loadChildren: () => import('./add-customer/add-customer.module').then(m => m.AddCustomerModule),
      canActivate: [AuthGuardService],
    },
    {
      path: 'add-customer',
      loadChildren: () => import('./add-customer/add-customer.module').then(m => m.AddCustomerModule),
      canActivate: [AuthGuardService],
    },
    {
      path: 'customer-detail/:customerId',
      loadChildren: () => import('./customer-detail/customer-detail.module').then(m => m.CustomerDetailModule),
      canActivate: [AuthGuardService],
    }
]


@NgModule ({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        RouterModule.forChild(routes),
        MaterialModule,
        BreadcrumbModule,
        DebounceModule
    ],
    declarations: [
        CustomersComponent
    ],
    exports: [
        CustomersComponent
    ]
})

export class CustomersModule {

}
