import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { UserApiService } from "../../services/api/userApi.service";
import { PaginationService } from "../../services/commonServices/pagination.service";
import * as moment from 'moment-timezone';



@Injectable({
    providedIn: 'root'
})


export class CustomerInternalService {


    public filter:any = {
      search: null,
      skip: 0,
      page_limit: 10
    }

    public customerCount = 0;
    public customerList = [];

    public totalCustomer = 0;
    public totalActiveCustomer = 0;
    public totalNewCustomer = 0;
    
    constructor (private userApiService: UserApiService, private paginationService: PaginationService) {

    }

    method_for_making_dto_filter () {
      console.log(this.filter, 'filter');
      // this.loginUserData = this.cookiesService.get_user_login_data(true);
      this.method_for_getting_count(this.filter);
    }



    method_for_getting_count (filter) {
        var query:any = {
            isActive: true,
            role: 'User'
        }

        if ( this.filter['search'] ) {
          query = {
            and: [{
                isActive: true,
                role: 'User'
              },
              {
                or: [
                    { firstName: { like: '%' + this.filter['search'] + '%' }},
                    { middleName: { like: '%' + this.filter['search'] + '%' }},
                    { lastName: { like: '%' + this.filter['search'] + '%' }},            
                    { email: { like: '%' + this.filter['search'] + '%' }},
                    { phoneNo: { like: '%' + this.filter['search'] + '%' }},
                  ]
            }]
          };
        }


        query = encodeURI(JSON.stringify(query));

        this.userApiService.get_userCount(query).subscribe(
            data => {
                this.customerCount = data['count'];
                this.paginationService.set_count_data(data['count'], filter['skip']);
                this.method_for_getting_list_of_customer(filter);
            }, error => {

            }
        )
    }


    method_for_getting_list_of_customer (filter) {
        var query:any = {
            where: {
                isActive: true,
                role: 'User'
            },
            include: [
              { relation: 'profilePic' },
              { relation : 'addresses' }
            ]
        };

        if ( this.filter['search'] ) {
          query['where'] = {
            and: [{
                isActive: true,
                role: 'User'
              },
              {
                or: [
                    { firstName: { like: '%' + this.filter['search'] + '%' }},
                    { middleName: { like: '%' + this.filter['search'] + '%' }},
                    { lastName: { like: '%' + this.filter['search'] + '%' }},            
                    { email: { like: '%' + this.filter['search'] + '%' }},
                    { phoneNo: { like: '%' + this.filter['search'] + '%' }},
                  ]
            }]
          }
        }
        query['order'] = "createdAt DESC";
        query['limit'] = this.paginationService.selected_drop_down_data;
        query['skip'] = this.paginationService.skip_data;

        query = encodeURI(JSON.stringify(query));

        this.userApiService.get_user(query).subscribe(
            data => {
                this.customerList = data;
            }, error => {

            }
        )
    }

    method_for_getting_slected_customer(customerId): Observable<any>{
      var userQuery:any = {
        where: {
          id: customerId
        },
        include: [
            { relation: 'addresses' ,
            scope: {
              include: [
                { relation: 'country' },
                { relation: 'state' },
                { relation: 'city' }
              ]
            }},
            { relation: 'vendors' },
            { relation: 'profilePic' },
        ]
      }

      userQuery = encodeURI(JSON.stringify(userQuery));
      return this.userApiService.get_users_findOne(userQuery)
    }

    update_proflie(dto){
      return this.userApiService.patch_users(dto);
    }


    method_for_getting_data () {
      this.method_for_total_customer ();
      this.method_for_active_customer ();
      this.method_for_total_new_customer ();
    }



    method_for_total_customer () {
      var query:any = { isActive: true, role: 'User' };
      query = encodeURI(JSON.stringify(query));
      this.userApiService.get_userCount(query).subscribe(
        data => {
            this.totalCustomer = data['count'];
        }, error => {

        }
      )
    }

    method_for_active_customer () {
      var query:any = { isActive: true, role: 'User' };
      query = encodeURI(JSON.stringify(query));
      this.userApiService.get_userCount(query).subscribe(
        data => {
            this.totalActiveCustomer = data['count'];
        }, error => {

        }
      )
    }

    method_for_total_new_customer () {
      let _dayStart = moment(moment().subtract(2, 'days')).startOf('day').format('x');
      let _dayEnd = moment(new Date()).format('x');
      var query:any = { isActive: true, role: 'User', createdAt: { between: [_dayStart, _dayEnd] }};
      query = encodeURI(JSON.stringify(query));
      this.userApiService.get_userCount(query).subscribe(
        data => {
            this.totalNewCustomer = data['count'];
        }, error => {

        }
      )
    }
}
