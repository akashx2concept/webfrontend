import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { VendorsApiService } from '../../../services/api/vendorsApi.service';
import { PaginationService } from '../../../services/commonServices/pagination.service';
import { OrderCustomerInternalService } from '../order-customer-internal.service';

@Component({
  selector: 'NA-customer-order',
  templateUrl: './customer-order.component.html',
  styleUrls: ['./customer-order.component.scss']
})
export class CustomerOrderComponent implements OnInit {
  public params;
  public currentPageNo:any = 1;

  constructor(
    public orderCustomerInternalService: OrderCustomerInternalService,
    public paginationService: PaginationService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private vendorsApiService: VendorsApiService) { }

    ngOnInit(): void {
      this.activatedRoute.params.subscribe(res => {
        console.log(res);
        this.params = res;
        this.orderCustomerInternalService.userId = res.customerId;
        this.method_for_getting_orderList();
      });


    }

    pageChanged (event) {
      this.currentPageNo = event;
      this.paginationService.paginationConfig.currentPage = event;
      this.orderCustomerInternalService.filter.skip = (this.orderCustomerInternalService.filter.page_limit * this.paginationService.paginationConfig.currentPage) - this.orderCustomerInternalService.filter.page_limit;
      this.method_for_getting_orderList();
    }

    method_for_change_pagination_dropdown () {
        setTimeout(() => {
          this.paginationService.paginationConfig.currentPage = 1
          this.currentPageNo = 1;
          this.orderCustomerInternalService.filter.skip = 0;
          this.paginationService.selected_drop_down_data = this.paginationService.paginationConfig.itemsPerPage;
          this.method_for_getting_orderList();
        })
    }


    method_for_change_in_filter() {
        this.paginationService.paginationConfig.currentPage = 1;
        this.orderCustomerInternalService.filter.skip  = 0;
        this.method_for_getting_orderList();
    }

    method_for_getting_orderList () {
      this.orderCustomerInternalService.method_for_making_dto_filter();
    }


    method_for_redirecting (value, item) {
      if (value == "user") {
        this.router.navigate(['home/customer/customer-detail', item.userId]);
      } else if (value == "vendor") {
        const query = {
          where: {
            id: item.vendorsId
          }
        }
        this.vendorsApiService.Vendors_findOne(JSON.stringify(query))
      .subscribe(res => {
        if(res.userId){
          this.router.navigate(['home/vendor/vendor-detail', res.userId]);
        }
      });
      } else if (value == "viewDetails") {
        this.router.navigate(['home/order/manage-order', item.id]);
      }
    }

}
