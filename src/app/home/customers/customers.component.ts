import { Component, OnInit } from "@angular/core";
import { PaginationService } from "../../services/commonServices/pagination.service";
import { CustomerInternalService } from "./customerInternalService";




@Component ({
    selector: 'NA-customers',
    templateUrl: './customers.component.html',
    styleUrls: [ './customers.component.scss' ]
})


export class CustomersComponent implements OnInit {


    public currentPageNo:any = 1;


    constructor (
      public customerIntrnalService: CustomerInternalService, 
      public paginationService: PaginationService) {

    }

    ngOnInit () {
      this.methodForGetCustomers();
      this.customerIntrnalService.method_for_getting_data();
    }

    pageChanged (event) {
      this.currentPageNo = event;
      this.paginationService.paginationConfig.currentPage = event; 
      this.customerIntrnalService.filter.skip = (this.customerIntrnalService.filter.page_limit * this.paginationService.paginationConfig.currentPage) - this.customerIntrnalService.filter.page_limit; 
      this.methodForGetCustomers();
    }

    method_for_change_pagination_dropdown () {
        setTimeout(() => {
          this.paginationService.paginationConfig.currentPage = 1
          this.currentPageNo = 1;
          this.customerIntrnalService.filter.skip = 0;
          this.paginationService.selected_drop_down_data = this.paginationService.paginationConfig.itemsPerPage;
          this.methodForGetCustomers();
        })
    }


    method_for_change_in_filter() {
        this.paginationService.paginationConfig.currentPage = 1;
        this.customerIntrnalService.filter.skip  = 0;
        this.methodForGetCustomers();
    }

    methodForGetCustomers(){
      this.customerIntrnalService.method_for_making_dto_filter();
    }

}
