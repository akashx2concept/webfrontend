import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentsComponent } from './payments.component';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { BreadcrumbModule } from '../../commonModule/breadcrumb/breadcrumb.module';
import { MaterialModule } from '../../material.module';

const routes: Routes = [
  {
    path: '',
    component: PaymentsComponent,
    children:[
      {
        path:'',
        loadChildren:() => import('./payment-list/payment-list.module').then(m => m.PaymentListModule)
      }
    ]
  }
]

@NgModule({
  declarations: [
    PaymentsComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    BreadcrumbModule,
    RouterModule.forChild(routes)
  ]
})
export class PaymentsModule { }
