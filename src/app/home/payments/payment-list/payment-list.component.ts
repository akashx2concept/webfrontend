import { Component, OnInit } from '@angular/core';
import { PaginationService } from '../../../services/commonServices/pagination.service';
import { PaymentInternalService } from '../paymentInternal.service';

@Component({
  selector: 'NA-payment-list',
  templateUrl: './payment-list.component.html',
  styleUrls: ['./payment-list.component.scss']
})


export class PaymentListComponent implements OnInit {

  public currentPageNo:any = 1;
  
  constructor (public  paymentInternalService: PaymentInternalService, public paginationService: PaginationService) {

  }

  ngOnInit(): void {
    this.method_for_getting_list (); 
  }

  pageChanged (event) {
    this.currentPageNo = event;
    this.paginationService.paginationConfig.currentPage = event; 
    this.paymentInternalService.filter.skip = (this.paymentInternalService.filter.page_limit * this.paginationService.paginationConfig.currentPage) - this.paymentInternalService.filter.page_limit; 
    this.method_for_getting_list();
  }

  method_for_change_pagination_dropdown () {
      setTimeout(() => {
        this.paginationService.paginationConfig.currentPage = 1
        this.currentPageNo = 1;
        this.paymentInternalService.filter.skip = 0;
        this.paginationService.selected_drop_down_data = this.paginationService.paginationConfig.itemsPerPage;
        this.method_for_getting_list();
      })
  }


  method_for_change_in_filter() {
      this.paginationService.paginationConfig.currentPage = 1;
      this.paymentInternalService.filter.skip  = 0;
      this.method_for_getting_list();
  }



  method_for_getting_list () {
    this.paymentInternalService.method_for_making_dto_filter();
  }

  
}
