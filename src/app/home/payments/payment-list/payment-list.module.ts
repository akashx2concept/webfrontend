import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentListComponent } from './payment-list.component';
import { RouterModule, Routes } from '@angular/router';
import { MaterialModule } from '../../../material.module';


const routes: Routes = [
  {
    path:'',
    component: PaymentListComponent
  }
]

@NgModule({
  declarations: [
    PaymentListComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule.forChild(routes)
  ]
})
export class PaymentListModule { }
