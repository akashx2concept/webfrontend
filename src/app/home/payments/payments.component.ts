import { Component, OnInit } from '@angular/core';
import { PaymentInternalService } from './paymentInternal.service';

@Component({
  selector: 'NA-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent implements OnInit {

  constructor (
    public paymentInternalService: PaymentInternalService
  ) { }

  ngOnInit(): void {
  }

}
