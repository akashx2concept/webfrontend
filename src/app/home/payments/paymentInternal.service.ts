import { Injectable } from "@angular/core";
import { OrderApiService } from "../../services/api/orderApi.service";
import { PaymentApiService } from "../../services/api/paymentApi.service";
import { CookiesService } from "../../services/commonServices/cookies.service";
import { PaginationService } from "../../services/commonServices/pagination.service";


@Injectable({
    providedIn: 'root'
})


export class PaymentInternalService {

    public filter:any = {
        search: null,
        skip: 0,
        page_limit: 10
    }

    public paymentDetailsCount = 0;
    public paymentDetailsData = [];

    public loginUserData;

    public totalSales = 0;
    public totalApplicationFees = 0;

    constructor (private paymentApiService: PaymentApiService, private orderApiService: OrderApiService,
        private cookiesService: CookiesService, private paginationService: PaginationService ) {

    }

    method_for_making_dto_filter () {
        console.log(this.filter, 'filter');
        // this.loginUserData = this.cookiesService.get_user_login_data(true);
        this.method_for_getting_count(this.filter);
    }


    method_for_getting_count (filter) {

      var query:any = {

        }
        const vendor = this.cookiesService.get_current_vendor_details();
        if(vendor && vendor.id){
          query = {
            vendorsId: vendor.id
          }
        }
        query = encodeURI(JSON.stringify(query));

        this.paymentApiService.get_paymentCount(query).subscribe(
            data => {
                this.paymentDetailsCount = data['count'];
                this.paginationService.set_count_data(data['count'], filter['skip']);
                this.method_for_getting_paymentList(filter);
            }, error => {

            }
        )
    }


    method_for_getting_paymentList (filter) {
        this.loginUserData = this.cookiesService.get_user_login_data();
        let query: any = {
            include: [
                { relation: "user" },
                { relation: "orders" },
                { relation: "vendors" }
            ],
            order: "createdAt DESC"
        }
        const vendor = this.cookiesService.get_current_vendor_details();
        if(vendor && vendor.id){
          query['where'] = {
            vendorsId: vendor.id
          }
        }

        query = JSON.stringify(query);
        this.paymentApiService.get_PaymentDetails(query).subscribe(
            data => {
                this.paymentDetailsData = data;
                for (let d=0; d<=this.paymentDetailsData.length-1; d++) {
                    this.paymentDetailsData[d]['paymentObj'] = JSON.parse(this.paymentDetailsData[d]['paymentObj']);
                    this.paymentDetailsData[d]['created'] = this.method_for_converting_utc_second_date(this.paymentDetailsData[d]['created']);
                 }
                console.log(this.paymentDetailsData);
                this.method_for_getting_analysis_TotalSales ();
            }, error => {

            }
        )
    }

    method_for_converting_utc_second_date (utcSeconds) {
        // var utcSeconds = 1596613271;
        var d = new Date(0); // The 0 there is the key, which sets the date to the epoch
        d.setUTCSeconds(utcSeconds);
        return d
    }


    method_for_getting_analysis_TotalSales () {
        var query = {};
        if (this.loginUserData['role'] == 'Root') {
            query = {};
        } else {
          const vendor = this.cookiesService.get_current_vendor_details();
          if(vendor.id){
            query['where'] = {
              vendorsId: vendor.id
            }
          }
        }
        query = JSON.stringify(query);
        this.orderApiService.orderSalesFessAnalysis(query).subscribe(
            data => {
                this.totalSales = data['grandTotal'];
                this.totalApplicationFees = data['totalApplicationFees'];
            }, error => {

            }
        )

    }


    method_for_generating_CSV () {
        var query:any = {
            include: [
                { relation: "user" },
                { relation: "orders" },
                { relation: "vendors" }
            ],
            order: "createdAt DESC"
        }
        const vendor = this.cookiesService.get_current_vendor_details();
        if(vendor && vendor.id){
          query['where'] = {
            vendorsId: vendor.id
          }
        }
        query = JSON.stringify(query);

        this.paymentApiService.get_PaymentDetails(query).subscribe(
            data => {
                console.log(data);

                var arrayData = [];

                for (let a=0; a<=data.length-1; a++) {
                    let g = [];

                    let customerName = "";
                    if (data[a]['userId'] && data[a]['user']['firstName']) customerName = `${customerName} ${data[a]['user']['firstName']}`;
                    if (data[a]['userId'] && data[a]['user']['middleName']) customerName = `${customerName} ${data[a]['user']['middleName']}`;
                    if (data[a]['userId'] && data[a]['user']['lastName']) customerName = `${customerName} ${data[a]['user']['lastName']}`;
                    g.push(`${customerName}`);

                    let email = data[a]['user']['email'];
                    g.push(email);

                    let vendorsName = "";
                    if (data[a]['vendorsId'] && data[a]['vendors']['storeName']) vendorsName = `${vendorsName} ${data[a]['vendors']['storeName']}`;
                    g.push(vendorsName);

                    let applicationFee = 0;
                    if (data[a]['ordersId'] && data[a]['orders']['application_fee_amount']) applicationFee =  parseInt(data[a]['orders']['application_fee_amount'])/100;
                    g.push(applicationFee);
                    
                    let grandTotal = 0;
                    if (data[a]['ordersId'] && data[a]['orders']['grandTotal']) grandTotal =  parseInt(data[a]['orders']['grandTotal'])/100;
                    g.push(grandTotal);

                    let orderID = "";
                    if (data[a]['ordersId'] && data[a]['orders']['orderNumber']) orderID =  data[a]['orders']['orderNumber'];
                    g.push(orderID);

                    let paymentStatus = "";
                    if (data[a]['paymentIntentId'] && data[a]['status']) paymentStatus =  data[a]['status'];
                    g.push(paymentStatus);

                    let orderTime:any = "";
                    if (data[a]['ordersId'] && data[a]['orders']['orderDate']) orderTime =  new Date (parseInt(data[a]['orders']['orderDate']));
                    g.push(orderTime);
                    
                    arrayData.push(g);
                }

                console.log(arrayData);
                setTimeout(() => {
                this.download_csv(arrayData);
                }, 2000)
            }, error => {

            }
        )

    }


    download_csv(data) {
        var csv = 'Customer Name,Customer Email,Vendors,Application Fee, Total Amount,Order Number, Payment Status, Order Date\n';
        data.forEach(function(row) {
                csv += row.join(',');
                csv += "\n";
        });
     
        // console.log(csv);
        var hiddenElement = document.createElement('a');
        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
        hiddenElement.target = '_blank';
        hiddenElement.download = `payment_${new Date()}.csv`;
        hiddenElement.click();
    }


}
