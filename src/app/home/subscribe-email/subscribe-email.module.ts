import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes , RouterModule } from '@angular/router';
import { MaterialModule } from '../../material.module';
import { AuthGuardService } from '../../services/commonServices/auth-guard/auth-guard.service';
import { BreadcrumbModule } from '../../commonModule/breadcrumb/breadcrumb.module';
import { ConfirmActionModule } from '../../commonModule/confirm-action/confirm-action.module';
import { SubscribeEmailListComponent } from './subscribe-email-list/subscribe-email-list.component';

const routes: Routes = [
  {
    path: '', component: SubscribeEmailListComponent
  }
]



@NgModule({
  declarations: [
    SubscribeEmailListComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    BreadcrumbModule,
    ConfirmActionModule,
    RouterModule.forChild(routes)
  ]
})
export class SubscribeEmailModule { }
// get_subscrie_email
