import { Injectable } from "@angular/core";
import { ProductApiService } from "../../services/api/productApi.service";
import { SubscribeEmailService } from "../../services/api/subscribe-email.service";
import { UserApiService } from "../../services/api/userApi.service";
import { VendorsApiService } from "../../services/api/vendorsApi.service";





@Injectable({
    providedIn: 'root'
})


export class SubscribeEmailInternalService {


    public filter:any = {
        search: null,
        skip: 0,
        page_limit: 10
    }


    public emailList = [];

    constructor (private subscribeEmailService: SubscribeEmailService) {

    }

    method_for_getting_list_of_subscribe () {
        var query:any = {

        };
        query = encodeURI(JSON.stringify(query));

        this.subscribeEmailService.get_subscrie_email(query).subscribe(
            (data : any) => {
                this.emailList = data;
            }, error => {

            }
        )
    }


}
