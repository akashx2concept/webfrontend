import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { ConfirmActionComponent } from '../../../commonModule/confirm-action/confirm-action.component';
import { ProductApiService } from '../../../services/api/productApi.service';
import { StripePaymentApiService } from '../../../services/api/stripePaymentApi.service';
import { SubscribeEmailService } from '../../../services/api/subscribe-email.service';
import { CookiesService } from '../../../services/commonServices/cookies.service';
import { NotificationService } from '../../../services/commonServices/notification.service';

import { SubscribeEmailInternalService } from '../subscribeEmailInternalService';

declare const Stripe;


@Component({
  selector: 'NA-subscribe-email-list',
  templateUrl: './subscribe-email-list.component.html',
  styleUrls: ['./subscribe-email-list.component.scss']
})
export class SubscribeEmailListComponent implements OnInit {


  private subEmail
  // public stripe = Stripe('pk_test_51IdCiXE1e1bineRZmQBEy9nIRvX8O2rry6UsxQjvxLjS0EliBzUXy0eYuBboFlyL4WlyxfpqiZS8FhTBA8NxCMte005tOlGUUH');
  private subscribe;

  constructor(
    public subscribeEmailInternalService: SubscribeEmailInternalService,
    private subscribeEmail: SubscribeEmailService,
    private notificationService: NotificationService,
    private dialog: MatDialog) {

   }

  ngOnInit(): void {
    this.subscribeEmailInternalService.method_for_getting_list_of_subscribe();
  }

  deleteSubscription(item){
    this.subscribeEmail.delete_subscrie_email(item.id)
    .subscribe(res => {
      this.notificationService.get_Notification('success', 'Email has been unsubscribed');
      this.subscribeEmailInternalService.method_for_getting_list_of_subscribe();
    })
  }

}
