import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SalesReportComponent } from './sales-report.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BreadcrumbModule } from '../../commonModule/breadcrumb/breadcrumb.module';
import { MaterialModule } from '../../material.module';

const routes: Routes = [
  {
    path: '',
    component: SalesReportComponent
  }
]

@NgModule({
  declarations: [
    SalesReportComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    BreadcrumbModule,
    RouterModule.forChild(routes)
  ]
})
export class SalesReportModule { }
