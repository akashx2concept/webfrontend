import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { PaginationService } from "../../../services/commonServices/pagination.service";
import { VendorInternalService } from "../vendorInternalService";




@Component ({
    selector: 'NA-venderList',
    templateUrl: './venderList.component.html',
    styleUrls: [ './venderList.component.scss' ]
})


export class VenderListComponent implements OnInit {

    public currentPageNo:any = 1;

    constructor (public  vendorInternalService: VendorInternalService, private router: Router, 
        public paginationService: PaginationService ) {

    }

    ngOnInit () {
        this.method_for_getting_vendor();
        this.vendorInternalService.method_for_getting_data();
      }
  
      pageChanged (event) {
        this.currentPageNo = event;
        this.paginationService.paginationConfig.currentPage = event; 
        this.vendorInternalService.filter.skip = (this.vendorInternalService.filter.page_limit * this.paginationService.paginationConfig.currentPage) - this.vendorInternalService.filter.page_limit; 
        this.method_for_getting_vendor();
      }
  
      method_for_change_pagination_dropdown () {
          setTimeout(() => {
            this.paginationService.paginationConfig.currentPage = 1
            this.currentPageNo = 1;
            this.vendorInternalService.filter.skip = 0;
            this.paginationService.selected_drop_down_data = this.paginationService.paginationConfig.itemsPerPage;
            this.method_for_getting_vendor();
          })
      }
  
  
      method_for_change_in_filter() {
          this.paginationService.paginationConfig.currentPage = 1;
          this.vendorInternalService.filter.skip  = 0;
          this.method_for_getting_vendor();
      }
    

    method_for_getting_vendor () {
        this.vendorInternalService.method_for_making_dto_filter();
    }

    method_for_vendorDetails (item) {
        this.router.navigate(['home/vendor/vendor-detail', item.id])
    }


}