import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { MaterialModule } from "../../../material.module";
import { AddVendorsComponent } from '../addVendors/addVendors.component'
// import { GoogleMapsAngularModule } from 'google-maps-angular';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgOptionHighlightModule } from '@ng-select/ng-option-highlight';
import { GoogleMapsModule } from "@angular/google-maps";
import { ImageCropperModule } from "ngx-image-cropper";
import { BreadcrumbModule } from "../../../commonModule/breadcrumb/breadcrumb.module";
import { FormDirModule } from "../../../commonModule/form-dir/form-dir.module";


const routes: Routes = [
    {
        path: '',
        component: AddVendorsComponent
    }
]



@NgModule({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        MaterialModule,
        RouterModule.forChild(routes),
        GooglePlaceModule,
        NgSelectModule,
        NgOptionHighlightModule,
        GoogleMapsModule,
        ImageCropperModule,
        BreadcrumbModule,
        FormDirModule
    ],
    declarations: [
        AddVendorsComponent
    ],
    exports: [
        AddVendorsComponent
    ]
})


export class AddVendorsModule {


}
