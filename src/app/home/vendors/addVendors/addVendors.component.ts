import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { CityApiService } from "../../../services/api/cityApi";
import { CountryApiService } from "../../../services/api/countryApi";
import { StateApiService } from "../../../services/api/stateApi";
import { UserApiService } from "../../../services/api/userApi.service";
import { NotificationService } from "../../../services/commonServices/notification.service";
import { CategoriesApiService } from "../../../services/api/CategoriesApi.service";
import { VendorsApiService } from "../../../services/api/vendorsApi.service";
import * as moment from 'moment';
import { VendorsTimingApiService } from "../../../services/api/vendorsTimingApi.service";
import { CookiesService } from "../../../services/commonServices/cookies.service";

@Component ({
    selector: 'NA-addVendors',
    templateUrl: './addVendors.component.html',
    styleUrls: [ './addVendors.component.scss' ]
})


export class AddVendorsComponent implements OnInit {

    public supplierInformation: FormGroup;
    public storeInformation: FormGroup;

    public countryList = [];
    public stateList = [];
    public cityList = [];

    public selectedCountry;
    public selectedState;
    public selectedCity;

    public disabled = false;
    public showSpinners = true;
    public showSeconds = false;
    public date: moment.Moment = moment().startOf('day');
    public stepHour = 1;
    public stepMinute = 1;
    public stepSecond = 1;

    public OpeningClosingHours = [
        { weekDay: "Monday", startTime: moment().startOf('day'), endTime: moment().endOf('day') },
        { weekDay: "Tuesday", startTime: moment().startOf('day'), endTime: moment().endOf('day') },
        { weekDay: "Wednesday", startTime: moment().startOf('day'), endTime: moment().endOf('day') },
        { weekDay: "Thursday", startTime: moment().startOf('day'), endTime: moment().endOf('day') },
        { weekDay: "Friday", startTime: moment().startOf('day'), endTime: moment().endOf('day') },
        { weekDay: "Saturday", startTime: moment().startOf('day'), endTime: moment().endOf('day') },
        { weekDay: "Sunday", startTime: moment().startOf('day'), endTime: moment().endOf('day') },
    ]



    center: google.maps.LatLngLiteral = {lat: 24, lng: 12};
    zoom = 8;
    markerOptions: google.maps.MarkerOptions = {draggable: false};
    markerPositions: google.maps.LatLngLiteral[] = [];

    showMap = false;
    public width;

    public latitude;
    public longitude;

    public designationList = [
        { id: 1, name: "Business Owner" },
        { id: 2, name: "Authorized Representative" }
    ];

    public storeAddressType_List = [
        { id: 1, name: "Same as business address" },
        { id: 2, name: "New Address" },
        { id: 3, name: "No Physical Store" }
    ]

    public vaendorCategoriesList = [];

    public updateUserId = null;
    public updateUserData = null;
    public updateVendorId = null;
    public updateVendorData = null;

    public  step = 0;

    public loginUserData;
    public userRole;

    constructor (private formBuilder: FormBuilder, private countryApiService: CountryApiService,
        private stateApiService: StateApiService, private cityApiService: CityApiService,
        private userApiService: UserApiService, private router: Router,
        private notificationService: NotificationService, private activatedRoute: ActivatedRoute,
        private categoriesApiService: CategoriesApiService, private vendorsApiService: VendorsApiService,
        private vendorsTimingApiService: VendorsTimingApiService, private cookiesService: CookiesService ) {

            setTimeout(() => {
                console.log(this.date, "date date")
            }, 1000*10)


        this.router.events.subscribe(( data ) => {
            if (data instanceof NavigationEnd ) {
                this.updateUserId = this.activatedRoute.snapshot.paramMap.get('userId');
                this.updateVendorId = this.activatedRoute.snapshot.paramMap.get('venderId');
                console.log(this.updateUserId, "==== updateUserId" )

                this.loginUserData = this.cookiesService.get_user_login_data();
                this.userRole = this.loginUserData['role'];

                if (this.updateUserId) {
                    this.method_for_getting_User(this.updateUserId);
                    this.method_for_getting_country();
                    this.method_for_getting_vendorCategories ();
                    if (this.updateVendorId) {
                        this.method_for_getting_vendor (this.updateVendorId);
                        this.method_for_get_update_time();
                    }
                }
            }
        })

    }

    ngOnInit () {
        this.validateForm();
        this.setStep(0)
        this.renovePasswordValidators();
    }


    renovePasswordValidators(){
      if(this.updateUserId){
        this.SUI_f.PS_accountPassword.setValidators([]);
        this.SUI_f.PS_confirmPassword.setValidators([]);
        this.SUI_f.PS_accountPassword.updateValueAndValidity();
        this.SUI_f.PS_confirmPassword.updateValueAndValidity();
      }
    }

    validateForm () {
        this.supplierInformation = this.formBuilder.group({
            SUI_id: [],
            SUI_firstName: ['', Validators.required],
            SUI_lastName: ['', Validators.required],
            SUI_designation: ['', Validators.required],
            PS_businessEmailAddress: ['', [Validators.required, Validators.email]],
            PS_accountPassword: ['', [Validators.required, Validators.minLength(8),
                Validators.maxLength(25),
                Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{7,}')
            ]],
            PS_confirmPassword: ['', [Validators.required, Validators.minLength(8),
                Validators.maxLength(25),
                Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{7,}')
            ]],
        })

        this.storeInformation = this.formBuilder.group({
            SI_id: [],
            SI_businessName: ['', Validators.required],
            SI_aboutTheStore: ['', Validators.required],
            SI_businessAddress: ['', Validators.required],
            SI_businessGoogleAddress: ['', Validators.required],
            SI_state: [null, Validators.required],
            SI_city: [null, Validators.required],
            SI_country: [null, Validators.required],
            SI_postal_code: ['', Validators.required],
            SI_businessRegistrationNumber: [''],
            SI_businessPhoneNumber: ['', Validators.required],
            SI_mainBusinessArea: [null, Validators.required],
            SI_storeName: ['', Validators.required],
            SI_storeAddressType: [null, Validators.required],
            SI_storeAddress: ['', Validators.required],
            SI_latitude: [],
            SI_longitude: []
        })



    }

    get SUI_f() { return this.supplierInformation.controls; };
    get SI_f() { return this.storeInformation.controls; };


    method_for_createUpdate_SupplierInformation () {
      this.method_for_create_supplierInformation();
    }


    method_for_create_supplierInformation () {
        if(this.supplierInformation.invalid){
            return false;
        }
        let userDto = {
            id: this.supplierInformation.value['SUI_id'],
            firstName: this.supplierInformation.value['SUI_firstName'],
            lastName: this.supplierInformation.value['SUI_lastName'],
            role: "Vendor",
            email: this.supplierInformation.value['PS_businessEmailAddress'],
            password: this.supplierInformation.value['PS_accountPassword'],
            vendorDesignation: this.supplierInformation.value['SUI_designation'],
        }
        if(userDto.id){
          delete userDto.password;
        }
        const API = userDto.id ? this.userApiService.patch_users: this.userApiService.post_users;
        API.bind(this.userApiService)(userDto).subscribe(
            data => {
                this.notificationService.get_Notification('success', "Successfully Created");

                if (this.userRole == 'Root') {
                    if(this.updateVendorId){
                      this.step =1;
                      this.router.navigate(['home/vendor/addVendors', data['id'], this.updateVendorId])
                    }else{
                      this.router.navigate(['home/vendor/addVendors', data['id']]);
                    }

                } else if (this.userRole == 'Vendor') {
                  if(this.updateVendorId){
                    this.step =1;
                    this.router.navigate(['vendor/addVendors', data['id'], this.updateVendorId])
                  }else{
                    this.router.navigate(['vendor/addVendors', data['id']])
                  }
                }
                this.nextStep();
            }, error => {

            }
        )
    }

    method_for_createUpdate_StoreInformation () {
        this.method_for_create_StoreInformation();
    }

    method_for_create_StoreInformation () {
        if(this.storeInformation.invalid){
            return false;
        }

        let dataDto = {
            id: this.storeInformation.value['SI_id'],
            businessName: this.storeInformation.value['SI_businessName'],
            vendorName: `${this.supplierInformation.value['SUI_firstName']} ${this.supplierInformation.value['SUI_lastName']}`,
            designation: this.supplierInformation.value['SUI_designation'],
            venderMobileNo: this.storeInformation.value['SI_businessPhoneNumber'],
            RegistrationNo: this.storeInformation.value['SI_businessRegistrationNumber'],
            businessAddress: this.storeInformation.value['SI_businessAddress'],
            businessGoogleAddress: this.storeInformation.value['SI_businessGoogleAddress'],
            postalCode: this.storeInformation.value['SI_postal_code'],
            aboutUs: this.storeInformation.value['SI_aboutTheStore'],
            categoriesId: this.storeInformation.value['SI_mainBusinessArea'],
            storeName: this.storeInformation.value['SI_storeName'],
            storeAddressType: this.storeInformation.value['SI_storeAddressType'],
            storeAddress: this.storeInformation.value['SI_storeAddress'],
            countryId: this.storeInformation.value['SI_country'],
            stateId: this.storeInformation.value['SI_state'],
            cityId: this.storeInformation.value['SI_city'],
            latitude: this.storeInformation.value['SI_latitude'],
            longitude: this.storeInformation.value['SI_longitude'],
            userId: this.updateUserId
        }


        const API = dataDto.id ? this.vendorsApiService.update_Vendors : this.vendorsApiService.create_Vendors;
        API.bind(this.vendorsApiService)(dataDto).subscribe(
            data => {
                this.notificationService.get_Notification('success', "Successfully Created");
                this.router.navigate(['home/vendor/addVendors', this.updateUserId, data['id'] ]);
                this.nextStep();
            }, error => {

            }
        )

    }



    method_for_getting_User (updateUserId) {
        let query = {
            where: {
                id: updateUserId
            }
        }
        this.userApiService.get_user(JSON.stringify(query)).subscribe(
            data => {
                if (data.length) {
                    this.updateUserData = data[0];
                    this.supplierInformation.setValue({
                        SUI_id : this.updateUserData.id,
                        SUI_firstName: this.updateUserData['firstName'],
                        SUI_lastName: this.updateUserData['lastName'],
                        SUI_designation: this.updateUserData['vendorDesignation'],
                        PS_businessEmailAddress: this.updateUserData['email'],
                        PS_accountPassword: this.supplierInformation.value['PS_accountPassword'],
                        PS_confirmPassword: this.supplierInformation.value['SUI_designation'],
                    })
                    this.nextStep();
                }
            }, error => {

            }
        )
    }

    method_for_getting_vendor (updateVendorId) {
        let query = {
            where: {
                id: updateVendorId
            },
            include: [
                { relation: 'country' },
                { relation: 'state' },
                { relation: 'city' },
                { relation: 'user' },
                { relation: 'categories' },
            ]
        }
        this.vendorsApiService.get_Vendors(JSON.stringify(query)).subscribe(
            data => {
                if (data.length) {
                    this.updateVendorData = data[0];
                    this.storeInformation.setValue({
                        SI_id: this.updateVendorData.id,
                        SI_businessName: this.updateVendorData['businessName'],
                        SI_aboutTheStore: this.updateVendorData['aboutUs'],
                        SI_businessAddress: this.updateVendorData['businessAddress'],
                        SI_businessGoogleAddress: this.updateVendorData['businessGoogleAddress'],
                        SI_state: this.updateVendorData['stateId'],
                        SI_city: this.updateVendorData['cityId'],
                        SI_country: this.updateVendorData['countryId'],
                        SI_postal_code: this.updateVendorData['postalCode'],
                        SI_businessRegistrationNumber: this.updateVendorData['RegistrationNo'],
                        SI_businessPhoneNumber: this.updateVendorData['venderMobileNo'],
                        SI_mainBusinessArea: this.updateVendorData['categoriesId'],
                        SI_storeName: this.updateVendorData['storeName'],
                        SI_storeAddressType: this.updateVendorData['storeAddressType'],
                        SI_storeAddress: this.updateVendorData['storeAddress'],
                        SI_latitude: this.updateVendorData['latitude'],
                        SI_longitude: this.updateVendorData['longitude']
                    })
                    this.method_for_change_in_country (this.updateVendorData['countryId']);
                    this.method_for_change_in_state(this.updateVendorData['stateId']);

                    this.center  = <google.maps.LatLngLiteral>{lat: parseInt(this.updateVendorData['latitude']), lng: parseInt(this.updateVendorData['longitude'])};
                    this.markerPositions.push({lat: parseInt(this.updateVendorData['latitude']), lng: parseInt(this.updateVendorData['longitude'])});
                    this.zoom = 12;
                    this.latitude = parseInt(this.updateVendorData['latitude']);
                    this.longitude = parseInt(this.updateVendorData['longitude']);

                    this.nextStep();
                }
            }, error => {

            }
        )
    }

    method_for_get_update_time() {
      var query = {
        where: { vendorsId: this.updateVendorId }
        }
        this.vendorsTimingApiService.get_VendorTimings(JSON.stringify(query)).subscribe(data => {
              if(data.length){
                  const dataTime = data.map(rec => {
                    let sTime = rec.startTime.split(':');
                    let eTime = rec.endTime.split(':');
                    sTime = +sTime[0]*60 + +sTime[1];
                    eTime = +eTime[0]*60 + +eTime[1];
                    rec.startTime = moment().startOf('day').minutes(sTime);
                    rec.endTime = moment().startOf('day').minutes(eTime);
                    return rec;
                  });
                  this.OpeningClosingHours = [...dataTime];
              }
        })
    }

    method_for_create_update_time () {
        console.log(this.OpeningClosingHours);

        var query = {
            where: { vendorsId: this.updateVendorId }
        }
        this.vendorsTimingApiService.get_VendorTimings(JSON.stringify(query)).subscribe(
            data => {

                if (!data.length) {
                    let hourTIme = Object.assign([], this.OpeningClosingHours);
                    for (let a=0; a<=hourTIme.length-1; a++) {
                        let dataDto = {
                            weekDay: hourTIme[a]['weekDay'],
                            startTime: hourTIme[a]['startTime'].format("HH:mm"),
                            endTime: hourTIme[a]['endTime'].format("HH:mm"),
                            vendorsId: this.updateVendorId
                        }
                        this.vendorsTimingApiService.create_VendorTimings(dataDto).subscribe(
                            data => {
                                if (this.userRole == 'Root') {
                                    this.router.navigate(['home/vendor/vendor-detail', this.updateUserId]);
                                } else if (this.userRole == 'Vendor') {
                                    this.router.navigate(['vendor/vendor-detail']);
                                }

                            }, error => {

                            }
                        )
                    }
                } else {
                  let hourTIme = Object.assign([], this.OpeningClosingHours);
                  for (let a=0; a<=hourTIme.length-1; a++) {
                      console.log("===============================");
                      console.log(hourTIme[a]['weekDay']);
                      console.log(hourTIme[a]['startTime'].format("HH:mm"));
                      console.log(hourTIme[a]['endTime'].format("HH:mm"));
                      hourTIme[a]['startTime'] = hourTIme[a]['startTime'].format("HH:mm");
                      hourTIme[a]['endTime'] = hourTIme[a]['endTime'].format('HH:mm')
                      let dataDto = {
                          ...hourTIme[a]
                      }
                      this.vendorsTimingApiService.update_VendorTimings(dataDto).subscribe(
                          data => {
                            if (this.userRole == 'Root') {
                                this.router.navigate(['home/vendor/vendor-detail', this.updateUserId]);
                            } else if (this.userRole == 'Vendor') {
                                this.router.navigate(['vendor/vendor-detail']);
                            }
                          }, error => {

                          }
                      )
                  }
                }


            }, error => {

            }
        )
    }

    method_for_getting_country (searchVal?) {
        console.log(searchVal)
        let query:any = {
            where: {
                isActive: true,
                isDeleted: false
            },
        }
        query = encodeURI(JSON.stringify(query));
        this.countryApiService.get_countries(query).subscribe(
            data => {
                this.countryList = data;
            }, error => {

            }
        )
    }

    method_for_change_in_country (value) {
        console.log(value);
        this.selectedCountry = value;
        let query:any = {
            where: {
                countryId: value['id'],
                isActive: true,
                isDeleted: false
            },
        }
        query = encodeURI(JSON.stringify(query));
        this.stateApiService.get_states(query).subscribe(
            data => {
                this.stateList = data;
            }, error => {

            }
        )
    }

    method_for_change_in_state (value) {
        console.log(value);
        this.selectedState = value;
        let query:any = {
            where: {
                stateId: value['id'],
                isActive: true,
                isDeleted: false
            },
        }
        query = encodeURI(JSON.stringify(query));
        this.cityApiService.get_cities(query).subscribe(
            data => {
                this.cityList = data;
            }, error => {

            }
        )
    }

    method_for_change_in_city (value) {
        this.selectedCity = value;
    }

    method_for_getting_vendorCategories () {
        let query = {
            where: {
                type: "Vendor"
            }
        }
        this.categoriesApiService.get_Categories(JSON.stringify(query)).subscribe(
            data => {
                this.vaendorCategoriesList = data;
            }, error => {

            }
        )
    }

    markerClicked (event) {
        console.log(event);
    }

    handleAddressChange (address) {
        console.log(address);
        this.markerPositions = [];
        this.center  = <google.maps.LatLngLiteral>{lat:address.geometry.location.lat(), lng:address.geometry.location.lng()};
        this.markerPositions.push({lat:address.geometry.location.lat(), lng:address.geometry.location.lng()});
        this.zoom = 12;
        this.latitude = address.geometry.location.lat();
        this.longitude = address.geometry.location.lng()
        this.storeInformation.patchValue({
            SI_businessAddress: address['formatted_address'],
            SI_businessGoogleAddress: address['formatted_address'],
            SI_storeAddress: address['formatted_address'],
            SI_latitude : this.latitude,
            SI_longitude: this.longitude
        });
        console.log(this.latitude, "==== latitude");
        console.log(this.longitude, "==== longitude");
        console.log(this.storeInformation.value, "==== Vendor");
    }

    addMarker(event: google.maps.MapMouseEvent) {
      // this.markerPositions.push(event.latLng.toJSON());
    }


    setStep(index: number) {
        this.step = index;
    }

    nextStep() {
        this.step++;
    }

    prevStep() {
        this.step--;
    }

    method_for_redirect(){
      window.history.back();
    }
}
