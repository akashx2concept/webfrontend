import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { MaterialModule } from "../../../material.module";
import { AuthGuardService } from "../../../services/commonServices/auth-guard/auth-guard.service";
import { ProductCategoryRequestComponent } from "../../categories/productCategories/product-category-request/product-category-request.component";
import { ProductCategoryRequestModule } from "../../categories/productCategories/product-category-request/product-category-request.module";
import { VendorCategoryRequestComponent } from "../../categories/venderCategories/vendor-category-request/vendor-category-request.component";
import { VendorCategoryRequestModule } from "../../categories/venderCategories/vendor-category-request/vendor-category-request.module";
import { CategoriesRequestsComponent } from "./categories-requests.component";




const routes: Routes = [
    {
        path: '',
        component: CategoriesRequestsComponent,
        children: [
            {
                path: 'vendor-category-request',
                component: VendorCategoryRequestComponent,
                canActivate: [AuthGuardService]
            },
            {
                path: 'product-category-request',
                component: ProductCategoryRequestComponent,
                canActivate: [AuthGuardService]
            }
        ]
    }
]


@NgModule({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        MaterialModule,
        ProductCategoryRequestModule,
        VendorCategoryRequestModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
      CategoriesRequestsComponent
    ],
    exports: [
      CategoriesRequestsComponent
    ]
})

export class CategoriesRequestsModule {

}
