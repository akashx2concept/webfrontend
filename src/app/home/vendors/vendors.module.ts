import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { DebounceModule } from "ngx-debounce";
import { BreadcrumbModule } from "../../commonModule/breadcrumb/breadcrumb.module";
import { MaterialModule } from "../../material.module";
import { AuthGuardService } from "../../services/commonServices/auth-guard/auth-guard.service";
import { VenderListComponent } from "./venderList/venderList.component";
import { VendorsComponent } from "./vendors.component";
import { VendorsCardComponent } from "./vendorsCard/vendorsCard.component";



const routes: Routes = [
    {
        path: '',
        component: VendorsComponent
    },
    {
        path: 'addVendors',
        loadChildren: () => import ('./addVendors/addVendors.module').then(m => m.AddVendorsModule),
        canActivate: [AuthGuardService],
    },
    {
        path: 'addVendors/:userId',
        loadChildren: () => import ('./addVendors/addVendors.module').then(m => m.AddVendorsModule),
        canActivate: [AuthGuardService],
    },
    {
        path: 'addVendors/:userId/:venderId',
        loadChildren: () => import ('./addVendors/addVendors.module').then(m => m.AddVendorsModule),
        canActivate: [AuthGuardService],
    },
    {
      path: 'vendor-detail/:id',
      loadChildren:() => import('./vendor-detail/vendor-detail.module').then(m => m.VendorDetailModule),
      canActivate: [AuthGuardService],
    },
    {
      path: 'vendor-requests',
      loadChildren: () => import('./vendor-request/vendor-request.module').then(m => m.VendorRequestModule)
    },
    {
      path:'reported-vendor',
      loadChildren: () => import('./vendor-reported/vendor-reported.module').then(m => m.VendorReportedModule)
    }

]



@NgModule ({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        RouterModule.forChild(routes),
        MaterialModule,
        BreadcrumbModule,
        DebounceModule
    ],
    declarations: [
        VendorsComponent,
        VendorsCardComponent,
        VenderListComponent
    ],
    exports: [
        VendorsComponent,
        VendorsCardComponent,
        VenderListComponent
    ]
})


export class VendorsModule {


}
