import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendorReportedListComponent } from './vendor-reported-list.component';



@NgModule({
  declarations: [
    VendorReportedListComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [VendorReportedListComponent]
})
export class VendorReportedListModule { }
