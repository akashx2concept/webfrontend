import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendorReportedComponent } from './vendor-reported.component';
import { VendorReportedListModule } from './vendor-reported-list/vendor-reported-list.module';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { BreadcrumbModule } from '../../../commonModule/breadcrumb/breadcrumb.module';
import { MaterialModule } from '../../../material.module';

const routes : Routes = [
  {
    path: '',
    component: VendorReportedComponent
  }
]

@NgModule({
  declarations: [
    VendorReportedComponent
  ],
  imports: [
    CommonModule,
    VendorReportedListModule,
    ReactiveFormsModule,
    MaterialModule,
    BreadcrumbModule,
    RouterModule.forChild(routes)
  ]
})
export class VendorReportedModule { }
