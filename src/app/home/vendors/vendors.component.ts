import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { PaginationService } from "../../services/commonServices/pagination.service";
import { VendorInternalService } from "./vendorInternalService";
import { VendorsInternalService } from "./vendorsInternal.service";




@Component({
    selector: 'NA-vendors',
    templateUrl: './vendors.component.html',
    styleUrls: [ './vendors.component.scss' ]
})


export class VendorsComponent implements OnInit {



    constructor (public vendorInternalService: VendorInternalService, private router: Router, 
        private paginationService: PaginationService ) {

    }


    ngOnInit () {

    }

    method_for_change_in_filter () {
        this.paginationService.paginationConfig.currentPage = 1;
        this.vendorInternalService.filter.skip  = 0;
        this.method_for_getting_vendor();
    }
  

    method_for_getting_vendor () {
        this.vendorInternalService.method_for_making_dto_filter();
    }
    
    method_for_redirect(value) {
        this.router.navigate([`home/vendor/${value}`])
    }

}