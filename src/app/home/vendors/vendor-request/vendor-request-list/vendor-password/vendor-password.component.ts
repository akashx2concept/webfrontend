import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'NA-vendor-password',
  templateUrl: './vendor-password.component.html',
  styleUrls: ['./vendor-password.component.scss']
})
export class VendorPasswordComponent implements OnInit {
  passwordForm:FormGroup;
  constructor(
    public dialogRef: MatDialogRef<VendorPasswordComponent>,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {

   }

  ngOnInit(): void {
    this.initForm()
  };

  initForm(){
    this.passwordForm = this.fb.group({
      newPassword: ['',  [Validators.required, Validators.minLength(8),
        Validators.maxLength(25),
        Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{7,}')
      ]],
      cnfPassword:  ['',  [Validators.required, Validators.minLength(8),
        Validators.maxLength(25),
        Validators.pattern('(?=.*[A-Za-z])(?=.*[0-9])(?=.*[$@$!#^~%*?&,.<>"\'\\;:\{\\\}\\\[\\\]\\\|\\\+\\\-\\\=\\\_\\\)\\\(\\\)\\\`\\\/\\\\\\]])[A-Za-z0-9\d$@].{7,}')
      ]]
    },  { validators: this.checkPasswords })
  }

  get f() { return this.passwordForm.controls; };

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    const password = group.get('newPassword').value;
    const confirmPassword = group.get('cnfPassword').value;

    return password === confirmPassword ? null : { notSame: true }
  }

  setPassword(){
    // debugger
      if(this.passwordForm.invalid){
        return false;
      }
      this.dialogRef.close(this.passwordForm.get('newPassword').value);
  }


}
