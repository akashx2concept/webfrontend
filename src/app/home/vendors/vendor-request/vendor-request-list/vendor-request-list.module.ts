import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendorRequestListComponent } from './vendor-request-list.component';
import { MaterialModule } from '../../../../material.module';
import { VendorPasswordComponent } from './vendor-password/vendor-password.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    VendorRequestListComponent,
    VendorPasswordComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  entryComponents:[VendorPasswordComponent],
  exports: [
    VendorRequestListComponent
  ]
})
export class VendorRequestListModule { }
