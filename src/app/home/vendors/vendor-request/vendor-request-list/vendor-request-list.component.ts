import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { subscribeOn } from 'rxjs/operators';
import { BecomeAVendorApiService } from '../../../../services/api/becomeAVendorApi.service';
import { UserApiService } from '../../../../services/api/userApi.service';
import { VendorsApiService } from '../../../../services/api/vendorsApi.service';
import { CookiesService } from '../../../../services/commonServices/cookies.service';
import { NotificationService } from '../../../../services/commonServices/notification.service';
import { VendorPasswordComponent } from './vendor-password/vendor-password.component';

@Component({
  selector: 'NA-vendor-request-list',
  templateUrl: './vendor-request-list.component.html',
  styleUrls: ['./vendor-request-list.component.scss']
})
export class VendorRequestListComponent implements OnInit {
  venderList  = [];
  hasLoaded = false;
  vendorRequest;
  slectedVendor;
  selectedUserId;
  selectedVendorId;
  requestType ;
  constructor(
    private becomeAVendorApiService: BecomeAVendorApiService,
    private cookiesService: CookiesService,
    private notificationService: NotificationService,
    private userApiService: UserApiService,
    private VendorsApiService: VendorsApiService,
    private dialog:MatDialog) { }

  ngOnInit(): void {
    this.method_for_getting_list ()
  }
  method_for_getting_list () {
    let query = {
      }
  this.becomeAVendorApiService.get_become_a_vendor(JSON.stringify(query)).subscribe(
        data => {
            this.venderList = data;
            this.hasLoaded = true;
        }, error => {

        }
    );
  }

  approveAndReject(vendor, type){
     this.requestType = type;
      if(type == 2){
        this.rejectVendor(vendor);
      }
      if(type == 1 || type == 3){
        this.vendorRequest = vendor;
        this.retrivePassword();
      }
  }

  rejectVendor(vendor){
      const query = {
        id: vendor.id,
        currentStatus: 'Rejected'
      }
      this.becomeAVendorApiService.update_become_a_vendor(query)
      .subscribe(res => {
        this.notificationService.get_Notification('success', 'Request has been rejected');
        this.method_for_getting_list();
        if(vendor.userId){
          this.updateUserProfile(vendor.userId);
        }
      });
  }

  updateUserProfile(userId){
    let API;
  const dto = {
    id: userId,
    isVendorRequestSubmitted: false
   }
  this.userApiService.patch_users(dto).subscribe(res => {});
  }

  retrivePassword(){
    this.dialog.open(VendorPasswordComponent, {
        width: '400px',
        data: {}
    })
    .afterClosed()
    .subscribe(res => {
      if(res){
        this.createUser(res);
      }
    });
   }

 createUser(password){
  let API;
  const dto = {
    "firstName": this.vendorRequest.firstName,
    "middleName": this.vendorRequest.middleName,
    "lastName": this.vendorRequest.lastName,
    "email": this.vendorRequest.email,  // hard code email for testing
    "username": this.vendorRequest.email,
    "role": "Vendor",
    "password": password,
    vendorDesignation: this.vendorRequest.designation
   }

  if(this.vendorRequest.userId){
    dto['id'] = this.vendorRequest.userId;
    API = this.userApiService.patch_users(dto);
  }else {
    API = this.userApiService.post_users(dto);
  }

  API.subscribe(res => {
      this.selectedUserId = res.id;
      this.method_for_sending_email_to_cendor (dto);
      if(this.requestType !== 3){
        this.createVendor();
      }
   });
 }


 method_for_sending_email_to_cendor (dto) {
    var dataDto = {
      email: dto['email'],
      password: dto['password']
    }
    this.userApiService.sendVendorApproveMail(dataDto).subscribe(
      data => {
        if(this.requestType == 3){
          this.notificationService.get_Notification('success','Email has been sent');
        }
      }, error => {

      }
    )
 }

 createVendor(){
   const dto = {
     ...this.vendorRequest,
     currentStatus: 'Approve',
     userId: this.selectedUserId
   }
   delete dto.id;
   this.VendorsApiService.create_Vendors(dto)
   .subscribe(res => {
     this.slectedVendor = res;
     this.selectedVendorId = res.id;
     this.approveBecomeAVendor();
   })
 }

 approveBecomeAVendor(){
   // need to check
   const dto = {
     id: this.vendorRequest.id,
     currentStatus: 'Approved',
     vendorsId: this.selectedVendorId,
     userId: this.selectedUserId
   }
   this.becomeAVendorApiService.update_become_a_vendor(dto)
   .subscribe(res => {
    this.notificationService.get_Notification('success','Vendor has been approved');
    this.method_for_getting_list();
   })
 }

}
