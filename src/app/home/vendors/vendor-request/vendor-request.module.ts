import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { BreadcrumbModule } from '../../../commonModule/breadcrumb/breadcrumb.module';
import { MaterialModule } from '../../../material.module';

import { VendorRequestComponent } from './vendor-request.component';
import { VendorRequestListModule } from './vendor-request-list/vendor-request-list.module';


const routes: Routes = [
  {
    path: '',
    component: VendorRequestComponent
  }
]

@NgModule({
  declarations: [
    VendorRequestComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    BreadcrumbModule,
    VendorRequestListModule,
    RouterModule.forChild(routes)
  ]
})
export class VendorRequestModule { }
