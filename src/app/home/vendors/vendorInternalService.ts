import { Injectable } from "@angular/core";
import { UserApiService } from "../../services/api/userApi.service";
import { VendorsApiService } from "../../services/api/vendorsApi.service";
import { PaginationService } from "../../services/commonServices/pagination.service";
import * as moment from 'moment-timezone';


@Injectable({
    providedIn: 'root'
})


export class VendorInternalService {


    public filter:any = {
        search: null,
        skip: 0,
        page_limit: 10
    }

    public vendorCount = 0;
    public vendorList = [];


    public totalCustomer = 0;
    public totalActiveCustomer = 0;
    public totalNewCustomer = 0;

    constructor (private vendorsApiService: VendorsApiService, private userApiService: UserApiService, 
        private paginationService: PaginationService ) {

    }


    method_for_making_dto_filter () {
        console.log(this.filter, 'filter');
        // this.loginUserData = this.cookiesService.get_user_login_data(true);
        this.method_for_getting_count(this.filter);
    }



    method_for_getting_count (filter) {
        var query:any = {
            isActive: true,
            role: 'Vendor'
        }

        if ( this.filter['search'] ) {
            query = {
                and: [{
                    isActive: true,
                    role: 'Vendor'
                  },
                  {
                    or: [
                        { firstName: { like: '%' + this.filter['search'] + '%' }},
                        { middleName: { like: '%' + this.filter['search'] + '%' }},
                        { lastName: { like: '%' + this.filter['search'] + '%' }},            
                        { email: { like: '%' + this.filter['search'] + '%' }},
                        { phoneNo: { like: '%' + this.filter['search'] + '%' }},
                      ]
                }]
            };
        }
        query = encodeURI(JSON.stringify(query));

        this.userApiService.get_userCount(query).subscribe(
            data => {
                this.vendorCount = data['count'];
                this.paginationService.set_count_data(data['count'], filter['skip']);
                this.method_for_getting_list_of_Vendor(filter);
            }, error => {

            }
        )
    }


    method_for_getting_list_of_Vendor (filter) { 
        var query:any = {
            where: {
                isActive: true,
                role: 'Vendor'
            },
            include: [
                { relation: "vendors" }
            ]
        };

        if ( this.filter['search'] ) {
            query['where'] = {
                and: [{
                    isActive: true,
                    role: 'Vendor'
                  },
                  {
                    or: [
                        { firstName: { like: '%' + this.filter['search'] + '%' }},
                        { middleName: { like: '%' + this.filter['search'] + '%' }},
                        { lastName: { like: '%' + this.filter['search'] + '%' }},            
                        { email: { like: '%' + this.filter['search'] + '%' }},
                        { phoneNo: { like: '%' + this.filter['search'] + '%' }},
                      ]
                }]
            }
        }

        query['order'] = "createdAt DESC";
        query['limit'] = this.paginationService.selected_drop_down_data;
        query['skip'] = this.paginationService.skip_data;

        query = encodeURI(JSON.stringify(query));

        this.userApiService.get_user(query).subscribe(
            data => {
                this.vendorList = data;
            }, error => {

            }
        )
    }

    method_for_getting_data () {
        this.method_for_total_customer ();
        this.method_for_active_customer ();
        this.method_for_total_new_customer ();
    }


    method_for_total_customer () {
        var query:any = { isActive: true, role: 'Vendor' };
        query = encodeURI(JSON.stringify(query));
        this.userApiService.get_userCount(query).subscribe(
          data => {
              this.totalCustomer = data['count'];
          }, error => {
  
          }
        )
      }
  
      method_for_active_customer () {
        var query:any = { isActive: true, role: 'Vendor' };
        query = encodeURI(JSON.stringify(query));
        this.userApiService.get_userCount(query).subscribe(
          data => {
              this.totalActiveCustomer = data['count'];
          }, error => {
  
          }
        )
      }
  
      method_for_total_new_customer () {
        let _dayStart = moment(moment().subtract(2, 'days')).startOf('day').format('x');
        let _dayEnd = moment(new Date()).format('x');
        var query:any = { isActive: true, role: 'Vendor', createdAt: { between: [_dayStart, _dayEnd] }};
        query = encodeURI(JSON.stringify(query));
        this.userApiService.get_userCount(query).subscribe(
          data => {
              this.totalNewCustomer = data['count'];
          }, error => {
  
          }
        )
      }



}