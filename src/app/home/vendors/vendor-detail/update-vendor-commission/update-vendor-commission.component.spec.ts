import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateVendorCommissionComponent } from './update-vendor-commission.component';

describe('UpdateVendorCommissionComponent', () => {
  let component: UpdateVendorCommissionComponent;
  let fixture: ComponentFixture<UpdateVendorCommissionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateVendorCommissionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateVendorCommissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
