import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { VendorsCommisionApiService } from '../../../../services/api/vendorsCommisionApi.service';
import { NotificationService } from '../../../../services/commonServices/notification.service';

@Component({
  selector: 'NA-update-vendor-commission',
  templateUrl: './update-vendor-commission.component.html',
  styleUrls: ['./update-vendor-commission.component.scss']
})
export class UpdateVendorCommissionComponent {
  commisionForm: FormGroup;
  updateDisabled: boolean;

  constructor(private matDialgoRef: MatDialogRef<UpdateVendorCommissionComponent>,
    @Inject(MAT_DIALOG_DATA) private data,
    private notificationService: NotificationService,
    private vendorCommisionApi:VendorsCommisionApiService,
    private fb: FormBuilder) {
    console.log(data)
    this.initVendorCommisonForm();
    this.getVendorCommison();
  }

  initVendorCommisonForm(){
    this.commisionForm = this.fb.group(
      {
        "id": [],
        "transactionCharge": ['', Validators.required],
        "commissionRate": ['', Validators.required],
        "enableCommission": [false, Validators.required],
        "vendorsId": [this.data.vendorId, Validators.required]
      }
    )
  }

  updateCommission(){
    this.updateDisabled = true;
    if(this.commisionForm.invalid){
      this.updateDisabled = false;
      return false;
    }
    const dto = this.commisionForm.value;
    console.log(dto);
    const API = dto.id ? this.vendorCommisionApi.update_vendor_commision : this.vendorCommisionApi.create_vendor_commision;
    API.bind(this.vendorCommisionApi)(dto).subscribe(res => {
      this.matDialgoRef.close();
      this.notificationService.get_Notification('success', "Vendor Commission has been set");
    }, err=> {
      this.updateDisabled = false;
    })
  }

  getVendorCommison(){
    const dto = {
      where:{
        vendorsId: this.data.vendorId
      }
    }
    this.vendorCommisionApi.get_vendor_commision(JSON.stringify(dto)).subscribe(res => {
      const commission = res[0];
      if(commission){
        this.commisionForm.patchValue(commission);
      }
    })
  }
}
