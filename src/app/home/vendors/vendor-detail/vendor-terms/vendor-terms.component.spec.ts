import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VendorTermsComponent } from './vendor-terms.component';

describe('VendorTermsComponent', () => {
  let component: VendorTermsComponent;
  let fixture: ComponentFixture<VendorTermsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VendorTermsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VendorTermsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
