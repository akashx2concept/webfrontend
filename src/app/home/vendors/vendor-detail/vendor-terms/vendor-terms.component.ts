import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { VendorsApiService } from '../../../../services/api/vendorsApi.service';
import { NotificationService } from '../../../../services/commonServices/notification.service';

@Component({
  selector: 'NA-vendor-terms',
  templateUrl: './vendor-terms.component.html',
  styleUrls: ['./vendor-terms.component.scss']
})
export class VendorTermsComponent implements OnInit {
  termaAndPolicyFrom:FormGroup;
  constructor(private fb:FormBuilder,
     private router:ActivatedRoute,
     private route:Router,
     private vendorsApiService: VendorsApiService,
     private notificationService: NotificationService) {
    this.initForm();
   }

  ngOnInit(): void {
    this.router.params.subscribe(res=> {
        if(res.id){
          this.getVendorDetail(res.id);
        }
    })
  }

  initForm(){
    this.termaAndPolicyFrom = this.fb.group({
      id: [],
      deliveryPolicy: [],
      collectionPolicy: [],
      cancellationPolicy: []
    })
  }

  getVendorDetail(id){
      const dto = {
        where: {
          id
        }
      }
      this.vendorsApiService.Vendors_findOne(JSON.stringify(dto))
      .subscribe(res => {
        this.termaAndPolicyFrom.patchValue(res);
      })
  }
  saveTermAndPolicy(){
      console.log(this.termaAndPolicyFrom.value);
      this.vendorsApiService.update_Vendors(this.termaAndPolicyFrom.value)
      .subscribe(res => {
        this.route.navigate(['../../'], {relativeTo: this.router});
        this.notificationService.notifier('success', 'Policy has been saved');
      },err =>{
        this.notificationService.notifier('error', 'Unable to save policy. Please Try later');
        this.route.navigate(['../../'], {relativeTo: this.router});
      })
  }

}
