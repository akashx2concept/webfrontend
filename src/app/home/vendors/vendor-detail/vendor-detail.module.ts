import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VendorDetailComponent } from './vendor-detail.component';
import { RouterModule, Routes } from '@angular/router';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { MaterialModule } from '../../../material.module';
import { VenderStripeConnectComponent } from '../../../commonModule/venderStripeConnect/venderStripeConnect.component';
import { ConfirmActionModule } from '../../../commonModule/confirm-action/confirm-action.module';
import { BreadcrumbModule } from '../../../commonModule/breadcrumb/breadcrumb.module';
import { UpdateVendorCommissionComponent } from './update-vendor-commission/update-vendor-commission.component';
import { ReactiveFormsModule } from '@angular/forms';
import { VendorTermsComponent } from './vendor-terms/vendor-terms.component';

const routes:Routes = [
  {path: '' , component:VendorDetailComponent},
  {path: 'term/:id' , component:VendorTermsComponent}
]



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TabsModule.forRoot(),
    ReactiveFormsModule,
    MaterialModule,
    BreadcrumbModule,
    ConfirmActionModule
  ],
  declarations: [
    VendorDetailComponent,
    VenderStripeConnectComponent,
    UpdateVendorCommissionComponent,
    VendorTermsComponent
  ],
  entryComponents:[UpdateVendorCommissionComponent],
  exports: [
    VendorDetailComponent,
    VenderStripeConnectComponent
  ]
})
export class VendorDetailModule { }
