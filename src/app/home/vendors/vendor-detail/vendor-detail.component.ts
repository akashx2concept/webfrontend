import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ConfirmActionComponent } from '../../../commonModule/confirm-action/confirm-action.component';
import { ImageCropperComponent } from '../../../commonModule/imageCropper/imageCropper.component';
import { OrderApiService } from '../../../services/api/orderApi.service';
import { ProductApiService } from '../../../services/api/productApi.service';
import { ProductImageApiService } from '../../../services/api/productImageApi.service';
import { UserApiService } from '../../../services/api/userApi.service';
import { VendorImageApiService } from '../../../services/api/vendorImageApi.service';
import { VendorsApiService } from '../../../services/api/vendorsApi.service';
import { CookiesService } from '../../../services/commonServices/cookies.service';
import { NeighborhoodsApiService } from '../../../services/neighborhoodsApi.service';
import { UpdateVendorCommissionComponent } from './update-vendor-commission/update-vendor-commission.component';



@Component({
  selector: 'NA-vendor-detail',
  templateUrl: './vendor-detail.component.html',
  styleUrls: ['./vendor-detail.component.scss']
})



export class VendorDetailComponent implements OnInit {



  public updateId;

  public userDetails;
  public vendorsDetails;
  public vendorTiming = [];

  private confirmRef: BsModalRef;

  public loginUserData;
  public loginVendorData;

  public userRole;

  public totalSales = 0;
  public totalApplicationFees = 0;
  public vendorIncome = 0;
  public totalProductCount = 0;
  public totalOrder = 0;

  constructor (private router: Router, private activatedRoute: ActivatedRoute,
    private userApiService: UserApiService, private modalService: BsModalService,
    private productImageApiService: ProductImageApiService, private vendorImageApiService: VendorImageApiService,
    private vendorsApiService: VendorsApiService, public neighborhoodsApiService: NeighborhoodsApiService,
    private cookiesService: CookiesService, private productApiService: ProductApiService,
    private dialog: MatDialog, private orderApiService: OrderApiService ) {

      this.updateId = this.activatedRoute.snapshot.paramMap.get('id');
      this.loginUserData = this.cookiesService.get_user_login_data();

      if (!this.updateId) {
        this.loginUserData = this.cookiesService.get_user_login_data();
        this.updateId = this.loginUserData['id'];
        this.loginVendorData = this.cookiesService.get_current_vendor_details();
      }

      this.userRole = this.loginUserData['role'];

  }


  ngOnInit(): void {
    this.method_for_getting_vender_details();
  }


  method_for_getting_vender_details() {
    let query = {
      where: {
        id: this.updateId
      },
      include: [
        {
          relation: 'vendors',
          scope: {

            include: [
              { relation: 'country' },
              { relation: 'state' },
              { relation: 'city' },
              { relation: 'user' },
              { relation: 'categories' },
              { relation: 'vendorTimings' },
              { relation: 'bannerImage',  scope: { include: [ { relation: "bannerImage" } ], order: "id Desc"}  },
              { relation: 'logoImage' }
            ]
          }
        }
      ]
    }

    if (this.loginVendorData && this.loginVendorData['id']) {
      query['include'][0]['scope']['where'] = {
        id: this.loginVendorData['id']
      };
    }

    this.userApiService.get_user(JSON.stringify(query)).subscribe(
      data => {
        console.log(data, '***');
        if (data.length) {
          this.userDetails = data[0];
          if (this.userDetails['vendors'].length) {
            this.vendorsDetails = this.userDetails['vendors'][0];
            if (this.vendorsDetails['vendorTimings'].length) {
              this.vendorTiming = this.vendorsDetails['vendorTimings'];
            }
            this.method_for_getting_analysis ();
          }
        }
      }, error => {

      }
    )


  }






  method_for_conect_stripe () {

  }


  updateStore(){
    if (this.userRole == 'Root') {
      if (this.vendorsDetails?.id) {
        this.router.navigate(['/home/vendor/addVendors', this.userDetails.id, this.vendorsDetails.id]);
      } else {
          this.router.navigate(['/home/vendor/addVendors', this.userDetails.id]);
      }
    } else if (this.userRole == 'Vendor') {
      if (this.vendorsDetails?.id) {
        this.router.navigate(['/vendor/addVendors', this.userDetails.id, this.vendorsDetails.id]);
      } else {
          this.router.navigate(['/vendor/addVendors', this.userDetails.id]);
      }
    }

  }

  uploadVendorBanner(){
      let initialState: any;
      initialState = { parentObj: {  } };
      this.confirmRef = this.modalService.show(ImageCropperComponent, {class: 'modal-dialog-centered modal-lg', initialState});
      this.confirmRef.content.action.subscribe((value) => {
          if (value){
            console.log(value);
            if (value.length) {
              // this.method_for_getting_addressList();
              this.method_for_product_Image(value[0]['id']);
            }
          } else {

          }
      });
  }

  uploadProfilePic(){
    let initialState: any;
      initialState = { parentObj: {  } };
      this.confirmRef = this.modalService.show(ImageCropperComponent, {class: 'modal-dialog-centered modal-lg', initialState});
      this.confirmRef.content.action.subscribe((value) => {
          if (value){
            console.log(value);
            if (value.length) {
              // this.method_for_getting_addressList();
              this.method_for_updateVendor(value[0]['id']);
            }
          } else {

          }
      });
  }


  method_for_updateVendor (imageId) {
    let dataDto = {
      id: this.vendorsDetails['id'],
      logoImageId: imageId
    }
    this.vendorsApiService.update_Vendors(dataDto).subscribe(
      data => {
        this.method_for_getting_vender_details();
      }, error => {

      }
    )
  }


  method_for_product_Image (imageId) {
    let data = {
      vendorsId: this.vendorsDetails['id'],
      bannerImageId: imageId,
    }
    this.vendorImageApiService.create_vendorImages(data).subscribe(
      data => {
        this.method_for_getting_vender_details();
      }, error => {

      }
    )
  }

  method_for_update_vendor () {

  }

  redirection(type){
    if(type === 'product'){
      this.router.navigate(['/home/product', this.vendorsDetails.id])
    }
    if(type === 'order'){
      this.router.navigate(['/home/order/manage-order'],{
        queryParams:{
          vendor: this.vendorsDetails.id
        }
      })
    }
  }


  deactivateStore(){
    const dto = {
      id: this.vendorsDetails.id,
      isActive: false,
      isDeleted: true
    }
    console.log(dto);
      this.dialog.open(ConfirmActionComponent, {
        width: '400px',
        data: {
          message: 'Would you like to deactivate this store?',
          title: 'Deactivate Store'
        }
      })
      .afterClosed()
      .subscribe(res => {
          if(res){
            this.vendorsApiService.update_Vendors(dto).subscribe(res => {
              this.method_for_getting_vender_details();
              window.scrollTo(0,0);
            })
          }
      })
  }

  activateStore(){
    const dto = {
      id: this.vendorsDetails.id,
      isActive: true,
      isDeleted: false
    }
    this.vendorsApiService.update_Vendors(dto).subscribe(res => {
      this.method_for_getting_vender_details();
    })
  }

  updateVendorCommision(){
    this.dialog.open(UpdateVendorCommissionComponent, {
        width: '320px',
        data: {
          vendorId: this.vendorsDetails.id
        }
    });
  }

  method_for_getting_analysis () {
    var query = {
      vendorId: this.vendorsDetails['id']
    };
    this.method_for_getting_product_count();
    this.method_for_getting_total_order_count();
    this.orderApiService.orderSalesFessAnalysis(JSON.stringify(query)).subscribe(
      data => {
        this.totalSales = data['grandTotal'];
        this.totalApplicationFees = data['totalApplicationFees'];
        this.vendorIncome = data['vendorIncome'];
      }, error => {

      }
    )
  }

  method_for_getting_product_count () {
    var query = {
      vendorsId: this.vendorsDetails['id'],
      isActive: true,
      isDeleted: false
    };
    this.productApiService.get_Products_count(JSON.stringify(query)).subscribe(
      data => {
        this.totalProductCount = data['count'];
      }, error => {

      }
    )
  }

  method_for_getting_total_order_count () {
    var query = {
      vendorsId: this.vendorsDetails['id'],
      status: 'OrderPlace'
    };
    this.orderApiService.get_Orders_count(JSON.stringify(query)).subscribe(
      data => {
        this.totalOrder = data['count'];
      }, error => {

      }
    )
  }

  method_for_redirect(){
    window.history.back();
  }
}
