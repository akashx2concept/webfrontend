import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { CookiesService } from "../../services/commonServices/cookies.service";




@Component({
    selector: 'NA-profile',
    templateUrl: './profile.component.html',
    styleUrls: [ './profile.component.scss' ]
})


export class ProfileComponent implements OnInit {


    public loginUserData;


    constructor (private cookiesService: CookiesService, private router: Router) {

        this.loginUserData = this.cookiesService.get_user_login_data();
    }

    ngOnInit () {

    }


    method_for_redirect (value) {
        this.router.navigate([`home/profile/${value}`])
    }
    
}