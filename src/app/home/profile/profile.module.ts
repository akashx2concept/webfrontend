import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuardService } from "../../services/commonServices/auth-guard/auth-guard.service";
import { ProfileComponent } from "./profile.component";


const routes: Routes = [
    {
        path: '',
        component: ProfileComponent,
        children: [
            {
                path: 'address',
                loadChildren: () => import('./address/address.module').then(m => m.AddressModule),
                canActivate: [AuthGuardService],
            }
        ]
    }
]


@NgModule ({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        ProfileComponent
    ],
    exports: [
        ProfileComponent
    ]
})

export class ProfileModule {

}