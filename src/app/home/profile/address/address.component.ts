import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { AddAddressComponent } from "../../../commonModule/addAddress/addAddress.component";
import { CookiesService } from "../../../services/commonServices/cookies.service";
import { AddressApiService } from "../../../services/api/addressApi";



@Component ({
    selector: 'NA-address',
    templateUrl: './address.component.html',
    styleUrls: [ './address.component.scss' ]
})


export class AddressComponent implements OnInit {

    public updateDetailsForm:FormGroup; 

    public countryList = [];
    public stateList = [];
    public cityList = [];

    public selectedCountry;
    public selectedState;
    public selectedCity;

    private confirmRef: BsModalRef;

    public loginUserData;

    public addressList = [];

    constructor (private formBuilder: FormBuilder, private modalService: BsModalService,
        private cookiesService: CookiesService, private addressApiService: AddressApiService ) {

        this.loginUserData = this.cookiesService.get_user_login_data();
    }

    ngOnInit () {
        this.method_for_getting_addressList();
    }   


    method_for_add_address () {
        // let initialState: any; 
        // let text = `Are you sure you want to delete from the list?`;
        // initialState = { parentObj: { userId: this.loginUserData['id'] } };
        // this.confirmRef = this.modalService.show(AddAddressComponent, {class: 'modal-dialog-centered modal-md', initialState});
        // this.confirmRef.content.action.subscribe((value) => {
        //     if (value){
        //         this.method_for_getting_addressList();
        //     } else {

        //     }
        // });
    }



    method_for_getting_addressList () {
        let query = {
            where: {
                // userId: this.loginUserData['id']
                userId: 1
            },
            include: [
                { relation: 'country' },
                { relation: 'state' },
                { relation: 'city' },
            ]
        }

        this.addressApiService.get_Addresses(JSON.stringify(query)).subscribe(
            data => {
                this.addressList = data;
            }, error => {

            }
        )
    }


   

}