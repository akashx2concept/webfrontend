import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { MaterialModule } from "../../../material.module";
import { AddressComponent } from "./address.component";



const routes: Routes = [
    {
        path: '',
        component: AddressComponent
    }
]


@NgModule({
    imports: [
        CommonModule,
        FormsModule, ReactiveFormsModule,
        MaterialModule,
        RouterModule.forChild(routes),
        
    ],
    declarations: [
        AddressComponent
    ],
    exports: [
        AddressComponent
    ]
})

export class AddressModule {

}