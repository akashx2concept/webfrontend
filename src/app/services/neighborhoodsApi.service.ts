import { Injectable } from "@angular/core";



@Injectable ({
    providedIn: 'root'
})


export class NeighborhoodsApiService {


    public config = {
        // baseUrl: "http://localhost:3000/",
        baseUrl: "http://54.237.210.1:3000/",
        // baseUrl: "https://www.test.conceptx.fi/",
        // baseUrl: "https://neighbourgoods.fi/",

        //  baseUrl: "http://52.0.131.57:3000/",

        // imageBaseUrl: 'http://localhost:3000',
        imageBaseUrl: "http://54.237.210.1:3000",
        // imageBaseUrl: "https://www.test.conceptx.fi",
        // imageBaseUrl: "https://neighbourgoods.fi",

        // imageBaseUrl: "http://52.0.131.57:3000",
    }

    constructor () {

    }


    // User APi
    Users = this.config.baseUrl + 'api/users';
    userCount = this.config.baseUrl + 'api/users/count';
    users_login = this.config.baseUrl + 'api/users/login';
    users_verify = this.config.baseUrl + 'api/users/emailVerification';
    user_change_password = this.config.baseUrl + 'api/users/change-password';

    users_findOne = this.config.baseUrl + 'api/users/findOne';
    user_logout = this.config.baseUrl + 'api/users/logout'
    uniqueEmail = this.config.baseUrl + 'api/users/searchEmailAddress';
    sendVendorApproveMail = this.config.baseUrl + 'api/users/sendVendorApproveMail';

    //Forgot Password
    sendForgetPasswordMail = this.config.baseUrl + 'api/users/sendForgetPasswordMail';
    verifyResetPasswordToken = this.config.baseUrl + 'api/users/verifyResetPasswordToken';
    forgotPasswordChange = this.config.baseUrl + 'api/users/forgotPasswordChange';

    VendorProductDashboardGraph = this.config.baseUrl + 'api/users/vendorProductDashboardGraph';
    orderSalesDashboardGraph = this.config.baseUrl + 'api/users/orderSalesDashboardGraph';


    //country
    countries = this.config.baseUrl + 'api/countries';

    // state
    states = this.config.baseUrl + 'api/states';

    //city
    cities = this.config.baseUrl + 'api/cities';



    // Address
    Addresses = this.config.baseUrl + 'api/Addresses';


    // Categories
    Categories = this.config.baseUrl + 'api/Categories';
    Categories_count = this.config.baseUrl + 'api/Categories/count';
    CategoriesRequests = this.config.baseUrl + 'api/CategorieRequests';


    // Cart Item
    CartItems = this.config.baseUrl + 'api/CartItems';


    //Vendors
    Vendors = this.config.baseUrl + 'api/Vendors';
    Vendors_findOne = this.config.baseUrl + 'api/Vendors/findOne';
    Vendors_count = this.config.baseUrl + 'api/Vendors/count';

    Vendors_FavoriteVendor = this.config.baseUrl + 'api/FavoriteVendors';

    //Vendor Timings
    VendorTimings = this.config.baseUrl + 'api/VendorTimings';

    //Vendors Commission
    VendorCommison = this.config.baseUrl + 'api/VendorCommissionRates';

    // Become A Vendor
    BecomeAVendor = this.config.baseUrl + 'api/BecomeVendors';

    //Setting
    AdminSetting = this.config.baseUrl + 'api/AdminSettings';
    AdminSetting_termAndCondition = this.config.baseUrl + 'api/AdminSettings/termAndCondition';
    AdminSetting_privacyPolicy = this.config.baseUrl + 'api/AdminSettings/privacyPolicy';

    //Product
    Products = this.config.baseUrl + 'api/Products';
    Products_count = this.config.baseUrl + 'api/Products/count';
    Products_findOne = this.config.baseUrl + 'api/Products/findOne';

    Products_ProductPrice = this.config.baseUrl + 'api/ProductPrices';

    // product image
    productImage = this.config.baseUrl + 'api/ProductImages';

    // vendor image
    vendorImages = this.config.baseUrl + 'api/vendorImages';

    // Image Upload
    StorageFiles_upload = this.config.baseUrl + 'api/StorageFiles/upload';

    // Vender Stripe
    StripePayments = this.config.baseUrl + 'api/StripePayments';
    StripePayments_onboardVendor = this.config.baseUrl + 'api/StripePayments/onboardVendor';
    StripePayments_onboardVendorRefresh = this.config.baseUrl + 'api/StripePayments/onboardVendorRefresh';
    StripePayments_customerPlaceOrder = this.config.baseUrl + 'api/StripePayments/customerPlaceOrder';
    StripePayments_getConnectedAccountData = this.config.baseUrl + 'api/StripePayments/getConnectedAccountData'
    StripePayments_getConnectedAccount_loginLink = this.config.baseUrl + 'api/StripePayments/getConnectedAccount_loginLink';
    StripePayments_getStripeCustomerCard = this.config.baseUrl + 'api/StripePayments/getStripeCustomerCard';



    PaymentDetails = this.config.baseUrl + 'api/PaymentDetails';
    PaymentDetails_count = this.config.baseUrl + 'api/PaymentDetails/count';

    // Order
    Orders = this.config.baseUrl + 'api/Orders';
    Orders_count = this.config.baseUrl + 'api/Orders/count';
    Orders_paymentStatus = this.config.baseUrl + 'api/Orders/paymentStatus';

    orderSalesFessAnalysis = this.config.baseUrl + 'api/Orders/orderSalesFessAnalysis';
    orderStatus = this.config.baseUrl + 'api/Orders/orderStatus';
    viewOrderBill = this.config.baseUrl + 'api/Orders/viewOrderBill';

    OrderItems = this.config.baseUrl + 'api/OrderItems';

    //Emailsubscription
    emailSubscribe = this.config.baseUrl + 'api/EmailSubscribes';

    contactUs = this.config.baseUrl + 'api/ContactUs';
}
