import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClientWitToken } from "../commonServices/custom-http-client.service";
import { NeighborhoodsApiService } from "../neighborhoodsApi.service";



@Injectable({
    providedIn: 'root'
})


export class OrderApiService {


    constructor (private httpWithToken: HttpClientWitToken, private neighborhoodsApiService: NeighborhoodsApiService,
        private httpClient: HttpClient) {

    }


    get_Orders (query):Observable <any> {
        return this.httpClient.get<any>(`${this.neighborhoodsApiService.Orders}?filter=${query}`, this.httpWithToken.get_autorization_header());
    }

    get_Orders_count (query): Observable<any> {
        return this.httpClient.get<any>(`${this.neighborhoodsApiService.Orders_count}?where=${query}`, this.httpWithToken.get_autorization_header());
    }


    orderSalesFessAnalysis (dataDto): Observable<any> {
        return this.httpClient.post<any>(`${this.neighborhoodsApiService.orderSalesFessAnalysis}`, dataDto, this.httpWithToken.get_autorization_header());
    }

    update_order_status(dto):Observable<any>{
      return this.httpClient.patch<any>(`${this.neighborhoodsApiService.Orders}`, dto, this.httpWithToken.get_autorization_header());
    }

    get_OrderItems (query): Observable<any> {
        return this.httpClient.get<any>(`${this.neighborhoodsApiService.OrderItems}/?filter=${query}`, this.httpWithToken.get_autorization_header());
    }

    orderStatus (dataDto): Observable<any> {
        return this.httpClient.post<any>(`${this.neighborhoodsApiService.orderStatus}`, dataDto, this.httpWithToken.get_autorization_header());
    }


    viewOrderBill (dataDto): Observable<any> {
        return this.httpClient.post<any>(`${this.neighborhoodsApiService.viewOrderBill}`, dataDto, this.httpWithToken.get_autorization_header());
    }

}
