import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClientWitToken } from "../commonServices/custom-http-client.service";
import { NeighborhoodsApiService } from "../neighborhoodsApi.service";




@Injectable({
    providedIn: 'root'
})

export class StateApiService {




    constructor (private httpWithToken: HttpClientWitToken, private neighborhoodsApiService: NeighborhoodsApiService,
        private httpClient: HttpClient) {

    }

    get_states (query): Observable<any> {
        return this.httpClient.get<any> (`${this.neighborhoodsApiService.states}?filter=${query}`, this.httpWithToken.get_autorization_header());
    }


}