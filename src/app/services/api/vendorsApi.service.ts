import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClientWitToken } from "../commonServices/custom-http-client.service";
import { NeighborhoodsApiService } from "../neighborhoodsApi.service";




@Injectable ({
    providedIn: 'root'
})


export class VendorsApiService {



    constructor (private httpWithToken: HttpClientWitToken, private neighborhoodsApiService: NeighborhoodsApiService,
        private httpClient: HttpClient) {

    }


    create_Vendors (dataDto): Observable<any> {
        return this.httpClient.post<any>(`${this.neighborhoodsApiService.Vendors}`, dataDto, this.httpWithToken.get_autorization_header());
    }

    update_Vendors (dataDto): Observable<any> {
        return this.httpClient.patch<any>(`${this.neighborhoodsApiService.Vendors}`, dataDto, this.httpWithToken.get_autorization_header());
    }

    get_Vendors (query): Observable<any> {
        return this.httpClient.get<any>(`${this.neighborhoodsApiService.Vendors}?filter=${query}`, this.httpWithToken.get_autorization_header());
    }

    Vendors_findOne (query): Observable<any> {
        return this.httpClient.get<any>(`${this.neighborhoodsApiService.Vendors_findOne}?filter=${query}`, this.httpWithToken.get_autorization_header());
    }


    Vendors_count (query): Observable<any> {
        return this.httpClient.get<any>(`${this.neighborhoodsApiService.Vendors_count}?where=${query}`, this.httpWithToken.get_autorization_header());
    }


}
