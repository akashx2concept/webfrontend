import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClientWitToken } from "../commonServices/custom-http-client.service";
import { NeighborhoodsApiService } from "../neighborhoodsApi.service";




@Injectable({
    providedIn: 'root'
})


export class CategoriesApiService {



    constructor (private httpWithToken: HttpClientWitToken, private neighborhoodsApiService: NeighborhoodsApiService,
        private httpClient: HttpClient) {

    }


    get_Categories (query): Observable<any> {
        return this.httpClient.get<any>(`${this.neighborhoodsApiService.Categories}?filter=${query}`, this.httpWithToken.get_autorization_header());
    }

    get_Categories_count (query): Observable<any> {
        return this.httpClient.get<any>(`${this.neighborhoodsApiService.Categories_count}?where=${query}`, this.httpWithToken.get_autorization_header());
    }

    create_Categories (dataDto): Observable<any> {
        return this.httpClient.post<any>(`${this.neighborhoodsApiService.Categories}`, dataDto, this.httpWithToken.get_autorization_header());
    }

    patch_Categories (dataDto): Observable<any> {
      return this.httpClient.patch<any>(`${this.neighborhoodsApiService.Categories}`, dataDto, this.httpWithToken.get_autorization_header());
   }
}
