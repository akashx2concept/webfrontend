import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClientWitToken } from "../commonServices/custom-http-client.service";
import { NeighborhoodsApiService } from "../neighborhoodsApi.service";


@Injectable({
    providedIn: 'root'
})


export class ImageAPIService {


    constructor (private httpWithToken: HttpClientWitToken, private neighborhoodsApiService: NeighborhoodsApiService,
        private httpClient: HttpClient) {

    }


    upload_image (formData):Observable<any> {
        return this.httpClient.post<any>(this.neighborhoodsApiService.StorageFiles_upload, formData, this.httpWithToken.get_autorization_header_for_image());
    }


}