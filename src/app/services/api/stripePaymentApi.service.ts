import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClientWitToken } from "../commonServices/custom-http-client.service";
import { NeighborhoodsApiService } from "../neighborhoodsApi.service";




@Injectable({
    providedIn: 'root'
})

export class StripePaymentApiService {



    constructor (private httpWithToken: HttpClientWitToken, private neighborhoodsApiService: NeighborhoodsApiService,
        private httpClient: HttpClient) {

    }


    StripePayments_onboardVendor (dataDto): Observable<any> {
        return this.httpClient.post<any>(`${this.neighborhoodsApiService.StripePayments_onboardVendor}`, dataDto, this.httpWithToken.get_autorization_header());
    } 

    StripePayments_onboardVendorRefresh (dataDto): Observable<any>{
        return this.httpClient.post<any>(`${this.neighborhoodsApiService.StripePayments_onboardVendorRefresh}`, dataDto, this.httpWithToken.get_autorization_header());
    }


    StripePayment (query): Observable<any> {
        return this.httpClient.get<any>(`${this.neighborhoodsApiService.StripePayments}?filter=${query}`, this.httpWithToken.get_autorization_header());
    }


    StripePayments_customerPlaceOrder (dataDto): Observable<any> {
        return this.httpClient.post<any>(`${this.neighborhoodsApiService.StripePayments_customerPlaceOrder}`, dataDto, this.httpWithToken.get_autorization_header());
    }

    StripePayments_getConnectedAccountData (dataDto): Observable<any> {
        return this.httpClient.post<any>(`${this.neighborhoodsApiService.StripePayments_getConnectedAccountData}`, dataDto, this.httpWithToken.get_autorization_header());
    }


    StripePayments_getConnectedAccount_loginLink (dataDto): Observable<any> {
        return this.httpClient.post<any>(`${this.neighborhoodsApiService.StripePayments_getConnectedAccount_loginLink}`, dataDto, this.httpWithToken.get_autorization_header());
    }

    Orders_paymentStatus (dataDto): Observable<any> {
        return this.httpClient.post<any>(`${this.neighborhoodsApiService.Orders_paymentStatus}`, dataDto, this.httpWithToken.get_autorization_header());
    }



    StripePayments_getStripeCustomerCard (dataDto): Observable<any> {
        return this.httpClient.post<any>(`${this.neighborhoodsApiService.StripePayments_getStripeCustomerCard}`, dataDto, this.httpWithToken.get_autorization_header());
    }
}