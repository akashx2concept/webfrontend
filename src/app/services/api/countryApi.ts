import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClientWitToken } from "../commonServices/custom-http-client.service";
import { NeighborhoodsApiService } from "../neighborhoodsApi.service";



@Injectable({
    providedIn: 'root'
})


export class CountryApiService {



    constructor (private httpWithToken: HttpClientWitToken, private neighborhoodsApiService: NeighborhoodsApiService,
        private httpClient: HttpClient) {

    }


    get_countries(query): Observable<any> {
        return this.httpClient.get<any>(`${this.neighborhoodsApiService.countries}?filter=${query}`, this.httpWithToken.get_autorization_header());
    }



}