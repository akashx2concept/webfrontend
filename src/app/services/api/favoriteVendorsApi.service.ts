import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClientWitToken } from "../commonServices/custom-http-client.service";
import { NeighborhoodsApiService } from "../neighborhoodsApi.service";



@Injectable ({
    providedIn: 'root'
})


export class FavoriteVendorsApiService {


    constructor (private htpWithToken: HttpClientWitToken, private neighborhoodsApiService: NeighborhoodsApiService,
        private httpClient: HttpClient) {

    }


    create_Vendors_FavoriteVendor (dataDto): Observable<any> {
        return this.httpClient.post<any>(`${this.neighborhoodsApiService.Vendors_FavoriteVendor}`, dataDto, this.htpWithToken.get_autorization_header());
    }


    delete_Vendors_FavoriteVendor (id):Observable<any> {
        return this.httpClient.delete<any>(`${this.neighborhoodsApiService.Vendors_FavoriteVendor}/${id}`, this.htpWithToken.get_autorization_header());
    }

    get_Vendors_FavoriteVendor (query): Observable<any> {
        return this.httpClient.get<any>(`${this.neighborhoodsApiService.Vendors_FavoriteVendor}?filter=${query}`, this.htpWithToken.get_autorization_header());
    }

}