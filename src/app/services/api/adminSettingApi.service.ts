import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClientWitToken } from "../commonServices/custom-http-client.service";
import { NeighborhoodsApiService } from "../neighborhoodsApi.service";




@Injectable ({
    providedIn: 'root'
})


export class AdminSettingAPIService {



    constructor (private httpWithToken: HttpClientWitToken, private neighborhoodsApiService: NeighborhoodsApiService,
        private httpClient: HttpClient) {

    }


    create_admin_setting (dataDto): Observable<any> {
        return this.httpClient.post<any>(`${this.neighborhoodsApiService.AdminSetting}`, dataDto, this.httpWithToken.get_autorization_header());
    }

    update_admin_setting (dataDto): Observable<any> {
        return this.httpClient.patch<any>(`${this.neighborhoodsApiService.AdminSetting}`, dataDto, this.httpWithToken.get_autorization_header());
    }

    get_admin_setting (query): Observable<any> {
        return this.httpClient.get<any>(`${this.neighborhoodsApiService.AdminSetting}?filter=${query}`, this.httpWithToken.get_autorization_header());
    }

    get_Adminsetting_termAndCondition ():Observable<any> {
        return this.httpClient.post<any>(`${this.neighborhoodsApiService.AdminSetting_termAndCondition}`, {});
    }

    get_Adminsetting_privacyPolicy ():Observable<any> {
        return this.httpClient.post<any>(`${this.neighborhoodsApiService.AdminSetting_privacyPolicy}`, {});
    }

}
