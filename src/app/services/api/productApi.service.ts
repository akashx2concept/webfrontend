import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClientWitToken } from "../commonServices/custom-http-client.service";
import { NeighborhoodsApiService } from "../neighborhoodsApi.service";




@Injectable ({
    providedIn: 'root'
})


export class ProductApiService {



    constructor (private httpWithToken: HttpClientWitToken, private neighborhoodsApiService: NeighborhoodsApiService,
        private httpClient: HttpClient) {

    }


    create_product (dataDto): Observable<any> {
        return this.httpClient.post<any>(`${this.neighborhoodsApiService.Products}`, dataDto, this.httpWithToken.get_autorization_header());
    }

    put_product (dataDto): Observable<any> {
      return this.httpClient.put<any>(`${this.neighborhoodsApiService.Products}`, dataDto, this.httpWithToken.get_autorization_header());
    }

    patch_product (dataDto): Observable<any> {
      return this.httpClient.patch<any>(`${this.neighborhoodsApiService.Products}`, dataDto, this.httpWithToken.get_autorization_header());
    }

    get_Products_count (query): Observable<any> {
        return this.httpClient.get<any>(`${this.neighborhoodsApiService.Products_count}?where=${query}`, this.httpWithToken.get_autorization_header());
    }

    get_Products (query): Observable<any> {
        return this.httpClient.get<any>(`${this.neighborhoodsApiService.Products}?filter=${query}`, this.httpWithToken.get_autorization_header());
    }

    Products_findOne (query): Observable<any> {
        return this.httpClient.get<any>(`${this.neighborhoodsApiService.Products_findOne}?where=${query}`, this.httpWithToken.get_autorization_header());
    }

    delete_product(id):Observable<any>{
        return this.httpClient.delete<any>(`${this.neighborhoodsApiService.Products}/${id}`,this.httpWithToken.get_autorization_header());
    }


    Products_ProductPrice (query):Observable<any> {
        return this.httpClient.get<any>(`${this.neighborhoodsApiService.Products_ProductPrice}?filter=${query}`, this.httpWithToken.get_autorization_header());
    }


   }
