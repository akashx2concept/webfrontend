import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClientWitToken } from "../commonServices/custom-http-client.service";
import { NeighborhoodsApiService } from "../neighborhoodsApi.service";


@Injectable ({
    providedIn: 'root'
})


export class CartItemApiService {


    constructor (private httpWithToken: HttpClientWitToken, private neighborhoodsApiService: NeighborhoodsApiService,
        private httpClient: HttpClient) {

    }

    get_CartItems (query): Observable<any> {
        return this.httpClient.get<any>(`${this.neighborhoodsApiService.CartItems}?filter=${query}`, this.httpWithToken.get_autorization_header());
    }


    delete_CartItems (id): Observable<any> {
        return this.httpClient.delete<any>(`${this.neighborhoodsApiService.CartItems}/${id}`, this.httpWithToken.get_autorization_header());
    }

    update_CartItems (dataDto): Observable<any> {
        return this.httpClient.patch<any>(`${this.neighborhoodsApiService.CartItems}`, dataDto, this.httpWithToken.get_autorization_header());
    }

    create_CartItems(dataDto): Observable<any> {
        return this.httpClient.post<any>(`${this.neighborhoodsApiService.CartItems}`, dataDto, this.httpWithToken.get_autorization_header());
    }
    

}