import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClientWitToken } from "../commonServices/custom-http-client.service";
import { NeighborhoodsApiService } from "../neighborhoodsApi.service";


@Injectable({
    providedIn: 'root'
})

export class AuthApiService {



    constructor (private httpWithToken: HttpClientWitToken, private neighborhoodsApiService: NeighborhoodsApiService,
        private httpClient: HttpClient) {

    }


    SignUp(dataDto):Observable<any> {
        return this.httpClient.post<any>(this.neighborhoodsApiService.Users, dataDto);
    }


    login(dataDto): Observable<any> {
        return this.httpClient.post<any>(this.neighborhoodsApiService.users_login, dataDto);
    }


    logout()  : Observable<any> {
      return this.httpClient.post<any>(`${this.neighborhoodsApiService.user_logout}`, {}, this.httpWithToken.get_autorization_header());
    }

}
