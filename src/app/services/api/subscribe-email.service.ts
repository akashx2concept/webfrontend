import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClientWitToken } from "../commonServices/custom-http-client.service";
import { NeighborhoodsApiService } from "../neighborhoodsApi.service";




@Injectable({
    providedIn: 'root'
})


export class SubscribeEmailService {

    emailPattern = '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';

    constructor (private httpWithToken: HttpClientWitToken, private neighborhoodsApiService: NeighborhoodsApiService,
        private httpClient: HttpClient) {

    }


    subscribe_email (dataDto): Observable<any> {
        return this.httpClient.post<any> (`${this.neighborhoodsApiService.emailSubscribe}`, dataDto);
    }

    get_subscrie_email(query){
      return this.httpClient.get<any>(`${this.neighborhoodsApiService.emailSubscribe}?where=${query}`, this.httpWithToken.get_autorization_header());
    }

    delete_subscrie_email(id){
      return this.httpClient.delete<any>(`${this.neighborhoodsApiService.emailSubscribe}/${id}`, this.httpWithToken.get_autorization_header());
    }

}
