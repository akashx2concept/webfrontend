import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClientWitToken } from "../commonServices/custom-http-client.service";
import { NeighborhoodsApiService } from "../neighborhoodsApi.service";




@Injectable({
    providedIn: 'root'
})


export class AddressApiService {



    constructor (private httpWithToken: HttpClientWitToken, private neighborhoodsApiService: NeighborhoodsApiService,
        private httpClient: HttpClient) {

    }


    get_Addresses (query): Observable<any> {
        return this.httpClient.get<any>(`${this.neighborhoodsApiService.Addresses}?filter=${query}`, this.httpWithToken.get_autorization_header());
    }

    create_Addresses (dataDto): Observable<any> {
        return this.httpClient.post<any>(`${this.neighborhoodsApiService.Addresses}`, dataDto, this.httpWithToken.get_autorization_header());
    }

    update_Addresses (dataDto): Observable<any> {
      return this.httpClient.put<any>(`${this.neighborhoodsApiService.Addresses}`, dataDto, this.httpWithToken.get_autorization_header());
    }





}
