import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClientWitToken } from "../commonServices/custom-http-client.service";
import { NeighborhoodsApiService } from "../neighborhoodsApi.service";



@Injectable({
    providedIn: 'root'
})

export class VendorImageApiService {



    constructor (private httpWithToken: HttpClientWitToken, private neighborhoodsApiService: NeighborhoodsApiService,
        private httpClient: HttpClient) {

    }

    create_vendorImages(dataDto): Observable<any> {
        return this.httpClient.post<any>(`${this.neighborhoodsApiService.vendorImages}`, dataDto, this.httpWithToken.get_autorization_header());
    }

}