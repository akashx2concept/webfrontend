import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClientWitToken } from "../commonServices/custom-http-client.service";
import { NeighborhoodsApiService } from "../neighborhoodsApi.service";


@Injectable({
    providedIn: 'root'
})


export class PaymentApiService {



    constructor (private httpWithToken: HttpClientWitToken, private neighborhoodsApiService: NeighborhoodsApiService,
        private httpClient: HttpClient) {

    }

    get_PaymentDetails (query): Observable<any> {
        return this.httpClient.get<any>(`${this.neighborhoodsApiService.PaymentDetails}?filter=${query}`, this.httpWithToken.get_autorization_header());
    }

    get_paymentCount (query): Observable<any> {
        return this.httpClient.get<any>(`${this.neighborhoodsApiService.PaymentDetails_count}?where=${query}`, this.httpWithToken.get_autorization_header());
    }



}