import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClientWitToken } from "../commonServices/custom-http-client.service";
import { NeighborhoodsApiService } from "../neighborhoodsApi.service";




@Injectable({
    providedIn: 'root'
})


export class ContactUsService {

    emailPattern = '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';

    constructor (private httpWithToken: HttpClientWitToken, private neighborhoodsApiService: NeighborhoodsApiService,
        private httpClient: HttpClient) {

    }


    create_contact_us (dataDto): Observable<any> {
        return this.httpClient.post<any> (`${this.neighborhoodsApiService.contactUs}`, dataDto);
    }

    get_contact_us(query){
      return this.httpClient.get<any>(`${this.neighborhoodsApiService.contactUs}?where=${query}`, this.httpWithToken.get_autorization_header());
    }

    delete_contact_us(id){
      return this.httpClient.delete<any>(`${this.neighborhoodsApiService.contactUs}/${id}`, this.httpWithToken.get_autorization_header());
    }

}
