import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClientWitToken } from "../commonServices/custom-http-client.service";
import { NeighborhoodsApiService } from "../neighborhoodsApi.service";




@Injectable ({
    providedIn: 'root'
})


export class VendorsCommisionApiService {



    constructor (private httpWithToken: HttpClientWitToken, private neighborhoodsApiService: NeighborhoodsApiService,
        private httpClient: HttpClient) {

    }


    create_vendor_commision (dataDto): Observable<any> {
        return this.httpClient.post<any>(`${this.neighborhoodsApiService.VendorCommison}`, dataDto, this.httpWithToken.get_autorization_header());
    }

    update_vendor_commision (dataDto): Observable<any> {
        return this.httpClient.patch<any>(`${this.neighborhoodsApiService.VendorCommison}`, dataDto, this.httpWithToken.get_autorization_header());
    }

    get_vendor_commision (query): Observable<any> {
        return this.httpClient.get<any>(`${this.neighborhoodsApiService.VendorCommison}?filter=${query}`, this.httpWithToken.get_autorization_header());
    }

}
