import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClientWitToken } from "../commonServices/custom-http-client.service";
import { NeighborhoodsApiService } from "../neighborhoodsApi.service";




@Injectable ({
    providedIn: 'root'
})


export class BecomeAVendorApiService {



    constructor (private httpWithToken: HttpClientWitToken, private neighborhoodsApiService: NeighborhoodsApiService,
        private httpClient: HttpClient) {

    }


    create_become_a_vendor (dataDto): Observable<any> {
        return this.httpClient.post<any>(`${this.neighborhoodsApiService.BecomeAVendor}`, dataDto);
    }

    update_become_a_vendor (dataDto): Observable<any> {
      return this.httpClient.patch<any>(`${this.neighborhoodsApiService.BecomeAVendor}`, dataDto, this.httpWithToken.get_autorization_header());
    }


    get_become_a_vendor (query): Observable<any> {
        return this.httpClient.get<any>(`${this.neighborhoodsApiService.BecomeAVendor}?filter=${query}`, this.httpWithToken.get_autorization_header());
    }



}
