import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClientWitToken } from "../commonServices/custom-http-client.service";
import { NeighborhoodsApiService } from '../neighborhoodsApi.service';


@Injectable({
    providedIn: 'root'
})



export class UserApiService {



    constructor (private httpWithToken: HttpClientWitToken, private neighborhoodsApiService: NeighborhoodsApiService,
        private httpClient: HttpClient) {

    }



    get_users_findOne (query): Observable<any> {
        return this.httpClient.get<any>(`${this.neighborhoodsApiService.users_findOne}?filter=${query}`, this.httpWithToken.get_autorization_header());
    }

    get_user (query): Observable<any> {
        return this.httpClient.get<any> (`${this.neighborhoodsApiService.Users}?filter=${query}`, this.httpWithToken.get_autorization_header());
    }

    get_userCount (query): Observable<any> {
        return this.httpClient.get<any> (`${this.neighborhoodsApiService.userCount}?where=${query}`, this.httpWithToken.get_autorization_header());
    }

    post_users (dataDto): Observable<any> {
        return this.httpClient.post<any>(`${this.neighborhoodsApiService.Users}`, dataDto, this.httpWithToken.get_autorization_header());
    }

    patch_users (dataDto): Observable<any> {
      return this.httpClient.patch<any>(`${this.neighborhoodsApiService.Users}`, dataDto, this.httpWithToken.get_autorization_header());
    }


    post_verify_user (dataDto): Observable<any> {
      return this.httpClient.post<any>(`${this.neighborhoodsApiService.users_verify}`, dataDto);
    }

    put_users (dataDto): Observable<any> {
      return this.httpClient.put<any>(`${this.neighborhoodsApiService.Users}`, dataDto, this.httpWithToken.get_autorization_header());
    }

    VendorProductDashboardGraph (dataDto): Observable<any> {
        return this.httpClient.post<any>(`${this.neighborhoodsApiService.VendorProductDashboardGraph}`, dataDto, this.httpWithToken.get_autorization_header());
    }

    orderSalesDashboardGraph (dataDto): Observable<any> {
      return this.httpClient.post<any>(`${this.neighborhoodsApiService.orderSalesDashboardGraph}`, dataDto, this.httpWithToken.get_autorization_header());
    }

    change_password(dto):Observable<any>{
      return this.httpClient.post<any>(`${this.neighborhoodsApiService.user_change_password}`, dto, this.httpWithToken.get_autorization_header());
    }

    sendVendorApproveMail (dataDto): Observable<any> {
      return this.httpClient.post<any>(`${this.neighborhoodsApiService.sendVendorApproveMail}`, dataDto, this.httpWithToken.get_autorization_header());
    }

    post_request_reset_password (dataDto): Observable<any> {
      return this.httpClient.post<any>(`${this.neighborhoodsApiService.sendForgetPasswordMail}`, dataDto);
    }
    post_verify_reset_password_token (dataDto): Observable<any> {
      return this.httpClient.post<any>(`${this.neighborhoodsApiService.verifyResetPasswordToken}`, dataDto);
    }

    post_reset_password(dataDto){
      return this.httpClient.post<any>(`${this.neighborhoodsApiService.forgotPasswordChange}`, dataDto);
    }
}
