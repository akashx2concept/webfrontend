import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClientWitToken } from "../commonServices/custom-http-client.service";
import { NeighborhoodsApiService } from "../neighborhoodsApi.service";




@Injectable({
    providedIn: 'root'
})


export class VendorsTimingApiService {



    constructor (private httpWithToken: HttpClientWitToken, private neighborhoodsApiService: NeighborhoodsApiService,
        private httpClient: HttpClient) {

    }


    create_VendorTimings (dataDto): Observable<any> {
        return this.httpClient.post<any> (`${this.neighborhoodsApiService.VendorTimings}`, dataDto, this.httpWithToken.get_autorization_header());
    }

    update_VendorTimings (dataDto): Observable<any> {
      return this.httpClient.put<any> (`${this.neighborhoodsApiService.VendorTimings}`, dataDto, this.httpWithToken.get_autorization_header());
    }

    get_VendorTimings (query): Observable<any> {
        return this.httpClient.get<any>(`${this.neighborhoodsApiService.VendorTimings}?filter=${query}`, this.httpWithToken.get_autorization_header());
    }

}
