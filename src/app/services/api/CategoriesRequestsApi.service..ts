import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";
import { HttpClientWitToken } from "../commonServices/custom-http-client.service";
import { NeighborhoodsApiService } from "../neighborhoodsApi.service";




@Injectable({
    providedIn: 'root'
})


export class CategoriesRequestsApiService {
    private listRefresh = new Subject<any>()
    public haslistRefresh:Observable<any>;


    constructor (private httpWithToken: HttpClientWitToken, private neighborhoodsApiService: NeighborhoodsApiService,
        private httpClient: HttpClient) {
        this.haslistRefresh = this.listRefresh.asObservable();
    }


    get_Categories_Request (query): Observable<any> {
        return this.httpClient.get<any>(`${this.neighborhoodsApiService.CategoriesRequests}?filter=${query}`, this.httpWithToken.get_autorization_header());
    }


    create_Categories_Request (dataDto): Observable<any> {
        return this.httpClient.post<any>(`${this.neighborhoodsApiService.CategoriesRequests}`, dataDto, this.httpWithToken.get_autorization_header());
    }

    patch_Categories_Request (dataDto): Observable<any> {
      return this.httpClient.patch<any>(`${this.neighborhoodsApiService.CategoriesRequests}`, dataDto, this.httpWithToken.get_autorization_header());
   }

   set requestGenerated(e){
     this.listRefresh.next({type: e.type, value:true})
   }
}
