import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { UserApiService } from "../api/userApi.service";
import { CookiesService } from "./cookies.service";
import { NotificationService } from "./notification.service";




@Injectable({
    providedIn: 'root'
})


export class GlobalDataService {



    constructor (private userApiService: UserApiService, private cookiesService: CookiesService,
        private notificationService: NotificationService, private router: Router ) {

    }



    method_for_getting_login_user_data (userData) {
        var userQuery:any = {
          where: {
            id: userData['id']
          },
          include: [
              { relation: 'addresses' },
              { relation: 'vendors' },
              { relation: 'profilePic' },
          ]
        }
        userQuery = encodeURI(JSON.stringify(userQuery));
        this.userApiService.get_users_findOne(userQuery).subscribe(
            data => {

                if (data && !data['isActive']) {
                    this.notificationService.get_Notification('info', 'You are maked as inActive please contact Admin');
                } else {
                    this.cookiesService.set_user_login_data(data);
                    if (data['role'] == 'Root') {
                        this.router.navigate(['home/dashboard']);
                    } else if (data['role'] == 'Vendor') {
                        if (data['vendors'].length) {
                            this.cookiesService.set_current_vender_details(data['vendors'][0]);
                        }
                        this.router.navigate(['vendor/dashboard']);
                    } else {
                        this.notificationService.get_Notification('info', 'You can not login with this role');
                        this.cookiesService.clearAll();
                        this.router.navigate(['auth/login']);
                    }
                }
            }, error => {
                this.notificationService.get_Notification('error', error.error.error.message);
            }
        )
    }



    method_for_getting_login_user_role (userData) {
        var userQuery:any = {
          where: {
            id: userData['id']
          },
          include: [
              { relation: 'addresses' },
              { relation: 'vendors' },
              { relation: 'profilePic' },
          ]
        }
        userQuery = encodeURI(JSON.stringify(userQuery));
        this.userApiService.get_users_findOne(userQuery).subscribe(
            data => {

                if (data && !data['isActive']) {
                    this.notificationService.get_Notification('info', 'You are maked as inActive please contact Admin');
                    this.cookiesService.clearAll();
                }else if(data['role'] == 'User' && data.isVendorRequestSubmitted){
                  this.notificationService.get_Notification('info', 'You can not login because of vendor request is pending');
                  this.cookiesService.clearAll();
                } else {
                    this.router.navigate(['/']);
                    this.cookiesService.set_user_login_data(data);
                    if (data['role'] == 'User' ) {
                        setTimeout(() => {
                            location.reload();
                        })
                    } else {
                        this.notificationService.get_Notification('info', 'You can not login with this role');
                        this.cookiesService.clearAll();
                    }
                }
            }, error => {
                this.notificationService.get_Notification('error', error.error.error.message);
            }
        )
    }

}
