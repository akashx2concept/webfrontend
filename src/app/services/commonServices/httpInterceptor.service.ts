import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpEvent, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError, finalize } from 'rxjs/operators';
import { NotificationService } from './notification.service';

import { Router } from '@angular/router';
import { CookiesService } from './cookies.service';
import { LoaderService } from "./loader.service";

declare global {
    interface Window { Intercom: any; }
}

@Injectable()

export class HttpErrorInterceptor implements HttpInterceptor {

    constructor (public notificationService: NotificationService, private cookiesService: CookiesService,
        private loaderService: LoaderService,
        private router: Router) {

    }


    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
       this.showLoader();
        return next.handle(request).pipe(retry(1),catchError((error: HttpErrorResponse) => {
                let errorMessage = error;
                // || error.error.message == "AUTHORIZATION_REQUIRED"
                this.onEnd();
                if (error.status == 401 && error.error.error.code == "AUTHORIZATION_REQUIRED") {
                    this.cookiesService.clearAll();
                    this.notificationService.get_Notification('error', "Your Session is expire please login again");
                    this.router.navigate(['/']);
                    setTimeout(() => {
                        location.reload();
                        return throwError(errorMessage);
                    }, 1000)

                } else return throwError(errorMessage);
            }),
            finalize(() => {
              this.onEnd();
          })
        )
    }

  private onEnd(): void {
      this.hideLoader();
  }
  private showLoader(): void {
      this.loaderService.show();
  }
  private hideLoader(): void {
      this.loaderService.hide();
  }
}
