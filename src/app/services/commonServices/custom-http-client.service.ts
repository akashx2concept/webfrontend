import { OnInit, Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { NeighborhoodsApiService } from '../neighborhoodsApi.service';
import { CookiesService } from './cookies.service';



@Injectable({
    providedIn: 'root'
})

export class HttpClientWitToken implements OnInit {
    private authToken;

    constructor( private cookiesService: CookiesService, private neighborhoodsApiService : NeighborhoodsApiService ) {
        this.authToken = this.cookiesService.get_user_token();
    }

    ngOnInit() {
        
    }

    get_autorization_header(token?){
        if (token != null || undefined) {
            this.authToken = token;
        } else {
            this.authToken = this.cookiesService.get_user_token();
        }
        let httpOptions;
        if (!this.authToken) {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type':  'application/json',
                    // 'Authorization':  this.authToken
                })
            };
        } else {
            httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type':  'application/json',
                    'Authorization':  this.authToken
                })
            };
        }
        return httpOptions;
    }



    get_autorization_header_for_image(token?){
        if (token != null || undefined) {
            this.authToken = token;
        } else {
            this.authToken = this.cookiesService.get_user_token();
        }
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': this.authToken
            })
        };
        return httpOptions;
    }


}