import { Injectable } from "@angular/core";
import { CookieService } from 'ngx-cookie-service';

@Injectable ({
    providedIn: 'root'
})

export class CookiesService {


    constructor (private cookieService: CookieService) {

    }

    setBreadcumbPageNavigation(data) {
        console.log(data, "breadcumbData")
        localStorage.setItem('breadcumbData', JSON.stringify(data));
    }

    getBreadcumbPageNavigation() {
        return JSON.parse(localStorage.getItem('breadcumbData'));
    }

    set_user_login_data (data) {
        var dataObjectString = JSON.stringify(data);
        localStorage.setItem('userdata', dataObjectString);
    }

    get_user_login_data () {
        var data_1 = localStorage.getItem('userdata')
        var data_object = JSON.parse(data_1);
        return data_object;
    }

    get_stripe_status () {
     return  localStorage.getItem('hasStripe');
    }

    set_stripe_status () {
      localStorage.setItem('hasStripe', 'true');
    }

    set_current_vender_details (data) {
        var dataObjectString = JSON.stringify(data);
        localStorage.setItem('currentVendor', dataObjectString);
    }

    get_current_vendor_details () {
        var data_1 = localStorage.getItem('currentVendor')
        var data_object = JSON.parse(data_1);
        return data_object;
    }


    set_user_token (token) {
        localStorage.setItem('token', token);
    }

    get_user_token () {
        var data_1 = localStorage.getItem('token');
        var data_object = data_1;
        return data_object;
    }


    clearAll(){
      localStorage.clear();
    }

}
