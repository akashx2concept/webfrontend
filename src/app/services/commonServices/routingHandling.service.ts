import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import * as _ from "lodash";


@Injectable ({
    providedIn: 'root'
})

export class RoutingHandlingService {


    public resUsableRoute = [
        {
            moduleName: 'Login',
            moduleSlug: 'loginModule',
            path: 'auth/login'
        },
        {
            moduleName: 'SignUp',
            moduleSlug: 'signUpModule',
            path: 'auth/signUp'
        },
        {
            moduleName: 'Home_Dashboard',
            moduleSlug: 'homeDashboardModule',
            path: 'home/dashboard'
        }
    ]


    constructor (private router: Router) {

    }

    method_for_redirect (value) {
        let indexx = _.findIndex(this.resUsableRoute, (o) => { return o['moduleSlug'] == value });
        if (indexx != -1) {
            this.router.navigate([this.resUsableRoute[indexx]['path']]);
        }  
    }

}